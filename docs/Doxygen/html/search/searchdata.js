var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvz",
  1: "abcdeilmnpqst",
  2: "bcdeilmnpst",
  3: "_bcdefghilmprsu",
  4: "_abcdefghiklmnopqrstvz",
  5: "hlmp",
  6: "acdmnpsu",
  7: "abcdegilmnorstu",
  8: "abcdefgilmnprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

