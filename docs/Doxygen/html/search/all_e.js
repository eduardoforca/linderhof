var searchData=
[
  ['opaque',['opaque',['../structBinaryRequestHeader.html#a691b97bbb562b6f9a3e5290fc36d1a6e',1,'BinaryRequestHeader::opaque()'],['../structBinaryResponseHeader.html#a98ff6daf96191d18671b14ad1f93699d',1,'BinaryResponseHeader::opaque()']]],
  ['opcode',['opcode',['../structDNSheader.html#a01dc34aeab2a5fcd6a7c7b563540a485',1,'DNSheader::opcode()'],['../structBinaryRequestHeader.html#abb46290b1a14cf5668b9dbaef624db43',1,'BinaryRequestHeader::opcode()'],['../structBinaryResponseHeader.html#a8d76948459273ced2201a2f0e121ebcd',1,'BinaryResponseHeader::opcode()']]],
  ['opt',['opt',['../structArgsCore.html#ad7f0b5f6b8ebefda1f76d9b75998db1b',1,'ArgsCore']]],
  ['option',['option',['../structArgsOpt.html#abebd44767927f31fe8f87d895218406f',1,'ArgsOpt']]],
  ['optional',['OPTIONAL',['../cliparser_8h.html#ab20bb18c75c9a443891b50ae39de26fba468b290a6b16925fa56374f74698a9d7',1,'cliparser.h']]]
];
