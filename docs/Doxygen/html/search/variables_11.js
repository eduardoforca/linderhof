var searchData=
[
  ['ra',['ra',['../structDNSheader.html#a673da0fb607725d9d530fd525142e821',1,'DNSheader']]],
  ['rcode',['rcode',['../structDNSheader.html#ab8627cd60b5e04d819dbae4683b0f7f3',1,'DNSheader']]],
  ['rd',['rd',['../structDNSheader.html#a04215136857538c53e9dcf3ca4145a92',1,'DNSheader']]],
  ['rdata',['rdata',['../structADDITIONAL.html#a164456de791cc08c6739f3070a43bf1f',1,'ADDITIONAL']]],
  ['rdlen',['rdlen',['../structADDITIONAL.html#a90e5ae65ec09b4e207883514731e9386',1,'ADDITIONAL']]],
  ['ref_5fip',['ref_ip',['../structLhfDraft.html#ae575f2a5fa2dab3d97d7d0c120869eac',1,'LhfDraft']]],
  ['ref_5fip_5flist',['ref_ip_list',['../draft_8h.html#aefb8cbb7382d589aad2ae68e85e83367',1,'draft.h']]],
  ['ref_5fport',['ref_port',['../structLhfDraft.html#a146b843075232b13173ad870a8aa4593',1,'LhfDraft']]],
  ['request',['request',['../structNtpBinaryRequestHeader.html#a9abc74f2a22e0708c0e95271438af4df',1,'NtpBinaryRequestHeader::request()'],['../structNtpBinaryResponseHeader.html#ac4c7b69a8b569d6f99c97d7fd57c7eab',1,'NtpBinaryResponseHeader::request()']]],
  ['requestid',['requestID',['../structTextProtocolHeader.html#a64532d89ca00420bccedea9adb131b62',1,'TextProtocolHeader']]],
  ['reserved',['reserved',['../structTextProtocolHeader.html#a601c155d67ae2409967d8ceda2e48502',1,'TextProtocolHeader']]],
  ['rm_5fvn_5fmode',['rm_vn_mode',['../structNtpBinaryRequestHeader.html#a474b47e3ee479ed2a3e6d6a32dd37762',1,'NtpBinaryRequestHeader::rm_vn_mode()'],['../structNtpBinaryResponseHeader.html#ac07f40b7dfba651b5b6598b0f9b5df75',1,'NtpBinaryResponseHeader::rm_vn_mode()']]]
];
