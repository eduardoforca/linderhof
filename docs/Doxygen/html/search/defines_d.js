var searchData=
[
  ['sampling',['SAMPLING',['../injector_8c.html#acd3fa8f18671a8c8f7b5ee902f6bc7ea',1,'injector.c']]],
  ['set_5fcmd',['SET_CMD',['../memcachedforge_8c.html#a0269f32fb712f0ecc3a0b3639ff46253',1,'memcachedforge.c']]],
  ['set_5fcmd_5fcont',['SET_CMD_CONT',['../memcachedforge_8c.html#aaf34a51e951958a899c5db7e8c072655',1,'memcachedforge.c']]],
  ['set_5fcmd_5fend',['SET_CMD_END',['../memcachedforge_8c.html#aa79bab359f2a2a543d755106e87fdc23',1,'memcachedforge.c']]],
  ['set_5fcmd_5fone',['SET_CMD_ONE',['../memcachedforge_8c.html#a65a2562f0504bd5754cd887274620de4',1,'memcachedforge.c']]],
  ['signal_5ferror',['SIGNAL_ERROR',['../dns_8c.html#ab49536a5015a1813f98dc2ddeb41570a',1,'SIGNAL_ERROR():&#160;dns.c'],['../memcached_8c.html#ab49536a5015a1813f98dc2ddeb41570a',1,'SIGNAL_ERROR():&#160;memcached.c'],['../ntp_8c.html#ab49536a5015a1813f98dc2ddeb41570a',1,'SIGNAL_ERROR():&#160;ntp.c']]],
  ['signal_5fstop',['SIGNAL_STOP',['../dns_8c.html#af533c0913d1a064d4fb9bfbd9d65f1ef',1,'SIGNAL_STOP():&#160;dns.c'],['../memcached_8c.html#af533c0913d1a064d4fb9bfbd9d65f1ef',1,'SIGNAL_STOP():&#160;memcached.c'],['../ntp_8c.html#af533c0913d1a064d4fb9bfbd9d65f1ef',1,'SIGNAL_STOP():&#160;ntp.c']]],
  ['snmp_5fdefault_5fport',['SNMP_DEFAULT_PORT',['../snmpforge_8h.html#af3a18cdf943f1073e57805b95d546175',1,'snmpforge.h']]],
  ['sockaddr_5fsize',['SOCKADDR_SIZE',['../netio_8h.html#a7a25d4b46bbde7c25e66e681ff89d492',1,'netio.h']]],
  ['ssdp_5fdefault_5fport',['SSDP_DEFAULT_PORT',['../ssdpforge_8h.html#a4e1239222cdcd32a7ceb23dadfa4fc19',1,'ssdpforge.h']]],
  ['success',['SUCCESS',['../error_8h.html#aa90cac659d18e8ef6294c7ae337f6b58',1,'error.h']]]
];
