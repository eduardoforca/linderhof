var searchData=
[
  ['mac',['mac',['../structNtpBinaryRequestHeader.html#af57d253ddfb1d63c6d6ba58384023b91',1,'NtpBinaryRequestHeader::mac()'],['../structNtpBinaryResponseHeader.html#a6587f4442fcc9f3dc79ddaa1b2297aea',1,'NtpBinaryResponseHeader::mac()']]],
  ['magic',['magic',['../structBinaryRequestHeader.html#a67f0770bca6a00fb97a872e71f06c531',1,'BinaryRequestHeader::magic()'],['../structBinaryResponseHeader.html#a9352819317eb3e68079bad10d7662d2c',1,'BinaryResponseHeader::magic()']]],
  ['mbz_5fitemsize',['mbz_itemsize',['../structNtpBinaryRequestHeader.html#ab14ef5ceac7fa709dad4f4bb15f7641e',1,'NtpBinaryRequestHeader::mbz_itemsize()'],['../structNtpBinaryResponseHeader.html#a298962452c145ca502cc4ad8d43a31f6',1,'NtpBinaryResponseHeader::mbz_itemsize()']]],
  ['memory',['memory',['../structmemoryData.html#a8695590dfd555369b49d99643d3d6f41',1,'memoryData']]],
  ['mirrorname',['mirrorName',['../structLhfDraft.html#ad23c861be0e7f23d832f36fa2849ac23',1,'LhfDraft']]]
];
