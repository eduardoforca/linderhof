#!/bin/bash

doxygen doxygen-config
echo "Generating PDF"
cd latex
make refman.pdf
echo "Renaming and moving pdf"
mv refman.pdf ReferenceFile.pdf
mv ReferenceFile.pdf ../

