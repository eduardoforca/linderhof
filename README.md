# Linderhof
***

Linderhof é uma biblioteca de ataques distribuídos de negação de serviço por reflexão amplificada, com suporte a diversos protocolos (DNS, NTP, Memcached, SNMP, SSDP, CoAP e Cldap).


# Instalação 
Requisitos:
- Cmake (versão 3.3 ou maior)

O arquivo **build.sh** é usado para a compilação do Linderhof. Os parâmetros aceitos pelo **build.sh** são: 	
	
	-b Build
	-g Generate build files
	-d Generate build files for debugging
	-h Show help

Assim, primeiro os build files devem ser gerados e então o build deve ser rodado. Um exemplo de compilação é:
	
	$ ./build.sh -gb

O binário gerado (lhf) estará na pasta bin do projeto. 

Caso o script não tenha sido rodado com privilégios administrativos, deve ser executado o comando abaixo para que deixe de ser necessário executar o lhf como superuser:
	
	$ sudo setcap cap_net_admin,cap_net_raw+ep bin/lhf


# Execução 

O Linderhof pode ser executado da seguinte forma: 
	
	$ bin/lhf -m <tipo_de_mirror> -t <IP_da_vítima> -r <IP_do_refletor | arquivo_com_lista_de_refletores>

Exemplo:

	$ bin/lhf -m ssdp -t 2001:db8::1 -r reflectors.txt
	
O tipo de mirror escolhido pode influenciar os argumentos necessários para a execução correta do programa. Todos os argumentos, tanto opcionais quanto obrigatórios, podem ser consultados na ajuda do programa:
	
	$ lhf -h



Distribuído sob a licença MIT. Veja [LICENSE](LICENSE) para mais informações.


