#include "injector/controller.h"

#include <math.h>

#include "common/blacksmith.h"
#include "injector/injector.h"
#include "interface/logger.h"

typedef struct {
    Packet *pkt;
    unsigned int injCells;
    unsigned int bucket;
    unsigned int level;
    unsigned int probes;
    float throughputCurrent;
    Injector **injectors;
} ControllerInjector;

int getBucketSize(int injector_idx, int8_t level, uint8_t aggressive_mode,
                  int rate[61], int masterClock) {
    int bucket_injector, bucket_excess, probe = 0;

    probe =
        level == -1 ? rate[masterClock % rate[0] + 1] : ProbesByLevel(level);

    if (level == 1 || aggressive_mode) return probe;

    bucket_injector = probe / num_injectors;

    bucket_excess = probe - bucket_injector * num_injectors;

    if (bucket_injector != 0 && bucket_excess == 0) return bucket_injector;

    if (bucket_excess > injector_idx)
        return bucket_injector + 1;
    else
        return bucket_injector;
}

/** @brief defines the number of probes and the current throughput for
 * p_controller */
static void getInjectorThp(ControllerInjector *p_controller) {
    int pkt = 0;

    for (int i = 0; i < p_controller->injCells; i++)
        pkt += p_controller->injectors[i]->net.pktCounter;
    p_controller->probes = pkt;
    p_controller->throughputCurrent =
        p_controller->probes * p_controller->pkt->pkt_size;
}

/** @brief unlocks the injector semaphore (net.sem). if aggressive_mode unlocks
 * all injectors. */
static void resetBucket(ControllerInjector *p_controller,
                        uint8_t aggressive_mode, uint8_t flooding_mode,
                        int clock) {
    int i;

    for (i = 0; i < p_controller->injCells; i++) {
        p_controller->injectors[i]->net.pktCounter = 0;
        p_controller->injectors[i]->net.pktDropped = 0;
    }

    if (p_controller->level == 1 && !aggressive_mode && !flooding_mode) {
        sem_post(&p_controller->injectors[clock % num_injectors]->net.sem);
        return;
    }

    for (i = 0; i < p_controller->injCells; i++) {
        sem_post(&p_controller->injectors[i]->net.sem);
    }
}

/** @brief Frees all the injectors buckets. Stops the attack*/
static void freeAttack(ControllerInjector *p_controller) {
    for (int i = 0; i < p_controller->injCells; i++) {
        p_controller->injectors[i]->net.freeBucket = true;
    }
}

void StartControllerInjector(Packet *p_pkt, int8_t p_level,
                             uint8_t aggressive_mode, uint8_t flooding_mode,
                             unsigned int p_duration, unsigned int p_inc,
                             int rate[61]) {
    unsigned int masterClock = 0;
    unsigned int incPoint = p_inc;
    int i;

    ControllerInjector controller;
    controller.pkt = p_pkt;
    controller.injCells = num_injectors;

    // printf("p_level = %d\n", p_level);
    // exit(1);

    controller.level = flooding_mode ? 0 : p_level;

    controller.injectors =
        StartInjector(controller.pkt, p_level, aggressive_mode);

    for (i = 0; i < controller.injCells; i++) {
        controller.injectors[i]->net.bucketMax = getBucketSize(
            i, controller.level, aggressive_mode, rate, masterClock);
    }

    if (flooding_mode || p_level > CONTROLLER_MAXLEVEL) {
        freeAttack(&controller);
    }

    while (1) {
        resetBucket(&controller, aggressive_mode, flooding_mode, masterClock);

        SleepOneSec();
        masterClock++;

        getInjectorThp(&controller);
        LogInjection(controller.level, num_injectors, controller.probes,
                     controller.injectors, rate, masterClock - 1);

        if (p_duration > 0 && masterClock >= p_duration) {
            break;
        }

        if (flooding_mode) continue;

        if (p_level == -1 || (controller.level < CONTROLLER_MAXLEVEL &&
                              incPoint != 0 && masterClock == incPoint)) {
            if (p_level != -1) {
                incPoint += p_inc;
                controller.level++;
            }

            for (i = 0; i < controller.injCells; i++) {
                controller.injectors[i]->net.bucketMax = getBucketSize(
                    i, controller.level, aggressive_mode, rate, masterClock);
                IncrementSourcePortNum(controller.injectors[i]->net.pkt);
            }
        }
    }

    for (i = 0; i < controller.injCells; i++) {
        controller.injectors[i]->net.bucketSize = 0;
        controller.injectors[i]->net.freeBucket = false;
    }

    for (i = 0; i < controller.injCells; i++) {
        InjectorDestroy(controller.injectors[i]);
    }
}
