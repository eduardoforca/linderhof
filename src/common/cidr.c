#include "common/cidr.h"

#include "common/common.h"

bool isCidrValid(char *input) {
    char *str, *ip, *mask_char;
    int mask;

    str = malloc(sizeof(char) * (strlen(input) + 1));
    strcpy(str, input);

    ip = strtok(str, "/");
    if (!ip) return false;

    mask_char = strtok(NULL, "\0");
    if (!mask_char) return false;

    mask = strtol(mask_char, NULL, 10);
    if (mask < 16 || mask > 32) return false;

    if (ip_version(ip) == 0) return false;

    free(str);

    return true;
}

void convertCidrToRange(char input[], char *first_address, char *last_address) {
    char *ip, *mask_char;
    int mask;
    unsigned int mask_uint, ip_uint, first_address_uint, last_address_uint;
    struct in_addr first_address_s, last_address_s;

    if (isCidrValid(input) == false) Efatal(ERROR_CIDR, "Invalid CIDR.");

    ip = strtok(input, "/");
    mask_char = strtok(NULL, "\0");

    mask = strtol(mask_char, NULL, 10);

    if (mask == 32) {
        strcpy(first_address, ip);
        strcpy(last_address, ip);
        return;
    }

    mask_uint = ~(0xFFFFFFFF >> mask);

    ip_uint = htonl(inet_addr(ip));

    first_address_uint = ip_uint & mask_uint;
    last_address_uint = ip_uint | ~mask_uint;

    if (mask < 31) {
        first_address_uint += 1;
        last_address_uint -= 1;
    }

    first_address_s.s_addr = htonl(first_address_uint);
    last_address_s.s_addr = htonl(last_address_uint);

    strcpy(first_address, inet_ntoa(first_address_s));
    strcpy(last_address, inet_ntoa(last_address_s));
}

void iterateAndCallFunction(uint8_t oct_first[4], uint8_t oct_last[4],
                            void (*f_payload)(void *, char[16]), void *p_arg) {
    char address[16];

    sprintf(address, "%d.%d.%d.%d", oct_last[0], oct_last[1], oct_last[2],
            oct_last[3]);

    if (oct_last[0] > oct_first[0]) {
        oct_last[0]--;
        iterateAndCallFunction(oct_first, oct_last, f_payload, p_arg);
    } else if (oct_last[1] > oct_first[1]) {
        oct_last[1]--;
        iterateAndCallFunction(oct_first, oct_last, f_payload, p_arg);
    } else if (oct_last[2] > oct_first[2]) {
        oct_last[2]--;
        iterateAndCallFunction(oct_first, oct_last, f_payload, p_arg);
    } else if (oct_last[3] > oct_first[3]) {
        oct_last[3]--;
        iterateAndCallFunction(oct_first, oct_last, f_payload, p_arg);
    }

    f_payload(p_arg, address);
}

void iterateRange(char *first_address, char *last_address,
                  void (*f_payload)(void *, char[16]), void *p_arg) {
    uint8_t oct_first[4], oct_last[4];

    oct_first[0] = strtol(strtok(first_address, "."), NULL, 10);
    oct_first[1] = strtol(strtok(NULL, ".\0"), NULL, 10);
    oct_first[2] = strtol(strtok(NULL, ".\0"), NULL, 10);
    oct_first[3] = strtol(strtok(NULL, ".\0"), NULL, 10);

    oct_last[0] = strtol(strtok(last_address, "."), NULL, 10);
    oct_last[1] = strtol(strtok(NULL, ".\0"), NULL, 10);
    oct_last[2] = strtol(strtok(NULL, ".\0"), NULL, 10);
    oct_last[3] = strtol(strtok(NULL, ".\0"), NULL, 10);

    iterateAndCallFunction(oct_first, oct_last, f_payload, p_arg);
}
