/** Error codes are defined on the .h file
 */
#include "common/common.h"

void defaultHandler(int p_errorCode);

typedef struct {
    void (*func)(int);
} Error;

Error err = {.func = defaultHandler};

/** @brief prints error msg*/
static void printerror(char *p_msg) {
    if (errno != 0) {
        perror(p_msg);
    } else {
        puts(p_msg);
    }
}

/** @brief default error handler*/
void defaultHandler(int p_errorCode) {
    memoryclean();
    exit(p_errorCode);
}

void Efatal(int p_errorCode, char *p_error) {
    printerror(p_error);
    err.func(p_errorCode);
}

/** @brief error log */
int Elog(int p_errorcode, char *p_msg) {
    if (NULL == p_msg) {
        return p_errorcode;
    }

    printerror(p_msg);

    return p_errorcode;
}

void ESetErrorAction(void p_func(int)) { err.func = p_func; }
