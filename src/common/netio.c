/** Socket types are defined on .h file (NetType).
 */
#include "common/netio.h"

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#define DELIM "."
#define MAX_SOCKEBUFFER 10485760

/** @brief create socket of type TCP*/
int createTCPsocket(int af) {
    int fd;

    if ((fd = socket(af, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        ELOG(ERROR_NET, "Cannot open TCP socket\n");
    }
    return fd;
}

/** @brief create socket of type RAW*/
int createRAWsocket(int af) {
    int fd;

    if (af == AF_INET)
        fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    else
        fd = socket(AF_INET6, SOCK_RAW, IPPROTO_UDP);

    if (fd < 0) {
        perror("socket() failed ");
        exit(EXIT_FAILURE);
    }

    return fd;
}

/** @brief create socket of type UDP*/
int createUDPsocket(int af) {
    int fd;

    if ((fd = socket(af, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        ELOG(ERROR_NET, "Cannot open udp socket");
    }
    return fd;
}

int CreateSocket(NetType p_type, bool p_blk, int af) {
    int fd;
    int ret;
    int val = 1;

    switch (p_type) {
        case UDP:
            fd = createUDPsocket(af);
            break;
        case TCP:
            fd = createTCPsocket(af);
            break;
        case RAW:
        default:
            fd = createRAWsocket(af);
    }

    BlockSocket(fd, p_blk);
    // Setting IP_HDRINCL.
    // NOTE: We will provide the IP header, but enabling this option, on linux,
    //            still makes the kernel calculates the checksum and
    //            total_length.

    if (p_type == RAW) {
        if (af == AF_INET)
            ret = setsockopt(fd, IPPROTO_IP, IP_HDRINCL, &val, sizeof(val));
        else
            ret = setsockopt(fd, IPPROTO_IPV6, IPV6_HDRINCL, &val, sizeof(val));

        if (ret < 0) {
            fprintf(stderr, "setsockopt() failed.\nError message: %s",
                    strerror(ret));
            ELOG(ERROR_NET, "Cannot set socket options\n");
        }
    }

    return fd;
}

void CloseSocket(int fd) {
    // Close only if the descriptor is valid.
    if (fd > 0) {
        close(fd);  // AS_SAFE!
    }
}

int SetSocketFlag(int p_socket, int p_flags) {
    int flags;

    if ((flags = fcntl(p_socket, F_GETFL)) == -1) {
        ELOG(ERROR_NET, "Cannot get socket flags\n");
    }

    flags |= p_flags;

    if (fcntl(p_socket, F_SETFL, flags) == -1) {
        ELOG(ERROR_NET, "Cannot set socket flags\n");
    }

    return SUCCESS;
}

int BlockSocket(int p_fd, bool blocking) {
    int flags = 0;

    if (blocking)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;

    return SetSocketFlag(p_fd, flags);
}

Packet *CreateEmptyPacket() {
    Packet *pac = NULL;
    memalloc(&pac, sizeof(Packet));
    pac->type = EMPTY;
    return pac;
}

void ReleasePacket(Packet *p_pkt) {
    memfree(&p_pkt->packet_ptr);
    memfree(&p_pkt);
}

int SendPacket(int p_socket, Packet *p_pkt) {
    switch (p_pkt->type) {
        case TCP:
            if (-1 == send(p_socket, p_pkt->packet_ptr, p_pkt->pkt_size, 0)) {
                return ERROR_NET;
            }
            break;

        case UDP:
        case RAW:
            if (-1 == sendto(p_socket, p_pkt->packet_ptr, p_pkt->pkt_size, 0,
                             (struct sockaddr *)&p_pkt->saddr,
                             sizeof(p_pkt->saddr)))
                return ERROR_NET;
            break;

        default:
            return ERROR_NET;
    }

    return SUCCESS;
}

int8_t ip_version(char *addr) {
    char buf[16];
    if (inet_pton(AF_INET, addr, buf)) {
        return 4;
    } else if (inet_pton(AF_INET6, addr, buf)) {
        return 6;
    }
    return 0;
}

int BindPort(int p_socket, struct sockaddr_in saddr) {
    struct sockaddr_in addrANY = {.sin_family = AF_INET,
                                  .sin_addr.s_addr = INADDR_ANY,
                                  .sin_port = saddr.sin_port};
    socklen_t lenAddr = sizeof(struct sockaddr_in);

    if (bind(p_socket, (struct sockaddr *)&addrANY, lenAddr) == -1) {
        ELOG(ERROR_NET, "BIND FAILED\n");
    }
    if (getsockname(p_socket, (struct sockaddr *)&saddr, &lenAddr) == -1) {
        ELOG(ERROR_NET, "GETSOCKNAME FAILED\n");
    }
    return SUCCESS;
}

/** Adapted from T50.
 */
int Setup_sendbuffer(int p_fd, uint32_t p_n) {
    uint32_t i;
    socklen_t len;

    SetSocketFlag(p_fd, SO_SNDBUFFORCE);

    // Getting SO_SNDBUF.
    len = sizeof(p_n);

    errno = 0;
    if (getsockopt(p_fd, SOL_SOCKET, SO_SNDBUF, &p_n, &len) == -1) {
        Efatal(ERROR_NET, "Cannot get socket buffer");
    }

    // Setting the maximum SO_SNDBUF in bytes.
    // 128      =  1 Kib
    // 10485760 = 80 Mib
    for (i = p_n + 128; i < MAX_SOCKEBUFFER; i += 128) {
        // Setting SO_SNDBUF.
        errno = 0;
        if (setsockopt(p_fd, SOL_SOCKET, SO_SNDBUFFORCE, &i, sizeof(i)) == -1) {
            if (errno == ENOBUFS || errno == ENOMEM) {
                break;
            }
            Efatal(ERROR_NET, "Cannot set socket buffer");
        }
    }
    return p_n;
}

void ConnectTCP(int p_socket, Packet *p_pkt) {
    int size;

    if (ip_version(p_pkt->ip_dest) == 4)
        size = sizeof(struct sockaddr_in);
    else
        size = sizeof(struct sockaddr_in6);

    if (connect(p_socket, (struct sockaddr *)&p_pkt->saddr, size) != 0) {
        Efatal(ERROR_NET, "Connection with the server failed...\n");
    }
}
