/** The list type is defined in the .h file.
 */
#include "common/common.h"

void InsertCell(List **p_list, char *p_data) {
    List *newCell = NULL;

    if (NULL == p_list) {
        return;
    }

    memalloc(&newCell, sizeof(List));
    strcpy(newCell->data, p_data);

    if (NULL == *p_list) {
        newCell->prev = NULL;
        newCell->next = NULL;
        *p_list = newCell;
    } else {
        newCell->prev = (*p_list)->prev;
        newCell->next = *p_list;

        (*p_list)->prev = newCell;
        *p_list = newCell;
    }
}

void InsertCellLast(List **p_list, char *p_data) {
    List *newCell = NULL;

    if (NULL == p_list) {
        return;
    }

    memalloc(&newCell, sizeof(List));
    strcpy(newCell->data, p_data);

    if (NULL == *p_list) {
        newCell->prev = NULL;
        newCell->next = NULL;
        *p_list = newCell;
    } else {
        List *last;
        for (last = *p_list; last->next != NULL; last = last->next)
            ;

        newCell->prev = last;
        newCell->next = NULL;

        last->next = newCell;
    }
}

int HowMany(List **p_list) {
    List *aux = NULL;
    int i = 0;

    if (p_list == NULL || *p_list == NULL) return 0;

    memalloc(&aux, sizeof(List));
    aux = *p_list;

    while (aux != NULL) {
        i++;
        aux = aux->next;
    }

    return (i);
}

char *getElementList(List **p_list, int position) {
    List *aux = NULL;

    memalloc(&aux, sizeof(List));
    aux = *p_list;

    for (int i = 0; i < position; i++) {
        aux = aux->next;
    }

    return (aux->data);
}

void shuffleList(List **p_list) {
    List *aux = NULL;
    int i, size, new_pos;
    char *node;

    memalloc(&aux, sizeof(List));
    memalloc(&node, IPSIZE);

    aux = *p_list;

    size = HowMany(&aux);
    List *nodes[size];

    for (i = 0; i < size; i++) {
        nodes[i] = aux;
        aux = aux->next;
    }

    srand(time(0));

    for (i = 0; i < size; i++) {
        new_pos = rand() % size;
        strcpy(node, nodes[i]->data);
        strcpy(nodes[i]->data, nodes[new_pos]->data);
        strcpy(nodes[new_pos]->data, node);
    }

    memfree(&aux);
    memfree(&node);
}

void Print_List(List **p_cell) {
    List *aux = NULL;

    if ((p_cell == NULL) || (*p_cell == NULL)) {
        printf("NULL\n");
        return;
    }

    memalloc(&aux, sizeof(List));
    aux = *p_cell;

    while (aux->next != NULL) {
        printf("%s, ", aux->data);
        aux = aux->next;
    }
    printf("%s\n", aux->data);
}

void RemoveCell(List **p_cell) {
    List *prev;
    List *next;

    if (NULL == p_cell || NULL == *p_cell) {
        return;
    }

    prev = (*p_cell)->prev;
    next = (*p_cell)->next;

    if (NULL != prev) {
        prev->next = next;
    } else {
        // new first
        *p_cell = next;
    }

    if (NULL != next) {
        next->prev = prev;
    }

    memfree(p_cell);
}

void FreeList(List **p_list) {
    List *aux;

    if (NULL == p_list || NULL == *p_list) {
        return;
    }

    LIST_FOREACH(*p_list, aux) { RemoveCell(p_list); }
}

void saveList(List **p_list, char *path) {
    FILE *fp;
    List *aux;

    fp = fopen(path, "w");

    LIST_FOREACH(*p_list, aux) { fprintf(fp, "%s\n", aux->data); }
}

void removeDuplicates(List **p_list) {
    List *aux, *aux2;

    LIST_FOREACH(*p_list, aux) {
        LIST_FOREACH(aux->next, aux2) {
            if (!strcmp(aux->data, aux2->data)) RemoveCell(&aux2);
        }
    }
}
