#include "common/blacksmith.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "common/common.h"
#include "common/netio.h"

/** @brief Returns checksum for IPV4 header.
 *  @param *buffer should contain all the other header information.
 *  @param *hdr_len is the length of *buffer.
 */
static uint16_t ip_checksum(const void *buffer, size_t hdr_len) {
    unsigned long sum = 0;
    const uint16_t *buf = buffer;

    for (; hdr_len > 1; hdr_len -= 2, *buf++) {
        sum += *buf;
        if (sum & 0x80000000)
            sum = (sum & 0xFFFF) + (sum >> 16);  // fold carries
    }

    // add the padding if packet length is odd
    if (hdr_len & 1) sum += *((uint8_t *)buf);

    while (sum >> 16) sum = (sum & 0xFFFF) + (sum >> 16);

    return (~sum);
}

/** @brief  Returns checksum for udp header.
 *  @param *buffer should contain all the header information
 *  @param src_addr and dest_addr are ip numbers for source and destination
 *  @param ip_v is the ip version
 */
uint16_t udp_checksum(const void *buffer, size_t length, uint32_t *src_addr,
                      uint32_t *dest_addr, int ip_v) {
    const uint16_t *buf = buffer;
    uint16_t *src_ip = (void *)src_addr;
    uint16_t *dest_ip = (void *)dest_addr;
    size_t len;
    uint32_t sum = 0;
    int i;

    for (len = length; len > 1; len -= 2, *buf++) {
        sum += *buf;
        if (sum & 0x80000000)
            sum = (sum & 0xFFFF) + (sum >> 16);  // fold carries
    }

    // add padding if packet length is odd
    if (len & 1) sum += *((uint8_t *)buf);

    i = ip_v == 6 ? 8 : 2;

    for (; i > 0; i--) {
        sum += *src_ip++;
        sum += *dest_ip++;
    }

    sum += htons(IPPROTO_UDP);
    sum += htons(length);

    // sum header if has carry
    while (sum >> 16) sum = (sum & 0xFFFF) + (sum >> 16);

    return (uint16_t)(~sum);
}

/** The UDP datagram memory is allocated here.
 * *datagram = (header ip + header udp + payload)
 *
 *  The source port is defined here at random if not source_port == 0. All the
 * rest of the information on the datagram was passed as parameter or is
 * default.
 */
Packet *ForgeUDP(char *ip_dest, char *ip_src, int dest_port, int source_port,
                 Packet *(*f_payload)(void *), void *p_arg) {
    char *datagram;
    Packet *firstPkt = NULL;
    Packet *lastPkt = NULL;
    uint8_t ip_v = 0;  // ip version
    int frame_length = 0;
    struct ip *ip_header = NULL;  // Pointer to the beginning of ip header
    struct ip6_hdr *ip6_header =
        NULL;                          // Pointer to the beginning of ip6 header
    struct udphdr *udp_header = NULL;  // Pointer to the beginning of udp header
    char *payload_ptr = NULL;          // Pointer to the beginning of payload

    if (NULL == f_payload) {
        Efatal(ERROR_BLACKSMITH, "Internal error");
    }

    ip_v = ip_version(ip_dest);

    for (Packet *payload = f_payload(p_arg); payload != NULL;
         payload = payload->next) {
        Packet *tmp = CreateEmptyPacket();
        tmp->next = NULL;

        datagram = NULL;

        if (ip_v == 4) {
            memalloc(&datagram,
                     IP_HEADER_SIZE + UDP_HEADER_SIZE + payload->pkt_size);
            ip_header =
                (struct ip *)datagram;  // Pointer to the beginning of ip header
            udp_header =
                (struct udphdr *)(datagram +
                                  IP_HEADER_SIZE);  // Pointer to the beginning
                                                    // of udp header
            payload_ptr =
                datagram + IP_HEADER_SIZE +
                UDP_HEADER_SIZE;  // Pointer to the beginning of payload

            // IPv4 header

            // Version
            ip_header->ip_v = 4;
            // Header length
            ip_header->ip_hl = 5;
            // Total length
            ip_header->ip_len =
                IP_HEADER_SIZE + UDP_HEADER_SIZE + payload->pkt_size;
            // Type of service
            ip_header->ip_tos = 0;
            // Fragment offset field
            ip_header->ip_off = 0;
            // Time to live
            ip_header->ip_ttl = 64;
            // Protocol
            ip_header->ip_p = IPPROTO_UDP;
            // Checksum
            ip_header->ip_sum = 0;
            ip_header->ip_sum = ip_checksum(datagram, ip_header->ip_len);
            // Source IPv4 address
            inet_pton(AF_INET, ip_src, &ip_header->ip_src);
            // Destination IPv4 address
            inet_pton(AF_INET, ip_dest, &ip_header->ip_dst);

            // Ethernet frame length = ethernet data (IP header + UDP
            // header
            // + UDP data)
            frame_length = IP_HEADER_SIZE + UDP_HEADER_SIZE + payload->pkt_size;

        } else if (ip_v == 6) {
            memalloc(&datagram,
                     IP6_HEADER_SIZE + UDP_HEADER_SIZE + payload->pkt_size);
            ip6_header = (struct ip6_hdr *)
                datagram;  // Pointer to the beginning of ip header
            udp_header =
                (struct udphdr *)(datagram +
                                  IP6_HEADER_SIZE);  // Pointer to the beginning
                                                     // of udp header
            payload_ptr =
                datagram + IP6_HEADER_SIZE +
                UDP_HEADER_SIZE;  // Pointer to the beginning of payload

            // IPv6 header

            // IPv6 version, Traffic class, Flow label
            ip6_header->ip6_flow = htonl((6 << 28) | (0 << 20) | 0);
            // Payload length
            ip6_header->ip6_plen = htons(UDP_HEADER_SIZE + payload->pkt_size);
            // Next header
            ip6_header->ip6_nxt = IPPROTO_UDP;
            // Hop limit
            ip6_header->ip6_hops = 64;
            // Source IPv6 address
            inet_pton(AF_INET6, ip_src, &ip6_header->ip6_src);
            // Destination IPv6 address
            inet_pton(AF_INET6, ip_dest, &ip6_header->ip6_dst);

            // Ethernet frame length = ethernet data (IP header + UDP
            // header
            // + UDP data)
            frame_length =
                IP6_HEADER_SIZE + UDP_HEADER_SIZE + payload->pkt_size;
        }

        // UDP header
        if (source_port == 0)
            source_port = (rand() % (60000 - 40000 + 1) + 40000);

        // Source port number (16 bits)
        udp_header->source = htons(source_port);
        // Destination port number (16 bits)
        udp_header->dest = htons(dest_port);
        // Length of UDP datagram (16 bits): UDP header + UDP data
        udp_header->len = htons(UDP_HEADER_SIZE + payload->pkt_size);

        // Payload
        memcpy(payload_ptr, payload->packet_ptr, payload->pkt_size);

        // UDP checksum (16 bits)
        udp_header->check = 0;

        if (ip_v == 4) {
            udp_header->check = udp_checksum(
                udp_header, UDP_HEADER_SIZE + payload->pkt_size,
                &ip_header->ip_src.s_addr, &ip_header->ip_dst.s_addr, ip_v);

        } else if (ip_v == 6) {
            udp_header->check =
                udp_checksum(udp_header, UDP_HEADER_SIZE + payload->pkt_size,
                             ip6_header->ip6_src.__in6_u.__u6_addr32,
                             ip6_header->ip6_dst.__in6_u.__u6_addr32, ip_v);
        }

        tmp->type = RAW;
        tmp->packet_ptr = datagram;
        tmp->pkt_size = frame_length;
        tmp->payload_size = payload->pkt_size;
        strcpy(tmp->ip_dest, ip_dest);
        tmp->dest_port = dest_port;

        if (NULL == firstPkt) {
            firstPkt = tmp;
            lastPkt = tmp;
        } else {
            lastPkt->next = tmp;
            lastPkt = tmp;
        }
    }

    return firstPkt;
}

// All the information on the return packet was passed by parameter or is
// default.
Packet *ForgeTCP(char *ip_dest, int dest_port, Packet *(*f_payload)(void *),
                 void *p_arg) {
    Packet *firstPkt = NULL;
    Packet *lastPkt = NULL;
    char *payload_ptr = NULL;  // Pointer to the beginning of payload
    int ip_v = 0;

    struct sockaddr_in *sin;
    struct sockaddr_in6 *sin6;

    if (NULL == f_payload) Efatal(ERROR_BLACKSMITH, "Internal error");

    ip_v = ip_version(ip_dest);

    for (Packet *payload = f_payload(p_arg); payload != NULL;
         payload = payload->next) {
        Packet *tmp = CreateEmptyPacket();
        tmp->next = NULL;

        if (ip_v == 4) {
            sin = (struct sockaddr_in *)&tmp->saddr;
            sin->sin_family = AF_INET;
            inet_pton(AF_INET, ip_dest, &sin->sin_addr);
            sin->sin_port = htons(dest_port);

        } else if (ip_v == 6) {
            sin6 = (struct sockaddr_in6 *)&tmp->saddr;
            sin6->sin6_family = AF_INET6;
            inet_pton(AF_INET6, ip_dest, &sin6->sin6_addr);
            sin6->sin6_port = htons(dest_port);
        }

        memalloc(&payload_ptr, payload->pkt_size);
        memcpy(payload_ptr, payload->packet_ptr, payload->pkt_size);

        tmp->type = TCP;
        tmp->packet_ptr = payload_ptr;
        tmp->pkt_size = payload->pkt_size;
        tmp->payload_size = payload->pkt_size;
        strcpy(tmp->ip_dest, ip_dest);
        tmp->dest_port = dest_port;

        if (NULL == firstPkt) {
            firstPkt = tmp;
            lastPkt = tmp;
        } else {
            lastPkt->next = tmp;
            lastPkt = tmp;
        }
    }

    return firstPkt;
}

void recalculateChecksum(Packet *pac) {
    char *datagram = NULL;
    struct ip *ip_header = NULL;
    struct ip6_hdr *ip6_header = NULL;
    struct udphdr *udp_header = NULL;

    int8_t ip_v = ip_version(pac->ip_dest);

    datagram = pac->packet_ptr;

    if (ip_v == 4) {
        ip_header = (struct ip *)datagram;
        udp_header = (struct udphdr *)(datagram + IP_HEADER_SIZE);

        udp_header->check = 0;

        udp_header->check = udp_checksum(
            udp_header, UDP_HEADER_SIZE + pac->payload_size,
            &ip_header->ip_src.s_addr, &ip_header->ip_dst.s_addr, ip_v);

    } else if (ip_v == 6) {
        ip6_header = (struct ip6_hdr *)datagram;
        udp_header = (struct udphdr *)(datagram + IP6_HEADER_SIZE);

        udp_header->check = 0;

        udp_header->check =
            udp_checksum(udp_header, UDP_HEADER_SIZE + pac->payload_size,
                         ip6_header->ip6_src.__in6_u.__u6_addr32,
                         ip6_header->ip6_dst.__in6_u.__u6_addr32, ip_v);
    }
}

// Used for incremental attacks.
void IncrementSourcePortNum(Packet *pac) {
    int ip_header_size;

    if (ip_version(pac->ip_dest) == 4)
        ip_header_size = IP_HEADER_SIZE;
    else
        ip_header_size = IP6_HEADER_SIZE;

    struct udphdr *udp_header =
        (struct udphdr *)(pac->packet_ptr + ip_header_size);

    udp_header->source = htons(htons(udp_header->source) + 1);

    recalculateChecksum(pac);
}

void changeVictim(Packet *pac, char *victim_address) {
    int ip_header_size;

    if (ip_version(pac->ip_dest) == 4)
        ip_header_size = IP_HEADER_SIZE;
    else
        ip_header_size = IP6_HEADER_SIZE;

    struct ip *ip_header = (struct ip *)pac->packet_ptr;

    struct udphdr *udp_header =
        (struct udphdr *)(pac->packet_ptr + ip_header_size);

    inet_pton(AF_INET, victim_address, &ip_header->ip_src);

    udp_header->check = 0;
    recalculateChecksum(pac);
}
