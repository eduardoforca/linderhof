#ifndef CIDR_H
#define CIDR_H

#include "common/common.h"

/** @brief Check if input is a valid CIDR
 *  @param input source CIDR.
 *  @return true if valid or false if invalid
 */
bool isCidrValid(char *input);

/** @brief Convert CIDR to IP Range
 *  @param input source CIDR.
 *  @param *first_address destination of the first address from range.
 *  @param *last_address destination of the last address from range.
 */
void convertCidrToRange(char input[], char *first_address, char *last_address);

/** @brief Iterate address range passing to given function
 *
 *  @param *first_address Range first address.
 *  @param *last_address Range last address.
 *  @param f_payload Function to call at each address.
 *  @param p_arg Parameter to f_payload function.
 */
void iterateRange(char *first_address, char *last_address,
                  void (*f_payload)(void *, char[16]), void *p_arg);
#endif
