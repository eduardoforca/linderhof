/** Network API
 *  Socket handling functions and IP helper functions.
 */

#ifndef NETIO_H
#define NETIO_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>

#include "common/common.h"

#define IP_HEADER_SIZE sizeof(struct ip)
#define IP6_HEADER_SIZE sizeof(struct ip6_hdr)
#define UDP_HEADER_SIZE sizeof(struct udphdr)

#define BLOCK true
#define NO_BLOCK false
#define SOCKADDR_SIZE sizeof(struct sockaddr)
#define MAX_PKT_SIZE 65507

typedef enum { EMPTY = 1, RAW, UDP, TCP, LHF } NetType;

typedef struct Packet {
    NetType type;                    // type of socket
    void *packet_ptr;                // pointer to packet
    size_t pkt_size;                 // size of packet
    size_t payload_size;             // size of payload (actual data on packet)
    char ip_dest[INET6_ADDRSTRLEN];  // destinations ip
    int dest_port;                   // destinations port
    struct sockaddr_storage saddr;   // represents network address
    struct Packet *next;
} Packet;

/** @brief Creates a BLOCK or NO_BLOCK socket
 *  @param p_type Socket type (Enum NetType)
 *  @param p_blk set BLOCK or NO_BLOCK socket mode
 *                   (p_blk == 0 then no_block; p_blk == 1 then block)
 *  @param af AF_INET or AF_INET6
 *  @return socket file descriptor if successful and -1 if not.
 */
int CreateSocket(NetType p_type, bool p_blk, int af);

/** @brief Closes Socket
 * @param fd socket descriptor
 */
void CloseSocket(int fd);

/** @brief Sets socket to BLOCK or NO_BLOCK mode
 *
 *  @param p_fd Socket file descriptor
 *  @param blocking BLOCK or NO_BLOCK socket mode
 *                  (blocking == 0 then no_block; blocking == 1 them block)
 *  @return 0 if successful and < 0 if not.
 */
int BlockSocket(int p_fd, bool blocking);

/** @brief Sets socket flags
 *
 *  @param p_socket Socket file descriptor
 *  @param p_flags Flags to be set
 *  @return SUCCESS
 */
int SetSocketFlag(int p_socket, int p_flags);

/** @brief Create an empty packet
 *  @returns packet created
 */
Packet *CreateEmptyPacket();

/** @brief Frees a Packet
 * @param p_pkt pointer to packet
 */
void ReleasePacket(Packet *p_pkt);

/** @brief sends packet via TCP or UDP or thru raw socket
 *
 *  @param p_pkt Packet to be sent
 *  @return SUCCESS
 */
int SendPacket(int p_socket, Packet *p_pkt);

/** @brief Allocates port number to a socket
 *
 *  @param p_socket socket to bind port to
 *  @param saddr port
 *  @return SUCCESS
 */
int BindPort(int p_socket, struct sockaddr_in saddr);

/** @brief Finds IP version
 *
 *  @param src IP
 *  @return 4 if IPv4, 6 if IPv6 and 0 if not found
 */
int8_t ip_version(char *src);

/** @brief Sets maximum socket send buffer in bytes
 *
 *  @param p_fd File descriptor
 *  @param p_n Minimal buffer size
 *  @return p_n
 */
int Setup_sendbuffer(int p_fd, uint32_t p_n);

/** @brief Connects to the client socket to the server socket
 *
 *  @param p_socket client socket
 *  @param p_pkt packet with the server saddr
 */
void ConnectTCP(int p_socket, Packet *p_pkt);

#endif  // NETIO_H
