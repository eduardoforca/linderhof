/** Responsible for the forge of packets.
 *  It includes TCP and UDP packets.
 *  Used on all the mirror manager files.
 */

#ifndef BLACKSMITH_H
#define BLACKSMITH_H

#include "common/netio.h"
#include "draft.h"

/** @brief UDP packet forge
 *
 *  @param ip_dest IP packet destination
 *  @param ip_src IP packet source
 *  @param dest_port Destination port
 *  @param f_payload Function that returns a Packet struct with
 *  pkt_pointer containing the pointer to the payload data.
 *  @param p_arg Parameter to f_payload function. If f_payload function is
 *  NULL p_arg with be used as payload
 *  @return the packet forged
 */
Packet *ForgeUDP(char *ip_dest, char *ip_src, int dest_port, int source_port,
                 Packet *(*f_payload)(void *), void *p_arg);

/** @brief TCP packet forge
 *
 *  @param ip_dest IP packet destination
 *  @param dest_port Destination port
 *  @param f_payload This function must return a Packet struct with
 * pkt_pointer containing the pointer to the payload data.
 *  @param p_arg Parameter to f_payload function.
 *  @return the packet forged
 */
Packet *ForgeTCP(char *ip_dest, int dest_port, Packet *(*f_payload)(void *),
                 void *p_arg);

/** @brief Increments the source port number
 *
 *  @param pac the packet whose udp header port needs to be incremented.
 */
void IncrementSourcePortNum(Packet *pac);

/** @brief Recalculate UDP Checksum
 *
 *  @param pac the packet whose udp checksum needs to be recalculated.
 */
void recalculateChecksum(Packet *pac);

/** @brief Recalculate UDP Checksum
 *
 *  @param pac the packet whose victim needs to be changed.
 *  @param victim_address the address in the new victim.
 */
void changeVictim(Packet *pac, char *victim_address);

#endif
