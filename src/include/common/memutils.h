/** Memory manager.
 */

#ifndef MEMUTILS_H
#define MEMUTILS_H

// easier usage of memoryalloc and memoryfree
#define memalloc(pointer, size) memoryalloc((void **)pointer, size, __func__)
#define memfree(pointer) memoryfree((void **)pointer)

/** @brief Allocates memory
 *  @param p_ptr pointer where memory will be allocated
 *  @param p_size size of memory to be allocated
 *  @param p_func function calling memoryalloc
 */
void memoryalloc(void **p_ptr, size_t p_size, const char *const p_func);

/** @brief Frees memory
 *  @param p_ptr pointer to be freed
 */
void memoryfree(void **p_ptr);

/** @brief Releases all allocated memory
 */
void memoryclean();

#endif
