/** File used for the reflectors list
 */
#ifndef LIST_REFLECTORS_H
#define LIST_REFLECTORS_H

#include <netinet/in.h>

#define IPSIZE INET6_ADDRSTRLEN

typedef struct ListData {
    char data[IPSIZE];
    struct ListData *next;
    struct ListData *prev;
} List;

#define LIST_FOREACH(p_list, aux_list) \
    for (aux_list = p_list; NULL != aux_list; aux_list = aux_list->next)

/** @brief Inserts Cell in list LILO
 *  @param p_list list where the cell is going to be inserted
 *  @param p_data data of the cell
 */
void InsertCell(List **p_list, char *p_data);

/** @brief Inserts Cell in list FIFO
 *  @param p_list list where the cell is going to be inserted
 *  @param p_data data of the cell
 */
void InsertCellLast(List **p_list, char *p_data);

char *getElementList(List **p_list, int position);

void shuffleList(List **p_list);

/** @brief Finds the number of elements in p_list
 *  @param p_list
 *  @return number of elements in p_list
 */
int HowMany(List **p_list);

void Print_List(List **p_cell);

/** @brief Removes p_cell of list
 *  @param p_cell
 */
void RemoveCell(List **p_cell);

/** @brief Frees p_list
 *  @param p_list
 */
void FreeList(List **p_list);

/** @brief Save a p_list to a file
 *  @param p_list
 *  @param path
 */
void saveList(List **p_list, char *path);

/** @brief Remove duplicates elements from p_list
 *  @param p_list
 */
void removeDuplicates(List **p_list);

#endif
