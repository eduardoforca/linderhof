/** Error handler and functions.
 */

#ifndef ERROR_H
#define ERROR_H

#include <errno.h>

#define SUCCESS 0
#define ERROR_FATAL -1

#define ERROR_MEMORY -10
#define ERROR_NET -20
#define ERROR_FILE -30
#define ERROR_THREAD -40
#define ERROR_SIGNAL -50

#define ERROR_DRAFT -100
#define ERROR_MIRROR -200

#define ERROR_INJECTOR -300
#define ERROR_INTERFACE -400
#define ERROR_PLANNER -500
#define ERROR_BLACKSMITH -600
#define ERROR_CIDR -700

#define ERROR_MEMCACHED -800

/** @brief displays error message
 *
 *  @param p_errorCode error code
 *  @param p_msg message to display
 *  @return error code.
 */

int Elog(int p_errorCode, char *p_msg);

#define ELOG(p_errorCode, p_msg) return Elog(p_errorCode, p_msg)

#define LOG(p_msg) Elog(SUCCESS, p_msg)

/** @brief sets error function handler on error fatal
 *
 * @param p_func function that will be used on fatal error
 */

void ESetErrorAction(void p_func(int));

/** @brief fires fatal error
 *
 *  @param p_errorCode error code
 *  @param p_error error message
 */

void Efatal(int p_errorCode, char *p_error);

#endif
