/** Thread handling.
 */
#ifndef PTHREADHELPER_H
#define PTHREADHELPER_H

#include <pthread.h>

#define LVL_FIFO 0
#define LVL_HIGH 1
#define LVL_MID 2
#define LVL_LOW 3

#define MAX_PRIOLVL(sched) sched_get_priority_max(sched)
#define LOW_PRIOLVL(sched) sched_get_priority_min(sched)
#define MID_PRIOLVL(sched) (MAX_PRIOLVL(sched) + LOW_PRIOLVL(sched)) / 2

/** @brief Creates a new thread
 *
 *  @param p_level current level
 *  @param p_func function where to start the execution of new thread
 *  @param p_args arguments of p_func
 *  @return new thread or ERROR_THREAD
 */
pthread_t CreateThread(int p_level, void *(*p_func)(void *), void *p_arg);

/** @brief Sets thread core affinity
 *  @param p_pid thread
 *  @param p_core core
 */
void SetThreadCore(pthread_t p_pid, int p_core);

/** @brief Sets thread priority level in scheduler
 *  @param p_level level to be set
 */
void SetThreadLevel(int p_level);

#endif
