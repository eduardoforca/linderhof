/** Time helper functions
 */

#ifndef TIMEHELPER_H
#define TIMEHELPER_H

/** @brief Gets string of current time with date.
 *  @param buf Buffer to output
 */
void GetCurrentTimeStr(char *buf);

/** @brief Sleeps one second
 */
void SleepOneSec();

/** @brief Sleeps one minute
 */
void SleepOneMinute();

/** @brief Sleeps a number of seconds
 *  @param p_sec how many seconds to sleep
 */
void SleepSec(unsigned int p_sec);

#endif
