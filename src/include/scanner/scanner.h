#ifndef SCANNER_H
#define SCANNER_H

#include "draft.h"

/** @brief Scan for reflectors
 *  @param p_arg attack draft
 */
void run_scanner(LhfDraft *p_draft);

#endif
