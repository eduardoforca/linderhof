#ifndef BENCHMARK_H
#define BENCHMARK_H

#include "common/common.h"
#include "draft.h"

typedef struct Benchmark {
    Packet *pkt;
    char *res;
    int res_length;
    int res_counter;
    float amp;
    void *info;
    struct Benchmark *next;
} Benchmark;

/** @brief Benchmark with different parameters
 *  @param p_arg attack draft
 */
void run_benchmark(LhfDraft *p_draft);

#endif
