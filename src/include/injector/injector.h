#ifndef INJECTOR_H
#define INJECTOR_H

#include <pthread.h>
#include <semaphore.h>

#include "common/common.h"
#include "injector/controller.h"

extern int num_injectors;

typedef struct {
    Packet *pkt;      // packet to be send
    int socket;       // socket number
    bool freeBucket;  // true if attack is over, false if attack is happening
    unsigned int bucketMax;     // maximum size of bucket
    unsigned int bucketSize;    // current size of bucket
    unsigned int pktCounter;    // number of packets send
    unsigned int pktDropped;    // number of packets dropped
    unsigned int victimsCount;  // number of victims
    unsigned int victimIdx;     // first victim index
    sem_t sem;                  // semaphore
} InjectorNet;

typedef struct {
    pthread_t id;     // thread
    InjectorNet net;  // injector operation information
} Injector;

/** @brief create and starts all injector with parameters and default
 * information
 *
 * @param p_pkt packet data
 * @param level
 * @param aggressive_mode flag for aggressive mode attack
 * @param p_file pointer to log file
 *
 * @return injector handler struct (array of injectors)
 */
Injector **StartInjector(Packet *p_pkt, int8_t level, uint8_t aggressive_mode);

/** @brief cancels thread and frees injector memory
 *
 * @param p_injector packet data to destroy.
 */
void InjectorDestroy(Injector *p_injector);

#endif /*INJECTOR_H*/
