/** Controls the attack injection.
 */
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "common/netio.h"

#define MEGABYTE 1048576
#define CONTROLLER_MAXLEVEL 10

#define ProbesByLevel(level) pow(10, (level - 1))

/** @brief gets the size of an injection bucket
 *
 *  @param injector_idx injector id
 *  @param level
 *  @param aggressive_mode flag
 *  @param rate array with custom rate
 *  @param masterClock
 *  @return size of the bucket
 */
int getBucketSize(int injector_idx, int8_t level, uint8_t aggressive_mode,
                  int rate[61], int masterClock);

/** @brief Starts the injector, controls the duration of the attack, manages
 * incremental attacks and calls for the end of the attack.
 *
 * @param p_pkt packet data to inject
 * @param p_level attack level
 * @param aggressive_mode flag for aggressive mode attack
 * @param flooding_mode flag for flooding mode attack
 * @param p_duration attack execution time
 * @param p_inc flag for incremental attack
 * @param rate array for custom rate
 */
void StartControllerInjector(Packet *p_pkt, int8_t p_level,
                             uint8_t aggressive_mode, uint8_t flooding_mode,
                             unsigned int p_duration, unsigned int p_inc,
                             int rate[61]);

#endif
