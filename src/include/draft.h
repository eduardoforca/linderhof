/*
 *  Library with definitions needed for attack draft
 */
#ifndef DRAFT_H
#define DRAFT_H

#include "common/common.h"

#define DEFAULT_COMPORT 2056
#define IPSIZE INET6_ADDRSTRLEN

typedef enum {
    NOTSET,
    DNS,
    MEMCACHED_GETSET,
    MEMCACHED_STATS,
    NTP,
    SSDP,
    SNMP,
    COAP,
    CLDAP
} MirrorType;

typedef struct {
    MirrorType type;          // Type of mirror to use
    char mirrorName[20];      // Mirror name
    uint8_t ip_version;       // IP version
    char target_ip[IPSIZE];   // Target IP
    char ref_ip[IPSIZE];      // Reflector IP
    int victimsCount;         // Victims counter
    uint32_t target_port;     // Target port
    uint32_t ref_port;        // Reflector port
    int8_t level;             // Attack level
    uint8_t aggressive_mode;  // Don't share level between reflectors
    int rate[61];             // Rate to send packets
    uint8_t flooding_mode;    // Send as many packets as possible
    int duration;             // Duration of attack in minutes
    uint32_t incAttack;       // Enable increment atttack if > 0
    char *args[3];            // Mirror CLI parameters
    char *scan_cidr;          // Scan for reflectors at given CIDR
    char *scan_path;          // File path to save reflectors found
    bool benchmark;           // Test mirror parameters
} LhfDraft;

List **ref_ip_list;  // reflectors list
List **vic_ip_list;  // victims list

typedef enum { SZX, URI_PATH } COAP_ARGS;
typedef enum { DNS_DOMAIN } DNS_ARGS;
typedef enum { UPNP_VERSION, UNICAST, REFLECTOR_IP } SSDP_ARGS;
typedef enum { COMMUNITY_STRING, MAX_REPETITIONS } SNMP_ARGS;

#endif
