/** Parser for the configuration file
 */

#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include "draft.h"

#define CONFIGURATION_FILE_NAME "linderhof.conf"
#define MAX_CONFIG_PARAM_LEN 20
#define MAX_CONFIG_VALUE_LEN 100
#define MAX_LINE_SIZE 100
#define DELIM "="

typedef int (*handler_t)(char, char *, LhfDraft *);
typedef int (*handlerGui_t)(char, char *);

/** @brief Parser for the configuration file
 *
 * @param file_path path of the configuration file
 * @param p_draft  LhfDraft applied to handler
 * @param handler  function handler to which the args read from the file are
 * applied
 * @return SUCCESS if execution is completed.
 */
int ParserConfig(char *file_path, LhfDraft *p_draft, handler_t handler);

/** @brief Parser for the configuration file on graphical interface
 *
 * @param file_path path of the configuration file
 * @param handler  function handler to which the args read from the file are
 * applied
 * @return SUCCESS if execution is completed.
 */
int ParserConfigGui(char *file_path, handlerGui_t handler);

#endif
