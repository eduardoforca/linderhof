/** Logging functions.
 */
#ifndef LOGGER_H
#define LOGGER_H

#include "draft.h"
#include "injector/injector.h"

/** @brief Prints the header and attack plan
 *
 * @param draft attack draft
 */
void LogHeader(LhfDraft draft);

/** @brief Logs the injection process
 *
 * @param p_level attack level
 * @param num_injector number of injectors
 * @param p_probes number of probes provided
 * @param injector array of injectors
 * @param rate array of custom rate
 * @param masterClock
 */
void LogInjection(unsigned int p_level, int num_injectors,
                  unsigned int p_probes, Injector **injector, int rate[61],
                  int masterClock);

#endif
