/** Contains the client parser, its helper functions and structs.
 * Used in interface for command line arguments.
 */

#ifndef CLIPARSER_H
#define CLIPARSER_H

#include "draft.h"

#define ARG_CMD -1

typedef enum { REQUIRED, OPTIONAL, NONE } ARG_OPTION;

typedef struct {
    char args;          // Short argument
    char argl[20];      // Long argument
    ARG_OPTION option;  // Is input mandatory?
    bool bounden;       // Is it a mandatory arg?
    char help[100];     // Help message
} ArgsOpt;

typedef struct {
    ArgsOpt *opt;  // Command args
    int (*handler)(char p_key, char *p_arg,
                   LhfDraft *p_draft);  // Function handler
} ArgsCore;

/** @brief Client Parser
 *
 *  @param p_core a ArgsCore struct that should contain all the possible
 * arguments and the function handler that treats them
 *  @param p_argc number of command line arguments
 *  @param p_argv command line arguments
 *  @param p_data Lhf Draft used as argument for the ArgsCore struct (applyed to
 * handler).
 *  @return SUCCESS if the execution is completed
 */
int ParserCLI(ArgsCore *p_core, int p_argc, char **p_argv, void *p_data);

#endif
