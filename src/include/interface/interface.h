/** Interface functions and helper structs
 */
#ifndef INTERFACE_H
#define INTERFACE_H

#include "draft.h"

#define DEFAULT_DURATION 0
// args
#define ARG_AGGRESSIVE 'a'
#define ARG_BENCHMARK 'b'
#define ARG_CONFIG 'c'
#define ARG_DURATION 'd'
#define ARG_CUSTOM_RATE 'e'
#define ARG_FLOOD 'f'
#define ARG_TARGET_PORT 'g'
#define ARG_HELP 'h'
#define ARG_INCREMENT 'i'
#define ARG_LEVEL 'l'
#define ARG_MIRROR 'm'
#define ARG_SHUFFLE 'o'
#define ARG_REFLECTOR_PORT 'p'
#define ARG_REFLECTOR_IP 'r'
#define ARG_SCANNER_CIDR 's'
#define ARG_SCANNER_PATH 'n'
#define ARG_TARGET_IP 't'
#define ARG_SNMP_COMMUNITY_STRING 'C'
#define ARG_DNS_DOMAIN 'D'
#define ARG_COAP_URI_PATH 'P'
#define ARG_SNMP_MAX_REPETITIONS 'R'
#define ARG_SSDP_UNICAST 'U'
#define ARG_SSDP_UPNP_VERSION 'V'
#define ARG_COAP_SZX 'Z'

#define COMMANDPKT_HEADERSIZE (sizeof(CmdType) + sizeof(int))

#define GetClientIP(p_addr) inet_ntoa(p_addr.sin_addr)

typedef enum { UnknownCmd = 0, AttackCmd, ExitCmd } CmdType;

typedef struct {
    CmdType type;
    unsigned int dataSize;
    LhfDraft data;
} CommandPkt;

typedef struct {
    int socket;
    struct sockaddr_in addr;
} ClientAddr;

/** @brief Prepares the LhfDraft and calls the start of the attack
 *  @param p_argc number of command line arguments
 *  @param p_argv command line arguments
 */
void Interface(int p_argc, char **p_argv);

/** @brief initializes the attack tool and the validates the draft
 *
 *  @param p_argc number of command line arguments
 *  @param p_argv command line arguments
 */
void LinderhofCli(int p_argc, char **p_argv);

#endif
