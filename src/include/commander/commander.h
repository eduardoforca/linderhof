/** Attack managing and starting functions
 */
#ifndef COMMANDER_H
#define COMMANDER_H

#include "draft.h"

/** @brief makes most of the necessary adjustments and initializations so that
 * the attack can start
 */
void linderhofBootstrap();

/** @brief requests the attack
 *
 * @param p_draft the attack draft
 * @return SUCCESS
 * */
int StartMirrorAttack(LhfDraft p_draft);

#endif
