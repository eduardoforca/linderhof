/** Planner and Hall of Mirrors
 *  builds the attack
 */
#ifndef HOM_H
#define HOM_H

#include "draft.h"

typedef struct {
    MirrorType type;
    void *atkData;  // attack draft
    int (*atk_cmd)(
        void *);  // ExecuteMirror function (changes with mirror type)
} LhfPlan;

/** @brief calls for mirror execution
 * @param p_plan attack plan
 */
void CallMirrors(LhfPlan *p_plan);

/** @brief constructs the attack plan
 *
 *  @param p_draft attack draft
 *  @return attack plan
 * */
LhfPlan *Planner(LhfDraft *p_draft);

#endif
