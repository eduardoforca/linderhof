/** Forges the attack
 * Sets all the protocol specific information
 */

#ifndef PACKETFURNACE_H
#define PACKETFURNACE_H

#include "common/netio.h"

#define SSDP_DEFAULT_PORT 1900

typedef struct __attribute__((__packed__)) {
    char *request_method;
    char *request_uri;
    char *request_version;
    char *host;
    char *man;
    char *mx;
    char *st;
    char *cpfn_upnp;
} SsdpBinaryRequestHeader;

/** @brief Sets all the SSDP protocol information on packet
 * @param p_arg command line mirror specific parameters (UNICAST)
 * @return packet forged
 */
Packet *ForgeSSDP(void *p_arg);

#endif
