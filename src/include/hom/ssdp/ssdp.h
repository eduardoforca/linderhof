/** Attack using SSDP protocol
 */

#ifndef SSDP_H
#define SSDP_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define SSDP_BENCHMARK_SLEEP_TIME 10

/** @brief Executes attack with the SSDP mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteSsdpMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_arg attack draft
 *  @return the packet forged
 */
Packet *createSsdpScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printSsdpBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createSsdpBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset);

#endif
