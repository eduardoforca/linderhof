/** Attack using memcached protocol
 */
#ifndef MEMCACHED_H
#define MEMCACHED_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define MEMCACHED_BENCHMARK_SLEEP_TIME 2

typedef struct {
    Packet *getPacket;
    Packet *setPacket;
    LhfDraft *draft;
} AttackPlan;

/** @brief Executes attack with the memcached mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteMemcachedMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_arg attack draft
 *  @return the packet forged
 */
Packet *createMemcachedScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printMemcachedBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createMemcachedBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                       uint8_t preset);

#endif
