/** Attack using CLDAP protocol
 */

#ifndef CLDAP_H
#define CLDAP_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define CLDAP_BENCHMARK_SLEEP_TIME 2

/** @brief Executes attack with the CLDAP mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteCldapMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_arg attack draft
 *  @return the packet forged
 */
Packet *createCldapScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printCldapBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createCldapBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                   uint8_t preset);

#endif
