/** Forges the attack
 * Sets all the protocol specific information
 */

#ifndef PACKETFURNACE_H
#define PACKETFURNACE_H

#include "common/netio.h"

#define CLDAP_DEFAULT_PORT 389

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapOperation;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapMessageId;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapProtocolOp;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapBaseObject;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapScope;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapDerefAliases;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapSizeLimit;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapTimeLimit;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} CldapTypesOnly;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapFilter;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapFilterAttribute;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapFilterValue;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapAttributes;

typedef struct {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} CldapAttribute;

typedef struct __attribute__((__packed__)) {
    CldapOperation *cldapOperation;
    CldapMessageId *cldapMessageId;
    CldapProtocolOp *cldapProtocolOp;
    CldapBaseObject *cldapBaseObject;
    CldapScope *cldapScope;
    CldapDerefAliases *cldapDerefAliases;
    CldapSizeLimit *cldapSizeLimit;
    CldapTimeLimit *cldapTimeLimit;
    CldapTypesOnly *cldapTypesOnly;
    CldapFilter *cldapFilter;
    CldapFilterAttribute *cldapFilterAttribute;
    CldapFilterValue *cldapFilterValue;
    CldapAttributes *cldapAttributes;
    CldapAttribute *cldapAttribute;
} CldapBinaryRequestHeader;

/** @brief Sets all the cldap protocol information on packet
 * @param p_arg command line mirror specific parameters (COMMUNITY_STRING,
 * MAX_REPETITIONS)
 * @return packet forged
 */
Packet *ForgeCLDAP(void *p_arg);

#endif
