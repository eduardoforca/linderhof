/** Attack using SNMP protocol
 */

#ifndef SNMP_H
#define SNMP_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define SNMP_BENCHMARK_SLEEP_TIME 4

/** @brief Executes attack with the SNMP mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteSnmpMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_arg attack draft
 *  @return the packet forged
 */
Packet *createSnmpScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printSnmpBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createSnmpBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset);

#endif
