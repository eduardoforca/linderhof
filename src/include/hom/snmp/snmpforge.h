/** Forges the attack
 * Sets all the protocol specific information
 */

#ifndef PACKETFURNACE_H
#define PACKETFURNACE_H

#include "common/netio.h"

#define SNMP_DEFAULT_PORT 161

typedef struct {
    uint8_t type;
    uint8_t length;
} SnmpSnmpMessage;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} SnmpSnmpVersion;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} SnmpCommunityString;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
} SnmpSnmpPdu;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint32_t value;
} SnmpRequestId;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint8_t value;
} SnmpNoRepeaters;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint16_t value;
} SnmpMaxRepetitions;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
} SnmpVarbindList;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
} SnmpVarbind;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
    uint8_t value[];
} SnmpOid;

typedef struct __attribute__((__packed__)) {
    uint8_t type;
    uint8_t length;
} SnmpVarbindValue;

typedef struct __attribute__((__packed__)) {
    SnmpSnmpMessage *snmpMessage;
    SnmpSnmpVersion *snmpVersion;
    SnmpCommunityString *communityString;
    SnmpSnmpPdu *snmpPdu;
    SnmpRequestId *requestId;
    SnmpNoRepeaters *noRepeaters;
    SnmpMaxRepetitions *maxRepetitions;
    SnmpVarbindList *varbindList;
    SnmpVarbind *varbind;
    SnmpOid *oid;
    SnmpVarbindValue *varbindValue;
} SnmpBinaryRequestHeader;

/** @brief Sets all the snmp protocol information on packet
 * @param p_arg command line mirror specific parameters (COMMUNITY_STRING,
 * MAX_REPETITIONS)
 * @return packet forged
 */
Packet *ForgeSNMP(void *p_arg);

#endif
