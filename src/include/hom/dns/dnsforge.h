/** Forges the attack
 * Sets all the protocol specific information
 */

#ifndef PACKETFURNACE_H
#define PACKETFURNACE_H

#include "common/netio.h"

#define DNS_DEFAULT_PORT 53
#define THROUGHPUT_DEFAULT 0

typedef struct {
    unsigned short id;  // identification number

    unsigned char rd : 1;      // recursion desired
    unsigned char tc : 1;      // truncated message
    unsigned char aa : 1;      // authoritative answer
    unsigned char opcode : 4;  // purpose of message
    unsigned char qr : 1;      // query/response flag

    unsigned char rcode : 4;  // response code
    unsigned char cd : 1;     // checking disabled
    unsigned char ad : 1;     // authenticated data
    unsigned char z : 1;      // its z! reserved
    unsigned char ra : 1;     // recursion available

    unsigned short q_count;     // number of question entries
    unsigned short ans_count;   // number of answer entries
    unsigned short auth_count;  // number of authority entries
    unsigned short add_count;   // number of resource entries

} DNSheader;

typedef struct {
    unsigned short qtype;
    unsigned short qclass;
} QUESTION;

typedef struct {
    uint16_t atype;
    uint16_t aclass;
    uint32_t ttl;
    uint16_t rdlen;
    uint16_t test;
    uint32_t rdata;
    uint64_t test1;
} ADDITIONAL;

/** @brief Sets all the dns protocol information on packet
 * @param p_arg command line mirror specific parameters (DNS_DOMAIN)
 * @return packet forged
 */
Packet *ForgeDNS(void *p_arg);

#endif