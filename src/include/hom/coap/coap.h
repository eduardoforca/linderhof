/** Attack using coap protocol
 */
#ifndef COAP_H
#define COAP_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define COAP_BENCHMARK_SLEEP_TIME 3

/** @brief Executes attack with the coap mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteCoapMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_draft attack draft
 *  @return the packet forged
 */
Packet *createCoapScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printCoapBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createCoapBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset);

#endif
