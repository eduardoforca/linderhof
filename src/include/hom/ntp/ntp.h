/** Attack using NTP protocol
 */
#ifndef NTP_H
#define NTP_H

#include "common/common.h"
#include "draft.h"
#include "scanner/benchmark.h"

#define NTP_BENCHMARK_SLEEP_TIME 2

/** @brief Executes attack with the NTP mirror
 *  @param p_arg attack draft
 *  @return SUCCESS
 */
int ExecuteNtpMirror(void *p_arg);

/** @brief Create packet to use with scanner
 *  @param p_arg attack draft
 *  @return the packet forged
 */
Packet *createNtpScanPacket(LhfDraft *p_draft);

/** @brief Print information about a benchmark
 *  @param benchmark
 */
void printNtpBenchmarkInfo(Benchmark *Benchmark);

/** @brief Create packets to do benchmark
 *  @param p_draft attack draft
 *  @param benchmark
 *  @param preset benchmark number to be executed
 *  @return 1 if successful and 0 if not
 */
uint8_t createNtpBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                 uint8_t preset);

#endif
