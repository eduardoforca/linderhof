#include "scanner/benchmark.h"

#include <unistd.h>

#include "common/cidr.h"
#include "common/common.h"
#include "draft.h"
#include "hom/cldap/cldap.h"
#include "hom/coap/coap.h"
#include "hom/dns/dns.h"
#include "hom/memcached/memcached.h"
#include "hom/ntp/ntp.h"
#include "hom/snmp/snmp.h"
#include "hom/ssdp/ssdp.h"
#include "injector/injector.h"

#define WAIT_TIME 5
#define BUFLEN 100000

typedef struct {
    int fd;
    struct sockaddr_in *sin;
    Packet *p_pkt;
} s_send_packet_args;

typedef struct {
    int *fd;
    struct sockaddr_in *sin;
    socklen_t *slen;
    Benchmark *benchmark;
} s_listener_args;

int getSleepTime(LhfDraft *p_draft) {
    if (!strcmp(p_draft->mirrorName, "CLDAP"))
        return CLDAP_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "COAP"))
        return COAP_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "DNS"))
        return DNS_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "MEMCACHED GETSET") ||
             !strcmp(p_draft->mirrorName, "MEMCACHED STAT"))
        return MEMCACHED_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "NTP"))
        return NTP_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "SNMP"))
        return SNMP_BENCHMARK_SLEEP_TIME;
    else if (!strcmp(p_draft->mirrorName, "SSDP"))
        return SSDP_BENCHMARK_SLEEP_TIME;
    else
        Efatal(ERROR_INTERFACE, "Invalid mirror name");

    return 1;
}

uint8_t createBenchmark(LhfDraft *p_draft, Benchmark **benchmark,
                        uint8_t preset) {
    if (!strcmp(p_draft->mirrorName, "CLDAP"))
        return createCldapBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "COAP"))
        return createCoapBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "DNS"))
        return createDnsBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "MEMCACHED GETSET") ||
             !strcmp(p_draft->mirrorName, "MEMCACHED STAT"))
        return createMemcachedBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "NTP"))
        return createNtpBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "SNMP"))
        return createSnmpBenchmarkPacket(p_draft, benchmark, preset);
    else if (!strcmp(p_draft->mirrorName, "SSDP"))
        return createSsdpBenchmarkPacket(p_draft, benchmark, preset);
    else
        Efatal(ERROR_INTERFACE, "Invalid mirror name");

    return 0;
}

void printBenchmarkInfo(LhfDraft *p_draft, Benchmark *benchmark) {
    if (!strcmp(p_draft->mirrorName, "CLDAP"))
        printCldapBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "COAP"))
        printCoapBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "DNS"))
        printDnsBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "MEMCACHED GETSET") ||
             !strcmp(p_draft->mirrorName, "MEMCACHED STAT"))
        printMemcachedBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "NTP"))
        printNtpBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "SNMP"))
        printSnmpBenchmarkInfo(benchmark);
    else if (!strcmp(p_draft->mirrorName, "SSDP"))
        printSsdpBenchmarkInfo(benchmark);
    else
        Efatal(ERROR_INTERFACE, "Invalid mirror name");
}

void send_benchmark_packet(int fd, struct sockaddr_in *sin, Packet *p_pkt,
                           char address[16]) {
    inet_pton(AF_INET, address, &sin->sin_addr);
    if (ERROR_NET == SendPacket(fd, p_pkt)) perror("sendto()");
}

void *listen_benchmark_responses(void *p_arg) {
    s_listener_args *args = (s_listener_args *)p_arg;
    char buf[BUFLEN + 1];
    int res_length;

    while (1) {
        res_length = recvfrom(*args->fd, buf, BUFLEN, 0, args->sin, args->slen);

        if (res_length == -1) perror("recvfrom()");

        if (args->benchmark->res != NULL) strcpy(args->benchmark->res, buf);

        args->benchmark->res_length += res_length;
        args->benchmark->res_counter++;
    }

    return NULL;
}

Benchmark *getBenchmark(Benchmark *benchmark, int idx) {
    for (int i = 0; i < idx; i++) {
        benchmark = benchmark->next;
        if (benchmark == NULL) break;
    }

    return benchmark;
}

void run_benchmark(LhfDraft *p_draft) {
    struct sockaddr_in *sin_dst, sin_src;
    socklen_t slen = sizeof(sin_dst);
    int fd, preset, benchmark_num, sleep_time;
    pthread_t listener;
    List *ip_list;

    Benchmark *benchmark = NULL;
    Benchmark *curr_benchmark = NULL;

    memalloc(&ip_list, sizeof(List));
    ip_list = *ref_ip_list;

    fd = CreateSocket(UDP, BLOCK, AF_INET);

    if (p_draft->target_port == 0)
        p_draft->target_port = (rand() % (60000 - 40000 + 1) + 40000);

    sin_src.sin_port = htons(p_draft->target_port);
    BindPort(fd, sin_src);

    sleep_time = getSleepTime(p_draft);

    while (ip_list != NULL) {
        preset = 0;
        benchmark_num = 0;

        strcpy(p_draft->ref_ip, ip_list->data);
        printf("%s\n\n", ip_list->data);

        while (1) {
            curr_benchmark = getBenchmark(benchmark, benchmark_num);

            if (curr_benchmark == NULL) {
                if (!createBenchmark(p_draft, &benchmark, preset++)) break;
                curr_benchmark = getBenchmark(benchmark, benchmark_num);
            }

            sin_dst = (struct sockaddr_in *)&curr_benchmark->pkt->saddr;
            sin_dst->sin_family = AF_INET;
            sin_dst->sin_port = htons(curr_benchmark->pkt->dest_port);

            s_listener_args listener_args = {.fd = &fd,
                                             .sin = sin_dst,
                                             .slen = &slen,
                                             .benchmark = curr_benchmark};
            listener = CreateThread(LVL_HIGH, listen_benchmark_responses,
                                    &listener_args);
            if (ERROR_THREAD == listener)
                Efatal(ERROR_THREAD, "Cannot create thread: ERROR_THREAD\n");

            sleep(1);

            send_benchmark_packet(fd, sin_dst, curr_benchmark->pkt,
                                  ip_list->data);

            printBenchmarkInfo(p_draft, curr_benchmark);

            // int prev_res_counter = 0;
            // while (1) {
            //     sleep(1);
            //     if (prev_res_counter == curr_benchmark->res_counter) break;
            //     prev_res_counter = curr_benchmark->res_counter;
            // }
            sleep(sleep_time);

            curr_benchmark->amp = (float)curr_benchmark->res_length /
                                  (float)curr_benchmark->pkt->pkt_size;

            printf("AMP: %-7.1f REQ: %-4lu(1) RES: %-6d(%d)\n\n",
                   curr_benchmark->amp, curr_benchmark->pkt->pkt_size,
                   curr_benchmark->res_length, curr_benchmark->res_counter);

            benchmark_num++;
        }

        ip_list = ip_list->next;

        free(benchmark);  // TODO: Fix free
        benchmark = NULL;

        printf("========================================\n");
    }
    close(fd);
    memfree(&ip_list);
}
