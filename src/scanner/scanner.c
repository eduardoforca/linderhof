#include "scanner/scanner.h"

#include <math.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common/cidr.h"
#include "common/common.h"
#include "draft.h"
#include "hom/cldap/cldap.h"
#include "hom/coap/coap.h"
#include "hom/dns/dns.h"
#include "hom/memcached/memcached.h"
#include "hom/ntp/ntp.h"
#include "hom/snmp/snmp.h"
#include "hom/ssdp/ssdp.h"
#include "injector/injector.h"

#define WAIT_TIME 5
#define BUFLEN 512

typedef struct {
    int *fd;
    struct sockaddr_in *sin;
    socklen_t *slen;
} s_listener_args;

typedef struct {
    int fd;
    struct sockaddr_in *sin;
    Packet *p_pkt;
} s_send_packet_args;

Packet *createScanPacket(LhfDraft *p_draft) {
    Packet *p_pkt = NULL;

    if (!strcmp(p_draft->mirrorName, "CLDAP"))
        p_pkt = createCldapScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "COAP"))
        p_pkt = createCoapScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "DNS"))
        p_pkt = createDnsScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "MEMCACHED GETSET") ||
             !strcmp(p_draft->mirrorName, "MEMCACHED STAT"))
        p_pkt = createMemcachedScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "NTP"))
        p_pkt = createNtpScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "SNMP"))
        p_pkt = createSnmpScanPacket(p_draft);
    else if (!strcmp(p_draft->mirrorName, "SSDP"))
        p_pkt = createSsdpScanPacket(p_draft);
    else
        Efatal(ERROR_INTERFACE, "Invalid mirror name");

    return p_pkt;
}

void send_scanner_packet(void *p_arg, char address[16]) {
    s_send_packet_args *args = (s_send_packet_args *)p_arg;

    inet_pton(AF_INET, address, &args->sin->sin_addr);
    if (ERROR_NET == SendPacket(args->fd, args->p_pkt)) perror("sendto()");
}

void *listen_scanner_responses(void *p_arg) {
    s_listener_args *args = (s_listener_args *)p_arg;
    char buf[BUFLEN + 1];
    char ip[16];

    if (ref_ip_list == NULL)
        memalloc(&ref_ip_list, sizeof(List));
    else
        FreeList(ref_ip_list);

    // printf("Listening...\n");

    while (1) {
        if (recvfrom(*args->fd, buf, BUFLEN, 0, args->sin, args->slen) == -1) {
            perror("recvfrom()");
        }

        strcpy(ip, (char *)inet_ntoa((struct in_addr)args->sin->sin_addr));
        InsertCellLast(ref_ip_list, ip);

        printf("FOUND %s:%d\n", inet_ntoa(args->sin->sin_addr),
               ntohs(args->sin->sin_port));
        // printf("Data: %s\n\n", buf);
    }

    return NULL;
}

void run_scanner(LhfDraft *p_draft) {
    struct sockaddr_in *sin_dst, sin_src;
    socklen_t slen = sizeof(sin_dst);
    int fd, listener, refCount;
    char *first_address, *last_address;

    first_address = malloc(16 * sizeof(char));
    last_address = malloc(16 * sizeof(char));

    convertCidrToRange(p_draft->scan_cidr, first_address, last_address);
    printf("Scanning IP range %s to %s\n\n", first_address, last_address);

    Packet *p_pkt = createScanPacket(p_draft);

    fd = CreateSocket(UDP, BLOCK, AF_INET);

    if (p_draft->target_port == 0)
        p_draft->target_port = (rand() % (60000 - 40000 + 1) + 40000);

    sin_src.sin_port = htons(p_draft->target_port);
    BindPort(fd, sin_src);

    sin_dst = (struct sockaddr_in *)&p_pkt->saddr;
    sin_dst->sin_family = AF_INET;
    sin_dst->sin_port = htons(p_pkt->dest_port);

    s_listener_args listener_args = {.fd = &fd, .sin = sin_dst, .slen = &slen};
    listener = CreateThread(LVL_HIGH, listen_scanner_responses, &listener_args);
    if (ERROR_THREAD == listener)
        Efatal(ERROR_THREAD, "Cannot create thread: ERROR_THREAD\n");

    sleep(1);

    s_send_packet_args send_packet_args = {
        .fd = fd, .sin = sin_dst, .p_pkt = p_pkt};
    iterateRange(first_address, last_address, send_scanner_packet,
                 &send_packet_args);

    sleep(WAIT_TIME);

    close(fd);

    removeDuplicates(ref_ip_list);

    refCount = HowMany(ref_ip_list);
    if (refCount == 0) {
        printf("no reflectors found\n");
        return;
    }

    printf("\n%d reflectors found: ", refCount);
    Print_List(ref_ip_list);

    if (p_draft->scan_path != NULL) {
        printf("Saving list to %s\n", p_draft->scan_path);
        saveList(ref_ip_list, p_draft->scan_path);
    }
}
