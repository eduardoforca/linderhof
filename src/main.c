#include "common/common.h"
#include "interface/interface.h"
#include "interface/gui.h"

int main(int argc, char **argv) {

    if(argc == 1) {
        run_gui(argc, argv);
        return 0;
    }

    setvbuf(stdout, NULL, _IOLBF, 0);
    LinderhofCli(argc, argv);
    memoryclean();
    return 0;
}
