#include "hom/coap/coapforge.h"

#include <netinet/in.h>
#include <netinet/udp.h>
#include <time.h>

#include "draft.h"

Packet *ForgeCoap(void *p_arg) {
    char **args = (char **)p_arg;
    Packet *pac = NULL;
    char *mem_packet;
    BinaryRequestHeader *mem_header = NULL;
    uint16_t message_id;
    uint8_t szx = 6;
    char uri_path[20];

    if (args[SZX] != NULL) {
        szx = strtol(args[SZX], NULL, 10);
    }

    if (args[URI_PATH] != NULL) {
        strcpy(uri_path, args[URI_PATH]);
    } else {
        strcpy(uri_path, ".well-known/core");
    }

    int packetSize =
        sizeof(BinaryRequestHeader) + strlen(uri_path) + BLOCK2SIZE;

    memalloc((void *)&pac, sizeof(Packet));

    memalloc((void *)&mem_packet, packetSize);

    mem_header = (BinaryRequestHeader *)mem_packet;

    message_id = rand() % 65536;

    mem_header->version = 1;
    mem_header->type = CON;
    mem_header->tk_len = 0;
    mem_header->code = GET;
    mem_header->message_id = htons(message_id);

    int pac_written = sizeof(BinaryRequestHeader);

    CoapOption opt;
    opt.delta = URIPATH;
    char *spath = strtok(uri_path, "/");
    while (spath) {
        opt.len = strlen(spath);
        memcpy(mem_packet + pac_written, &opt, sizeof(CoapOption));
        pac_written += sizeof(CoapOption);

        memcpy(mem_packet + pac_written, spath, opt.len);
        pac_written += opt.len;

        opt.delta = URIPATHCONT;
        spath = strtok(NULL, "/");
    }

    opt.delta = BLOCK2;
    opt.len = 1;
    memcpy(mem_packet + pac_written, &opt, sizeof(CoapOption));
    pac_written += sizeof(CoapOption);
    mem_packet[pac_written++] = szx;

    pac->packet_ptr = mem_packet;
    pac->pkt_size = pac_written;

    return pac;
}