#include "hom/cldap/cldapforge.h"

#include <netinet/udp.h>
#include <time.h>

#include "draft.h"

Packet *ForgeCLDAP(void *p_arg) {
    Packet *pac = NULL;
    char *cldap_packet;
    CldapBinaryRequestHeader *cldap_header = NULL;
    int i, packetSize, offset = 0;

    int cldapOperationSize, cldapMessageIdSize, cldapProtocolOpSize,
        cldapBaseObjectSize, cldapScopeSize, cldapDerefAliasesSize,
        cldapSizeLimitSize, cldapTimeLimitSize, cldapTypesOnlySize,
        cldapFilterSize, cldapFilterAttributeSize, cldapFilterValueSize,
        cldapAttributesSize, cldapAttributeSize;

    memalloc((void *)&pac, sizeof(Packet));

    cldap_header = malloc(sizeof(CldapBinaryRequestHeader));

    cldapMessageIdSize = sizeof(CldapMessageId);
    cldap_header->cldapMessageId = malloc(cldapMessageIdSize);
    cldap_header->cldapMessageId->type = 0x02;
    cldap_header->cldapMessageId->length = 0x01;
    cldap_header->cldapMessageId->value = 0x99;

    char base_object[] = "";  // root
    cldapBaseObjectSize = sizeof(CldapBaseObject) + strlen(base_object);
    cldap_header->cldapBaseObject = malloc(cldapBaseObjectSize);
    cldap_header->cldapBaseObject->type = 0x04;  // Octet string
    cldap_header->cldapBaseObject->length = strlen(base_object);
    for (i = 0; i < strlen(base_object); i++)
        cldap_header->cldapBaseObject->value[i] = base_object[i];

    cldapScopeSize = sizeof(CldapScope);
    cldap_header->cldapScope = malloc(cldapScopeSize);
    cldap_header->cldapScope->type = 0x0A;  // Enumerated
    cldap_header->cldapScope->length = 0x01;
    cldap_header->cldapScope->value = 0x00;  // base

    cldapDerefAliasesSize = sizeof(CldapDerefAliases);
    cldap_header->cldapDerefAliases = malloc(cldapDerefAliasesSize);
    cldap_header->cldapDerefAliases->type = 0x0A;  // Enumerated
    cldap_header->cldapDerefAliases->length = 0x01;
    cldap_header->cldapDerefAliases->value = 0x03;  // derefAlways

    cldapSizeLimitSize = sizeof(CldapSizeLimit);
    cldap_header->cldapSizeLimit = malloc(cldapSizeLimitSize);
    cldap_header->cldapSizeLimit->type = 0x02;  // Integer
    cldap_header->cldapSizeLimit->length = 0x01;
    cldap_header->cldapSizeLimit->value = 0x00;  // no size limit

    cldapTimeLimitSize = sizeof(CldapTimeLimit);
    cldap_header->cldapTimeLimit = malloc(cldapTimeLimitSize);
    cldap_header->cldapTimeLimit->type = 0x02;  // Integer
    cldap_header->cldapTimeLimit->length = 0x01;
    cldap_header->cldapTimeLimit->value = 0x00;  // no time limit

    cldapTypesOnlySize = sizeof(CldapTypesOnly);
    cldap_header->cldapTypesOnly = malloc(cldapTypesOnlySize);
    cldap_header->cldapTypesOnly->type = 0x01;  // Boolean
    cldap_header->cldapTypesOnly->length = 0x01;
    cldap_header->cldapTypesOnly->value =
        0x00;  // false (attributes and values)

    char filter_attribute[] = "objectClass";
    cldapFilterAttributeSize =
        sizeof(CldapFilterAttribute) + strlen(filter_attribute);
    cldap_header->cldapFilterAttribute = malloc(cldapFilterAttributeSize);
    cldap_header->cldapFilterAttribute->type = 0x04;  // Octet string
    cldap_header->cldapFilterAttribute->length = strlen(filter_attribute);
    for (i = 0; i < strlen(filter_attribute); i++)
        cldap_header->cldapFilterAttribute->value[i] = filter_attribute[i];

    char filter_value[] = "*";
    cldapFilterValueSize = sizeof(CldapFilterValue) + strlen(filter_value);
    cldap_header->cldapFilterValue = malloc(cldapFilterValueSize);
    cldap_header->cldapFilterValue->type = 0x04;  // Octet string
    cldap_header->cldapFilterValue->length = strlen(filter_value);
    for (i = 0; i < strlen(filter_value); i++)
        cldap_header->cldapFilterValue->value[i] = filter_value[i];

    cldapFilterSize = sizeof(CldapFilter) + 4;
    cldap_header->cldapFilter = malloc(cldapFilterSize);
    cldap_header->cldapFilter->type = 0xA3;  // equal
    cldap_header->cldapFilter->length = 0x84;
    cldap_header->cldapFilter->value[0] = 0x00;
    cldap_header->cldapFilter->value[1] = 0x00;
    cldap_header->cldapFilter->value[2] = 0x00;
    cldap_header->cldapFilter->value[3] =
        0x02 + strlen(filter_attribute) + 0x02 + strlen(filter_value);

    char attribute[] = "*";
    cldapAttributeSize = sizeof(CldapAttribute) + strlen(attribute);
    cldap_header->cldapAttribute = malloc(cldapAttributeSize);
    cldap_header->cldapAttribute->type = 0x04;  // Octet string
    cldap_header->cldapAttribute->length = strlen(attribute);
    for (i = 0; i < strlen(attribute); i++)
        cldap_header->cldapAttribute->value[i] = attribute[i];

    cldapAttributesSize = sizeof(CldapAttributes) + 4;
    cldap_header->cldapAttributes = malloc(cldapAttributesSize);
    cldap_header->cldapAttributes->type = 0x30;  // Sequence
    cldap_header->cldapAttributes->length = 0x84;
    cldap_header->cldapAttributes->value[0] = 0x00;
    cldap_header->cldapAttributes->value[1] = 0x00;
    cldap_header->cldapAttributes->value[2] = 0x00;
    cldap_header->cldapAttributes->value[3] = 0x02 + strlen(attribute);

    cldapProtocolOpSize = sizeof(CldapProtocolOp) + 4;
    cldap_header->cldapProtocolOp = malloc(cldapProtocolOpSize);
    cldap_header->cldapProtocolOp->type = 0x63;
    cldap_header->cldapProtocolOp->length = 0x84;
    cldap_header->cldapProtocolOp->value[0] = 0x00;
    cldap_header->cldapProtocolOp->value[1] = 0x00;
    cldap_header->cldapProtocolOp->value[2] = 0x00;
    cldap_header->cldapProtocolOp->value[3] =
        cldapBaseObjectSize + cldapScopeSize + cldapDerefAliasesSize +
        cldapSizeLimitSize + cldapTimeLimitSize + cldapTypesOnlySize +
        cldapFilterSize + cldapFilterAttributeSize + cldapFilterValueSize +
        cldapAttributesSize + cldapAttributeSize;

    cldapOperationSize = sizeof(CldapOperation) + 4;
    cldap_header->cldapOperation = malloc(cldapOperationSize);
    cldap_header->cldapOperation->type = 0x30;
    cldap_header->cldapOperation->length = 0x84;
    cldap_header->cldapOperation->value[0] = 0x00;
    cldap_header->cldapOperation->value[1] = 0x00;
    cldap_header->cldapOperation->value[2] = 0x00;
    cldap_header->cldapOperation->value[3] =
        cldapMessageIdSize + cldapProtocolOpSize + cldapBaseObjectSize +
        cldapScopeSize + cldapDerefAliasesSize + cldapSizeLimitSize +
        cldapTimeLimitSize + cldapTypesOnlySize + cldapFilterSize +
        cldapFilterAttributeSize + cldapFilterValueSize + cldapAttributesSize +
        cldapAttributeSize;

    packetSize = cldapOperationSize + cldapMessageIdSize + cldapProtocolOpSize +
                 cldapBaseObjectSize + cldapScopeSize + cldapDerefAliasesSize +
                 cldapSizeLimitSize + cldapTimeLimitSize + cldapTypesOnlySize +
                 cldapFilterSize + cldapFilterAttributeSize +
                 cldapFilterValueSize + cldapAttributesSize +
                 cldapAttributeSize;

    memalloc((void *)&cldap_packet, packetSize);

    offset = 0;
    memmove(cldap_packet + offset, cldap_header->cldapOperation,
            cldapOperationSize);
    offset += cldapOperationSize;
    memmove(cldap_packet + offset, cldap_header->cldapMessageId,
            cldapMessageIdSize);
    offset += cldapMessageIdSize;
    memmove(cldap_packet + offset, cldap_header->cldapProtocolOp,
            cldapProtocolOpSize);
    offset += cldapProtocolOpSize;
    memmove(cldap_packet + offset, cldap_header->cldapBaseObject,
            cldapBaseObjectSize);
    offset += cldapBaseObjectSize;
    memmove(cldap_packet + offset, cldap_header->cldapScope, cldapScopeSize);
    offset += cldapScopeSize;
    memmove(cldap_packet + offset, cldap_header->cldapDerefAliases,
            cldapDerefAliasesSize);
    offset += cldapDerefAliasesSize;
    memmove(cldap_packet + offset, cldap_header->cldapSizeLimit,
            cldapSizeLimitSize);
    offset += cldapSizeLimitSize;
    memmove(cldap_packet + offset, cldap_header->cldapTimeLimit,
            cldapTimeLimitSize);
    offset += cldapTimeLimitSize;
    memmove(cldap_packet + offset, cldap_header->cldapTypesOnly,
            cldapTypesOnlySize);
    offset += cldapTypesOnlySize;
    memmove(cldap_packet + offset, cldap_header->cldapFilter, cldapFilterSize);
    offset += cldapFilterSize;
    memmove(cldap_packet + offset, cldap_header->cldapFilterAttribute,
            cldapFilterAttributeSize);
    offset += cldapFilterAttributeSize;
    memmove(cldap_packet + offset, cldap_header->cldapFilterValue,
            cldapFilterValueSize);
    offset += cldapFilterValueSize;
    memmove(cldap_packet + offset, cldap_header->cldapAttributes,
            cldapAttributesSize);
    offset += cldapAttributesSize;
    memmove(cldap_packet + offset, cldap_header->cldapAttribute,
            cldapAttributeSize);
    offset += cldapAttributeSize;

    pac->packet_ptr = cldap_packet;
    pac->pkt_size = packetSize;

    free(cldap_header->cldapOperation);
    free(cldap_header->cldapMessageId);
    free(cldap_header->cldapProtocolOp);
    free(cldap_header->cldapBaseObject);
    free(cldap_header->cldapScope);
    free(cldap_header->cldapDerefAliases);
    free(cldap_header->cldapSizeLimit);
    free(cldap_header->cldapTimeLimit);
    free(cldap_header->cldapTypesOnly);
    free(cldap_header->cldapFilter);
    free(cldap_header->cldapFilterAttribute);
    free(cldap_header->cldapFilterValue);
    free(cldap_header->cldapAttributes);
    free(cldap_header->cldapAttribute);
    free(cldap_header);

    return pac;
}
