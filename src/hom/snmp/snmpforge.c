#include "hom/snmp/snmpforge.h"

#include <netinet/udp.h>
#include <time.h>

#include "draft.h"

Packet *ForgeSNMP(void *p_arg) {
    char **args = (char **)p_arg;
    Packet *pac = NULL;
    char *snmp_packet;
    SnmpBinaryRequestHeader *snmp_header = NULL;
    int i, packetSize, offset = 0;
    char community_string[20];
    int max_repetitions;

    if (args[COMMUNITY_STRING] != NULL) {
        strcpy(community_string, args[COMMUNITY_STRING]);
    } else {
        strcpy(community_string, "public");
    }

    if (args[MAX_REPETITIONS] != NULL) {
        max_repetitions = strtol(args[MAX_REPETITIONS], NULL, 10);
    } else {
        max_repetitions = 2000;
    }

    int snmpMessageSize, snmpVersionSize, communityStringSize, snmpPduSize,
        requestIdSize, noRepeatersSize, maxRepetitionsSize, varbindListSize,
        varbindSize, oidSize, varbindValueSize;

    uint32_t request_id = rand() % 4294967295;

    memalloc((void *)&pac, sizeof(Packet));

    snmp_header = malloc(sizeof(SnmpBinaryRequestHeader));

    varbindValueSize = sizeof(SnmpVarbindValue);
    snmp_header->varbindValue = malloc(varbindValueSize);
    snmp_header->varbindValue->type = 0x05;  // Null
    snmp_header->varbindValue->length = 0x00;

    oidSize = sizeof(SnmpOid) + 1 * sizeof(uint8_t);
    snmp_header->oid = malloc(oidSize);
    snmp_header->oid->type = 0x06;  // Object Identifier
    snmp_header->oid->length = 0x01;
    snmp_header->oid->value[0] = 0x2b;

    varbindSize = sizeof(SnmpVarbind);
    snmp_header->varbind = malloc(varbindSize);
    snmp_header->varbind->type = 0x30;  // Sequence
    snmp_header->varbind->length = oidSize + varbindValueSize;

    varbindListSize = sizeof(SnmpVarbindList);
    snmp_header->varbindList = malloc(varbindListSize);
    snmp_header->varbindList->type = 0x30;  // Sequence
    snmp_header->varbindList->length = varbindSize + oidSize + varbindValueSize;

    maxRepetitionsSize = sizeof(SnmpMaxRepetitions);
    snmp_header->maxRepetitions = malloc(maxRepetitionsSize);
    snmp_header->maxRepetitions->type = 0x02;  // Integer
    snmp_header->maxRepetitions->length = 0x02;
    snmp_header->maxRepetitions->value =
        (max_repetitions >> 8) | (max_repetitions << 8);

    noRepeatersSize = sizeof(SnmpNoRepeaters);
    snmp_header->noRepeaters = malloc(noRepeatersSize);
    snmp_header->noRepeaters->type = 0x02;  // Integer
    snmp_header->noRepeaters->length = 0x01;
    snmp_header->noRepeaters->value = 0x00;

    requestIdSize = sizeof(SnmpRequestId);
    snmp_header->requestId = malloc(requestIdSize);
    snmp_header->requestId->type = 0x02;  // Integer
    snmp_header->requestId->length = 0x04;
    snmp_header->requestId->value =
        ((request_id >> 24) & 0xff) |       // move byte 3 to byte 0
        ((request_id >> 8) & 0xff00) |      // move byte 2 to byte 1
        ((request_id << 8) & 0xff0000) |    // move byte 1 to byte 2
        ((request_id << 24) & 0xff000000);  // move byte 0 to byte 3

    snmpPduSize = sizeof(SnmpSnmpPdu);
    snmp_header->snmpPdu = malloc(snmpPduSize);
    snmp_header->snmpPdu->type = 0xa5;  // GetBulkRequest
    snmp_header->snmpPdu->length = requestIdSize + noRepeatersSize +
                                   maxRepetitionsSize + varbindListSize +
                                   varbindSize + oidSize + varbindValueSize;

    communityStringSize =
        sizeof(SnmpCommunityString) + strlen(community_string);
    snmp_header->communityString = malloc(communityStringSize);
    snmp_header->communityString->type = 0x04;  // Octet String
    snmp_header->communityString->length = strlen(community_string);

    for (i = 0; i < strlen(community_string); i++) {
        snmp_header->communityString->value[i] = community_string[i];
    }

    snmpVersionSize = sizeof(SnmpSnmpVersion);
    snmp_header->snmpVersion = malloc(snmpVersionSize);
    snmp_header->snmpVersion->type = 0x02;  // Integer
    snmp_header->snmpVersion->length = 0x01;
    snmp_header->snmpVersion->value = 0x01;

    snmpMessageSize = sizeof(SnmpSnmpMessage);
    snmp_header->snmpMessage = malloc(snmpMessageSize);
    snmp_header->snmpMessage->type = 0x30;  // Sequence
    snmp_header->snmpMessage->length =
        snmpVersionSize + communityStringSize + snmpPduSize + requestIdSize +
        noRepeatersSize + maxRepetitionsSize + varbindListSize + varbindSize +
        oidSize + varbindValueSize;

    packetSize = snmpMessageSize + snmpVersionSize + communityStringSize +
                 snmpPduSize + requestIdSize + noRepeatersSize +
                 maxRepetitionsSize + varbindListSize + varbindSize + oidSize +
                 varbindValueSize;

    memalloc((void *)&snmp_packet, packetSize);

    offset = 0;
    memmove(snmp_packet + offset, snmp_header->snmpMessage, snmpMessageSize);
    offset += snmpMessageSize;
    memmove(snmp_packet + offset, snmp_header->snmpVersion, snmpVersionSize);
    offset += snmpVersionSize;
    memmove(snmp_packet + offset, snmp_header->communityString,
            communityStringSize);
    offset += communityStringSize;
    memmove(snmp_packet + offset, snmp_header->snmpPdu, snmpPduSize);
    offset += snmpPduSize;
    memmove(snmp_packet + offset, snmp_header->requestId, requestIdSize);
    offset += requestIdSize;
    memmove(snmp_packet + offset, snmp_header->noRepeaters, noRepeatersSize);
    offset += noRepeatersSize;
    memmove(snmp_packet + offset, snmp_header->maxRepetitions,
            maxRepetitionsSize);
    offset += maxRepetitionsSize;
    memmove(snmp_packet + offset, snmp_header->varbindList, varbindListSize);
    offset += varbindListSize;
    memmove(snmp_packet + offset, snmp_header->varbind, varbindSize);
    offset += varbindSize;
    memmove(snmp_packet + offset, snmp_header->oid, oidSize);
    offset += oidSize;
    memmove(snmp_packet + offset, snmp_header->varbindValue, varbindValueSize);

    pac->packet_ptr = snmp_packet;
    pac->pkt_size = packetSize;

    free(snmp_header->snmpMessage);
    free(snmp_header->snmpVersion);
    free(snmp_header->communityString);
    free(snmp_header->snmpPdu);
    free(snmp_header->requestId);
    free(snmp_header->noRepeaters);
    free(snmp_header->maxRepetitions);
    free(snmp_header->varbindList);
    free(snmp_header->varbind);
    free(snmp_header->oid);
    free(snmp_header->varbindValue);
    free(snmp_header);

    return pac;
}
