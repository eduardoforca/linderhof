#include "hom/ssdp/ssdpforge.h"

#include <netinet/udp.h>
#include <time.h>

#include "draft.h"

typedef enum { UPNP_V1_0, UPNP_V1_1, UPNP_V2_0 } UPNP_VERSION_NUM;

Packet *ForgeSSDP(void *p_arg) {
    char **args = (char **)p_arg;
    Packet *pac = NULL;
    char *ssdp_packet;
    SsdpBinaryRequestHeader *ssdp_header = NULL;

    char request_method[] = "M-SEARCH ";
    char request_uri[] = "* ";
    char request_version[] = "HTTP/1.1\r\n";
    char host1[] = "HOST: ";
    char *host2;
    char host3[] = ":1900\r\n";
    char man[] = "MAN: \"ssdp:discover\"\r\n";
    char mx[] = "MX: 1\r\n";
    char st[] = "ST: ssdp:all\r\n";
    char cpfn_upnp[] = "CPFN.UPNP.ORG: test\r\n";
    char end_request[] = "\r\n";

    int request_method_size;
    int request_uri_size;
    int request_version_size;
    int host_size;
    int man_size;
    int mx_size;
    int st_size;
    int cpfn_upnp_size;
    int end_request_size;

    int packetSize = 0;
    int offset = 0;

    uint8_t upnp_version = UPNP_V1_0;

    if (args[UPNP_VERSION] == NULL || !strcmp(args[UPNP_VERSION], "1.0"))
        upnp_version = UPNP_V1_0;
    else if (!strcmp(args[UPNP_VERSION], "1.1"))
        upnp_version = UPNP_V1_1;
    else if (!strcmp(args[UPNP_VERSION], "2.0"))
        upnp_version = UPNP_V2_0;
    else
        Efatal(ERROR_MIRROR, "Invalid UPnP version. Must be 1.0, 1.1 or 2.0\n");

    if (upnp_version == UPNP_V1_0 && args[UNICAST] != NULL)
        Efatal(ERROR_MIRROR, "UPnP 1.0 only accepts multicast\n");

    if (args[UNICAST] != NULL) {
        host2 = malloc(strlen(args[REFLECTOR_IP]) + 1);
        strcpy(host2, args[REFLECTOR_IP]);
    } else {
        host2 = malloc(16 * sizeof(char));
        strcpy(host2, "239.255.255.250");
    }

    memalloc((void *)&pac, sizeof(Packet));

    ssdp_header = malloc(sizeof(SsdpBinaryRequestHeader));

    request_method_size = strlen(request_method);
    ssdp_header->request_method = malloc(request_method_size + 1);
    strcpy(ssdp_header->request_method, request_method);

    request_uri_size = strlen(request_uri);
    ssdp_header->request_uri = malloc(request_uri_size + 1);
    strcpy(ssdp_header->request_uri, request_uri);

    request_version_size = strlen(request_version);
    ssdp_header->request_version = malloc(request_version_size + 1);
    strcpy(ssdp_header->request_version, request_version);

    host_size = strlen(host1) + strlen(host2) + strlen(host3);
    ssdp_header->host = malloc(host_size + 1);
    strcpy(ssdp_header->host, host1);
    strcat(ssdp_header->host, host2);
    strcat(ssdp_header->host, host3);

    man_size = strlen(man);
    ssdp_header->man = malloc(man_size + 1);
    strcpy(ssdp_header->man, man);

    mx_size = strlen(mx);
    ssdp_header->mx = malloc(mx_size + 1);
    strcpy(ssdp_header->mx, mx);

    st_size = strlen(st);
    ssdp_header->st = malloc(st_size + 1);
    strcpy(ssdp_header->st, st);

    cpfn_upnp_size = strlen(cpfn_upnp);
    ssdp_header->cpfn_upnp = malloc(cpfn_upnp_size + 1);
    strcpy(ssdp_header->cpfn_upnp, cpfn_upnp);

    end_request_size = strlen(end_request);

    packetSize = request_method_size + request_uri_size + request_version_size +
                 host_size + man_size + st_size + end_request_size;

    if (args[UNICAST] == NULL) {
        packetSize += mx_size;
        if (upnp_version == UPNP_V2_0) packetSize += cpfn_upnp_size;
    }

    memalloc((void *)&ssdp_packet, packetSize);

    memmove(ssdp_packet + offset, ssdp_header->request_method,
            request_method_size);
    offset += request_method_size;
    memmove(ssdp_packet + offset, ssdp_header->request_uri, request_uri_size);
    offset += request_uri_size;
    memmove(ssdp_packet + offset, ssdp_header->request_version,
            request_version_size);
    offset += request_version_size;
    memmove(ssdp_packet + offset, ssdp_header->host, host_size);
    offset += host_size;
    memmove(ssdp_packet + offset, ssdp_header->man, man_size);
    offset += man_size;

    if (args[UNICAST] == NULL) {
        memmove(ssdp_packet + offset, ssdp_header->mx, mx_size);
        offset += mx_size;
    }

    memmove(ssdp_packet + offset, ssdp_header->st, st_size);
    offset += st_size;

    if (args[UNICAST] == NULL && upnp_version == UPNP_V2_0) {
        memmove(ssdp_packet + offset, ssdp_header->cpfn_upnp, cpfn_upnp_size);
        offset += cpfn_upnp_size;
    }

    memmove(ssdp_packet + offset, end_request, end_request_size);

    pac->packet_ptr = ssdp_packet;
    pac->pkt_size = packetSize;

    free(host2);
    free(ssdp_header->request_method);
    free(ssdp_header->request_uri);
    free(ssdp_header->request_version);
    free(ssdp_header->host);
    free(ssdp_header->man);
    free(ssdp_header->mx);
    free(ssdp_header->st);
    free(ssdp_header->cpfn_upnp);
    free(ssdp_header);

    return pac;
}
