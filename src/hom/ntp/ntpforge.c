#include "hom/ntp/ntpforge.h"

#include <sys/types.h>

Packet *ForgeNtpBinary(void *p_arg) {
    Packet *pac = NULL;
    char *ntp_packet;
    NtpBinaryRequestHeader *ntp_header = NULL;
    int packetSize = 0;

    memalloc((void *)&pac, sizeof(Packet));

    packetSize = sizeof(NtpBinaryRequestHeader);
    memalloc((void *)&ntp_packet, packetSize);

    ntp_header = (NtpBinaryRequestHeader *)ntp_packet;

    // magic occurs
    ntp_header->rm_vn_mode = 0x17;  // Sets the response bit to 0, More bit to
                                    // 0, Version field to 2, Mode field to 7
    ntp_header->auth_seq = 0x00;    /* key, sequence number */
    ntp_header->implementation = 0x03;  // Sets the implementation to 3
    ntp_header->request = 0x2a;

    pac->packet_ptr = ntp_packet;
    pac->pkt_size = packetSize;

    return pac;
}