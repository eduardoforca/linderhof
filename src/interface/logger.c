#include "interface/logger.h"

#include <math.h>

#include "common/timeHelper.h"
#include "draft.h"
#include "injector/controller.h"
#include "injector/injector.h"
#include "interface/interface.h"

void LogHeader(LhfDraft draft) {
    printf("\n");
    printf("    _     ___ _   _ ____  _____ ____  _   _  ___  _____\n");
    printf("   | |   |_ _| \\ | |  _ \\| ____|  _ \\| | | |/ _ \\|  ___|\n");
    printf("   | |    | ||  \\| | | | |  _| | |_) | |_| | | | | |_   \n");
    printf("   | |___ | || |\\  | |_| | |___|  _ <|  _  | |_| |  _|  \n");
    printf("   |_____|___|_| \\_|____/|_____|_| \\_\\_| |_|\\___/|_|    \n");
    printf("\n");
    printf("#############################################################\n\n");
    printf("\t\t\tAttack plan\n");
    printf("Mirror: %s\n", draft.mirrorName);
    printf("Target IP: %s\n", draft.target_ip);
    printf("Reflector(s) IP: ");
    Print_List(ref_ip_list);
    if (draft.ref_port != 0) {
        printf("Reflector port: %d\n", draft.ref_port);
    }
    if (draft.duration != DEFAULT_DURATION) {
        printf("Attack Duration: %d seconds\n", draft.duration);
    }
    if (draft.flooding_mode) {
        printf("Attack set to flooding mode\n");
    }
    if (!draft.flooding_mode && draft.level > 1) {
        printf("Attack to begin at level %d\n", draft.level);
    }
    if (!draft.flooding_mode && draft.aggressive_mode) {
        printf("Attack set to aggressive mode\n");
    }
    if (!draft.flooding_mode && draft.incAttack) {
        printf("Attack set to be incremental\n");
    }
    printf("\n");
    printf("#############################################################\n\n");
}

void LogInjection(unsigned int p_level, int num_injectors,
                  unsigned int p_probes, Injector **injector, int rate[61],
                  int masterClock) {
    int total = 0;
    int i;
    char date_time[20];
    GetCurrentTimeStr(date_time);

    printf("%s\n", date_time);

    if (p_level > 0 && rate[0] == 0)
        printf("Level: %u -> %.0lf req/s\n\n", p_level, ProbesByLevel(p_level));

    for (i = 0; i < num_injectors; i++) {
        total += injector[i]->net.pktCounter;
        printf("[%d] %s ", i + 1, injector[i]->net.pkt->ip_dest);
        printf("requests sent/expected: %u", injector[i]->net.pktCounter);

        if (rate[0] != 0) {
            printf("/%u\n", injector[i]->net.bucketMax);
            continue;
        }

        if (p_level == 0) {
            printf("\n");
        } else if (p_level == 1) {
            injector[i]->net.pktCounter || injector[i]->net.pktDropped
                ? printf("/1\n")
                : printf("/0\n");
        } else
            printf("/%u\n", injector[i]->net.bucketMax);
    }

    if (rate[0] == 0)
        printf("\nTOTAL: %d/%.0lf req/s\n", total, ProbesByLevel(p_level));
    else
        printf("\nTOTAL: %d/%d req/s\n", total,
               rate[masterClock % rate[0] + 1]);

    printf("-------------------------------------------------------------\n");
}
