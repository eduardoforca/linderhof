#include "interface/configparser.h"

#include <ctype.h>

#include "interface/interface.h"

typedef enum { INTEGER, STRING, BOOL } PARAM_TYPE;

typedef struct {
    char name[MAX_CONFIG_PARAM_LEN];
    char letter;
    uint8_t type;
} params_t;

static params_t params[] = {
    {"MIRROR", ARG_MIRROR, STRING},
    {"TARGET", ARG_TARGET_IP, STRING},
    {"TARGET_PORT", ARG_TARGET_PORT, INTEGER},
    {"REFLECTOR", ARG_REFLECTOR_IP, STRING},
    {"REFLECTOR_PORT", ARG_REFLECTOR_PORT, INTEGER},
    {"LEVEL", ARG_LEVEL, INTEGER},
    {"DURATION", ARG_DURATION, INTEGER},
    {"FLOOD", ARG_FLOOD, BOOL},
    {"INCREMENT", ARG_INCREMENT, INTEGER},
    {"AGGRESSIVE", ARG_AGGRESSIVE, BOOL},
    {"CUSTOM_RATE", ARG_CUSTOM_RATE, STRING},
    {"SCANNER_CIDR", ARG_SCANNER_CIDR, STRING},
    {"SCANNER_PATH", ARG_SCANNER_PATH, STRING},
    {"BENCHMARK", ARG_BENCHMARK, BOOL},
    {"SHUFFLE", ARG_SHUFFLE, BOOL},
    {"DOMAIN_NAME", ARG_DNS_DOMAIN, STRING},
    {"UPNP_VERSION", ARG_SSDP_UPNP_VERSION, STRING},
    {"UNICAST", ARG_SSDP_UNICAST, BOOL},
    {"COMMUNITY_STRING", ARG_SNMP_COMMUNITY_STRING, STRING},
    {"MAX_REPETITIONS", ARG_SNMP_MAX_REPETITIONS, INTEGER},
    {"SZX", ARG_COAP_SZX, INTEGER},
    {"URI_PATH", ARG_COAP_URI_PATH, STRING},
};

/** @brief reads string from the configuration file*/
void read_str_from_config_line(char *config_line, char *val) {
    char *token;

    strtok(config_line, DELIM);
    token = strtok(NULL, DELIM);
    strcpy(val, token);
}

/** @brief reads boolean from the configuration file*/
bool read_bool_from_config_line(char *config_line) {
    char *token;

    strtok(config_line, DELIM);
    token = strtok(NULL, DELIM);

    if (!strcasecmp(token, "true"))
        return true;
    else
        return false;
}

/** @brief removes spaces in text read form file*/
void remove_spaces(char *text) {
    const char *aux = text;

    while (1) {
        while (isspace(*aux)) {
            ++aux;
        }

        if (*aux == '"') {
            aux++;
            do {
                *text++ = *aux++;
            } while (*aux != '"');
            aux++;
            continue;
        }

        if (!(*text++ = *aux++)) return;
    }
}
/** Used on interface.
 *  If file_path == NULL, uses default.
 *  *p_draft doesn't change in this function. Only send to handler.
 */
int ParserConfig(char *file_path, LhfDraft *p_draft, handler_t handler) {
    char buf[MAX_LINE_SIZE];
    char buf_aux[MAX_LINE_SIZE];
    char sval[MAX_CONFIG_VALUE_LEN];
    char *param_name;
    int i, val = 0;
    FILE *fp = NULL;

    if (file_path == NULL) {
        printf("No configuration file provided. Using default file %s\n",
               CONFIGURATION_FILE_NAME);
        file_path = CONFIGURATION_FILE_NAME;
    }

    fp = fopen(file_path, "r+");

    if (fp == NULL) {
        printf("No file found: %s\n", file_path);
        Efatal(ERROR_FILE, "No configuration file found\n");
    }

    while (fgets(buf, MAX_LINE_SIZE, fp) != NULL) {
        if (buf[0] == '\n') continue;

        remove_spaces(buf);

        if (buf[0] == '#' || buf[0] == '[' || strlen(buf) < 3) continue;

        strcpy(buf_aux, buf);
        param_name = strtok(buf_aux, DELIM);

        if (strlen(param_name) + 1 == strlen(buf)) {
            continue;
        }

        for (i = 0; i < lengthof(params); i++) {
            if (!strcmp(param_name, params[i].name)) {
                switch (params[i].type) {
                    case INTEGER:
                    case STRING:
                        read_str_from_config_line(buf, sval);
                        handler(params[i].letter, sval, p_draft);
                        break;
                    case BOOL:
                        val = read_bool_from_config_line(buf);
                        if (val == true)
                            handler(params[i].letter, NULL, p_draft);
                        break;
                }
            }
        }
    }

    fclose(fp);

    return SUCCESS;
}

int ParserConfigGui(char *file_path, handlerGui_t handler) {
    char buf[MAX_LINE_SIZE];
    char buf_aux[MAX_LINE_SIZE];
    char sval[MAX_CONFIG_VALUE_LEN];
    char *param_name;
    int i, val = 0;
    FILE *fp = NULL;

    if (file_path == NULL) {
        printf("No configuration file provided. Using default file %s\n",
               CONFIGURATION_FILE_NAME);
        file_path = CONFIGURATION_FILE_NAME;
    }

    fp = fopen(file_path, "r+");

    if (fp == NULL) {
        printf("No file found: %s\n", file_path);
        return ERROR_FILE;
    }

    while (fgets(buf, MAX_LINE_SIZE, fp) != NULL) {
        if (buf[0] == '\n') continue;

        remove_spaces(buf);

        if (buf[0] == '#' || buf[0] == '[' || strlen(buf) < 3) continue;

        strcpy(buf_aux, buf);
        param_name = strtok(buf_aux, DELIM);

        if (strlen(param_name) + 1 == strlen(buf)) {
            continue;
        }

        for (i = 0; i < lengthof(params); i++) {
            if (!strcmp(param_name, params[i].name)) {
                switch (params[i].type) {
                    case INTEGER:
                    case STRING:
                        read_str_from_config_line(buf, sval);
                        handler(params[i].letter, sval);
                        break;
                    case BOOL:
                        val = read_bool_from_config_line(buf);
                        if (val == true)
                            handler(params[i].letter, NULL);
                        break;
                }
            }
        }
    }

    fclose(fp);

    return SUCCESS;
}
