#include <fcntl.h>
#include <gtk/gtk.h>

#include "interface/configparser.h"
#include "interface/interface.h"

// Make them global

GtkWidget *window;
GtkBuilder *builder;

GtkWidget *cb_rate;
GtkWidget *cb_aggressive;
GtkWidget *cb_duration;
GtkWidget *cb_flood;
GtkWidget *cb_increment;
GtkWidget *cb_shuffle;
GtkWidget *cb_ssdp_unicast;
GtkWidget *cbx_coap_szx;
GtkWidget *cbx_ssdp_upnp;
GtkWidget *entry_rates_file;
GtkWidget *entry_coap_uri_path;
GtkWidget *entry_dns_domain;
GtkWidget *entry_reflector_ip;
GtkWidget *entry_reflector_port;
GtkWidget *entry_snmp_community_string;
GtkWidget *entry_target_ip;
GtkWidget *entry_target_port;
GtkWidget *rb_attack;
GtkWidget *rb_scan;
GtkWidget *rb_benchmark;
GtkWidget *rb_cldap;
GtkWidget *rb_coap;
GtkWidget *rb_dns;
GtkWidget *rb_memcached;
GtkWidget *rb_ntp;
GtkWidget *rb_snmp;
GtkWidget *rb_ssdp;
GtkWidget *spin_duration;
GtkWidget *spin_increment;
GtkWidget *spin_level;
GtkWidget *spin_snmp_max_repetitions;
GtkWidget *rate_file_chooser;
GtkWidget *open_rates_file_button;
GtkWidget *entry_device_ip;
GtkWidget *open_devices_file;
GtkWidget *entry_device_port;
GtkWidget *entry_cidr;
GtkWidget *entry_scan_port;
GtkWidget *entry_output_file;
GtkWidget *open_output_file_button;


GtkWidget *button_start;
GtkWidget *dialog_file_chooser;
GtkWidget *menuitem_about;
GtkWidget *menuitem_new;
GtkWidget *menuitem_open;
GtkWidget *menuitem_save_as;
GtkWidget *menuitem_save;
GtkWidget *page_output;
GtkWidget *page_setup;
GtkWidget *progress_bar;
GtkWidget *result_text_view;
GtkWidget *stack_main;
GtkWidget *stk_mirror_configuration;
GtkWidget *stk_action;
GtkWidget *dialog_about;
GtkWidget *graph_packets;
GtkWidget *result_level;
GtkWidget *result_packets;
GtkWidget *result_total;

char executable[10];

typedef struct _Timer {
    gint max;
    gint progress;
} Timer;

Timer timer;

gdouble CHART_LENGHT = 20;
gdouble CHART_HEIGHT = 1;
int values[100];

int child_pid = 0;

int stopRunning() {
    if(child_pid == 0) return 0;

    kill(child_pid, SIGKILL);
    child_pid = 0;
    gtk_button_set_label(GTK_BUTTON(button_start), "Start");

    return 0;
}

gboolean update_progress_bar() {
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress_bar),
                                  (gfloat)++timer.progress / timer.max);
    return timer.progress < timer.max;
}

gboolean on_graph_packets_scroll_event(GtkWidget *widget, GdkEvent *event,
                                       gpointer data) {
    GdkEventScroll *scroll_event;
    scroll_event = (GdkEventScroll *)event;

    switch (scroll_event->direction) {
        case GDK_SCROLL_UP:
            if (CHART_LENGHT >= 10) CHART_LENGHT -= 5;
            break;

        case GDK_SCROLL_DOWN:
            if (CHART_LENGHT <= 95) CHART_LENGHT += 5;
            break;

        default:
            return FALSE;
            break;
    }

    gtk_widget_queue_draw(graph_packets);

    return FALSE;
}

gboolean on_graph_packets_draw(GtkWidget *widget, cairo_t *cr, gpointer data) {
    guint width, height, i;
    gdouble value_height, step_size;
    // const double dashes[] = {2.0};
    gchar axis_label[10];

    width = gtk_widget_get_allocated_width(widget);
    height = gtk_widget_get_allocated_height(widget);
    step_size = ((gdouble)width) / (CHART_LENGHT - 1);

    // horizontal lines and axis label
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.3);
    cairo_set_line_width(cr, 0.25);

    for (i = 0; i < 5; i++) {
        gdouble h =
            height - height / (CHART_HEIGHT * 1.1) * (i * CHART_HEIGHT / 4);
        sprintf(axis_label, "%.1f", i * (CHART_HEIGHT / 4));
        cairo_move_to(cr, 0, h - 3);  // -3 to be above dashed line
        cairo_show_text(cr, axis_label);
        cairo_move_to(cr, 0, h);
        if (i > 0)  // not draw dashed line at bottom
            cairo_line_to(cr, width - 1, h);
    }
    cairo_stroke(cr);

    // vertical lines
    cairo_set_line_width(cr, 0.15);
    for (gdouble j = width; j >= step_size; j -= step_size) {
        cairo_move_to(cr, j, height);
        cairo_line_to(cr, j, 0);
    }
    cairo_stroke(cr);

    cairo_set_source_rgba(cr, 1.0, 0.43, 0.0, 1.0);
    cairo_set_line_width(cr, 1.0);
    cairo_set_dash(cr, NULL, 0, 0);

    value_height = (1.0 - (gdouble)values[0] / (CHART_HEIGHT * 1.1)) * height;
    cairo_move_to(cr, width, value_height);
    for (i = 1; i < CHART_LENGHT; i++) {
        cairo_translate(cr, -step_size, 0);
        value_height =
            (1.0 - (gdouble)values[i] / (CHART_HEIGHT * 1.1)) * height;
        cairo_line_to(cr, width, value_height);
    }

    cairo_stroke_preserve(cr);

    cairo_set_source_rgba(cr, 1.0, 0.43, 0.0, 0.3);
    cairo_set_line_width(cr, 0.0);
    cairo_line_to(cr, width, height);

    cairo_translate(cr, step_size * (CHART_LENGHT - 1), 0);
    cairo_line_to(cr, width, height);
    cairo_close_path(cr);

    cairo_fill(cr);

    return FALSE;
}

void on_rb_action_toggled(GtkWidget *button) {
    const char *button_label;

    if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))) return;

    button_label = gtk_button_get_label(GTK_BUTTON(button));

    if (strcmp(button_label, "Scan") == 0) {
        gtk_stack_set_visible_child_name(GTK_STACK(stk_action), "page_scan");
    } else if (strcmp(button_label, "Benchmark") == 0) {
        gtk_stack_set_visible_child_name(GTK_STACK(stk_action), "page_benchmark");
    } else {
        gtk_stack_set_visible_child_name(GTK_STACK(stk_action), "page_attack");
    }
}

void on_rb_mirror_toggled(GtkWidget *button) {
    const char *button_label;

    if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))) return;

    button_label = gtk_button_get_label(GTK_BUTTON(button));

    if (strcmp(button_label, "COAP") == 0)
        gtk_stack_set_visible_child_name(GTK_STACK(stk_mirror_configuration),
                                         "page_coap");
    else if (strcmp(button_label, "DNS") == 0)
        gtk_stack_set_visible_child_name(GTK_STACK(stk_mirror_configuration),
                                         "page_dns");
    else if (strcmp(button_label, "SNMP") == 0)
        gtk_stack_set_visible_child_name(GTK_STACK(stk_mirror_configuration),
                                         "page_snmp");
    else if (strcmp(button_label, "SSDP") == 0)
        gtk_stack_set_visible_child_name(GTK_STACK(stk_mirror_configuration),
                                         "page_ssdp");
    else
        gtk_stack_set_visible_child_name(GTK_STACK(stk_mirror_configuration),
                                         "page_empty");
}

void on_cb_rate_toggled(GtkCheckButton *b) {
    gtk_widget_set_sensitive(
        entry_rates_file, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(b)));
    gtk_widget_set_sensitive(
        open_rates_file_button,
        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(b)));
    gtk_widget_set_sensitive(
        spin_level, !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(b)));
}

void on_cb_duration_toggled(GtkCheckButton *b) {
    gtk_widget_set_sensitive(
        spin_duration, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(b)));
}

void on_cb_increment_toggled(GtkCheckButton *b) {
    gtk_widget_set_sensitive(
        spin_increment, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(b)));
}

static gboolean io_dialog(GIOChannel *channel, GIOCondition condition,
                          gpointer data) {
    gchar *text, *text_utf8;
    gsize len;
    gsize bytes_written;

    int packets = 0;
    int packets_max = 0;
    static int packets_total = 0;
    static int level = 0;

    gchar result_level_str[10];
    gchar result_packets_str[30];
    gchar result_total_str[20];

    g_return_val_if_fail(channel != NULL, FALSE);

    if (condition & G_IO_IN) {
        g_io_channel_read_line(channel, &text, &len, NULL, NULL);
        text_utf8 = g_locale_to_utf8(text, len, NULL, &bytes_written, NULL);

        if (strncmp("###", text_utf8, 3) == 0) {
            packets_total = 0;
            level = 0;
            gtk_label_set_text(GTK_LABEL(result_level), "");
            gtk_label_set_text(GTK_LABEL(result_packets), "");
            gtk_label_set_text(GTK_LABEL(result_total), "");
        }

        if (strncmp("Level:", text_utf8, 6) == 0) {
            sscanf(text_utf8, "Level: %d", &level);
        }

        if (strncmp("TOTAL:", text_utf8, 6) == 0) {
            sscanf(text_utf8, "TOTAL: %d/%d req/s", &packets, &packets_max);

            packets_total += packets;

            for (int i = 99; i > 0; i--) values[i] = values[i - 1];
            values[0] = packets;

            if (packets > CHART_HEIGHT) CHART_HEIGHT = packets;

            sprintf(result_level_str, "Level: %d", level);
            sprintf(result_packets_str, "%d/%d pps", packets, packets_max);
            sprintf(result_total_str, "Total: %d packets", packets_total);
            gtk_label_set_text(GTK_LABEL(result_level), result_level_str);
            gtk_label_set_text(GTK_LABEL(result_packets), result_packets_str);
            gtk_label_set_text(GTK_LABEL(result_total), result_total_str);

            gtk_widget_queue_draw(graph_packets);
            update_progress_bar();
        }

        GtkTextIter end;
        GtkTextBuffer *tb;
        GtkTextMark *mark = NULL;
        tb = gtk_text_view_get_buffer(GTK_TEXT_VIEW(result_text_view));
        gtk_text_buffer_get_end_iter(tb, &end);
        gtk_text_buffer_insert(tb, &end, text_utf8, bytes_written);

        GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment(
            GTK_SCROLLED_WINDOW(page_output));
        if (gtk_adjustment_get_value(adjustment) >=
            gtk_adjustment_get_upper(adjustment) -
                gtk_adjustment_get_page_size(adjustment) - 1e-12) {
            mark = gtk_text_buffer_create_mark(tb, NULL, &end, FALSE);
            gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(result_text_view),
                                               mark);
        }

        g_free(text);
        g_free(text_utf8);
        return TRUE;
    }

    gtk_button_set_label(GTK_BUTTON(button_start), "Start");
    return FALSE;
}

int clear_fields() {
    gtk_entry_set_text(GTK_ENTRY(entry_target_ip), "");
    gtk_entry_set_text(GTK_ENTRY(entry_reflector_ip), "");
    gtk_entry_set_text(GTK_ENTRY(entry_target_port), "");
    gtk_entry_set_text(GTK_ENTRY(entry_reflector_port), "");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_level), 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_rate), FALSE);
    gtk_entry_set_text(GTK_ENTRY(entry_rates_file), "");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_duration), FALSE);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_duration), 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_flood), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_increment), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_shuffle), FALSE);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_increment), 1);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_aggressive), FALSE);
    gtk_entry_set_text(GTK_ENTRY(entry_dns_domain), "");
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(cbx_ssdp_upnp), "1.1");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_ssdp_unicast), FALSE);
    gtk_entry_set_text(GTK_ENTRY(entry_snmp_community_string), "");
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_snmp_max_repetitions), 1000);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(cbx_coap_szx), "0");
    gtk_entry_set_text(GTK_ENTRY(entry_coap_uri_path), "");
    gtk_entry_set_text(GTK_ENTRY(entry_cidr), "");
    gtk_entry_set_text(GTK_ENTRY(entry_scan_port), "");
    gtk_entry_set_text(GTK_ENTRY(entry_output_file), "");
    gtk_entry_set_text(GTK_ENTRY(entry_device_ip), "");
    gtk_entry_set_text(GTK_ENTRY(entry_device_port), "");
    return 0;
}

int fill_fields(char key, char *arg) {
    switch (key) {
        case ARG_MIRROR:
            if (!strcasecmp(arg, "cldap")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_cldap), TRUE);
            } else if (!strcasecmp(arg, "coap")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_coap), TRUE);
            } else if (!strcasecmp(arg, "dns")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_dns), TRUE);
            } else if (!strcasecmp(arg, "memcached_getset")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_memcached),
                                             TRUE);
            } else if (!strcasecmp(arg, "memcached_stat")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_memcached),
                                             TRUE);
            } else if (!strcasecmp(arg, "ntp")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_ntp), TRUE);
            } else if (!strcasecmp(arg, "snmp")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_snmp), TRUE);
            } else if (!strcasecmp(arg, "ssdp")) {
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb_ssdp), TRUE);
            } else {
                Efatal(ERROR_INTERFACE, "Invalid input for option mirror (-m)");
            }
            break;

        case ARG_TARGET_IP:
            gtk_entry_set_text(GTK_ENTRY(entry_target_ip), arg);
            break;

        case ARG_REFLECTOR_IP:
            gtk_entry_set_text(GTK_ENTRY(entry_reflector_ip), arg);
            gtk_entry_set_text(GTK_ENTRY(entry_device_ip), arg);
            break;

        case ARG_TARGET_PORT:
            gtk_entry_set_text(GTK_ENTRY(entry_target_port), arg);
            break;

        case ARG_REFLECTOR_PORT:
            gtk_entry_set_text(GTK_ENTRY(entry_reflector_port), arg);
            gtk_entry_set_text(GTK_ENTRY(entry_device_port), arg);
            break;

        case ARG_LEVEL:
            gtk_spin_button_set_value(
                GTK_SPIN_BUTTON(spin_level),
                (strtol(arg, NULL, 10) > 0) ? strtol(arg, NULL, 10) : 1);
            break;

        case ARG_CUSTOM_RATE:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_rate), TRUE);
            gtk_entry_set_text(GTK_ENTRY(entry_rates_file), arg);
            break;

        case ARG_DURATION:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_duration), TRUE);
            gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_duration),
                                      (strtol(arg, NULL, 10) > 0)
                                          ? strtol(arg, NULL, 10)
                                          : DEFAULT_DURATION);
            break;

        case ARG_FLOOD:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_flood), TRUE);
            break;

        case ARG_INCREMENT:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_increment), TRUE);
            gtk_spin_button_set_value(
                GTK_SPIN_BUTTON(spin_increment),
                (strtol(arg, NULL, 10) > 0) ? strtol(arg, NULL, 10) : 0);
            break;

        case ARG_SHUFFLE:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_shuffle), TRUE);
            break;

        case ARG_AGGRESSIVE:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_aggressive),
                                         TRUE);
            break;

        case ARG_SCANNER_CIDR:
            gtk_entry_set_text(GTK_ENTRY(entry_cidr), arg);
            break;

        case ARG_SCANNER_PATH:
            gtk_entry_set_text(GTK_ENTRY(entry_output_file), arg);
            break;

        case ARG_CONFIG:
            break;

        case ARG_DNS_DOMAIN:
            gtk_entry_set_text(GTK_ENTRY(entry_dns_domain), arg);
            break;

        case ARG_SSDP_UPNP_VERSION:
            gtk_combo_box_set_active_id(GTK_COMBO_BOX(cbx_ssdp_upnp), arg);
            break;

        case ARG_SSDP_UNICAST:
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_ssdp_unicast),
                                         TRUE);
            break;

        case ARG_SNMP_COMMUNITY_STRING:
            gtk_entry_set_text(GTK_ENTRY(entry_snmp_community_string), arg);
            break;

        case ARG_SNMP_MAX_REPETITIONS:
            gtk_spin_button_set_value(
                GTK_SPIN_BUTTON(spin_snmp_max_repetitions),
                (strtol(arg, NULL, 10) > 0) ? strtol(arg, NULL, 10) : 1000);
            break;

        case ARG_COAP_SZX:
            gtk_combo_box_set_active_id(GTK_COMBO_BOX(cbx_coap_szx), arg);
            break;

        case ARG_COAP_URI_PATH:
            gtk_entry_set_text(GTK_ENTRY(entry_coap_uri_path), arg);
            break;

        default:
            break;
    }

    return 0;
}


void start_attack() {
    gint fd_output;
    gchar *cmd[30] = {NULL};
    gchar *mirror;
    gint duration_max = 0;
    int i = 0;

    cmd[i++] = strdup(executable);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_cldap)) == TRUE) {
        mirror = strdup("cldap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_coap)) ==
               TRUE) {
        mirror = strdup("coap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_dns)) ==
               TRUE) {
        mirror = strdup("dns");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_memcached)) ==
               TRUE) {
        mirror = strdup("memcached");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ntp)) ==
               TRUE) {
        mirror = strdup("ntp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_snmp)) ==
               TRUE) {
        mirror = strdup("snmp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ssdp)) ==
               TRUE) {
        mirror = strdup("ssdp");
    } else {
        return;
    }

    cmd[i++] = strdup("-m");
    cmd[i++] = mirror;
    cmd[i++] = strdup("-t");
    cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_target_ip)));
    cmd[i++] = strdup("-r");
    cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_reflector_ip)));

    gchar *target_port =
        strdup(gtk_entry_get_text(GTK_ENTRY(entry_target_port)));
    if (strlen(target_port) > 0) {
        cmd[i++] = "-g";
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_target_port)));
    }

    gchar *reflector_port =
        strdup(gtk_entry_get_text(GTK_ENTRY(entry_reflector_port)));
    if (strlen(reflector_port) > 0) {
        cmd[i++] = "-p";
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_reflector_port)));
    }

    cmd[i++] = "-l";
    gchar level[3];
    sprintf(level, "%d",
            (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_level)));
    cmd[i++] = strdup(level);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_rate)) == TRUE) {
        gchar *rate_file =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_rates_file)));
        if (strlen(rate_file) > 0) {
            cmd[i++] = "-e";
            cmd[i++] = rate_file;
        }
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_duration)) == TRUE) {
        cmd[i++] = "-d";
        duration_max =
            (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_duration));
        gchar duration[10];
        sprintf(duration, "%d", duration_max);
        cmd[i++] = strdup(duration);
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_flood)) == TRUE) {
        cmd[i++] = "-f";
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_increment)) == TRUE) {
        cmd[i++] = "-i";
        gchar increment[10];
        sprintf(
            increment, "%d",
            (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_increment)));
        cmd[i++] = strdup(increment);
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_shuffle)) == TRUE) {
        cmd[i++] = "-o";
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_aggressive)) ==
        TRUE) {
        cmd[i++] = "-a";
    }

    if (!strcmp(mirror, "dns")) {
        gchar *dns_domain =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        if (strlen(dns_domain) > 0) {
            cmd[i++] = "-D";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
    }

    if (!strcmp(mirror, "ssdp")) {
        cmd[i++] = "-V";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_ssdp_upnp)));
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_ssdp_unicast)) ==
            TRUE) {
            cmd[i++] = "-U";
        }
    }

    if (!strcmp(mirror, "snmp")) {
        gchar *community_string =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_snmp_community_string)));
        if (strlen(community_string) > 0) {
            cmd[i++] = "-C";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
        cmd[i++] = "-R";
        gchar max_repetitions[10];
        sprintf(max_repetitions, "%d",
                (gint)gtk_spin_button_get_value(
                    GTK_SPIN_BUTTON(spin_snmp_max_repetitions)));
        cmd[i++] = strdup(max_repetitions);
    }

    if (!strcmp(mirror, "coap")) {
        gchar *uri_path =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        if (strlen(uri_path) > 0) {
            cmd[i++] = "-P";
            cmd[i++] =
                strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        }
        cmd[i++] = "-Z";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_coap_szx)));
    }

    cmd[i] = NULL;

    // print execution command
    for (i = 0; i < 30 && cmd[i] != NULL; i++) {
        printf("%s ", cmd[i]);
    }
    printf("\n");

    for (int i = 0; i < 100; i++) values[i] = 0;
    CHART_HEIGHT = 1;
    gtk_widget_queue_draw(graph_packets);

    if (g_spawn_async_with_pipes(NULL, cmd, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
                                 NULL, NULL, &child_pid, NULL, &fd_output, NULL,
                                 NULL)) {
        GIOChannel *channel = g_io_channel_unix_new(fd_output);
        g_io_add_watch(channel, G_IO_IN | G_IO_HUP, io_dialog, NULL);
        g_io_channel_unref(channel);
    } else {
        exit(1);
    }

    timer.max = duration_max;
    timer.progress = 0;
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress_bar), 0);

    gtk_button_set_label(GTK_BUTTON(button_start), "Stop");    

    if (!strcmp(gtk_stack_get_visible_child_name(GTK_STACK(stack_main)),
                "page_setup"))
        gtk_stack_set_visible_child(GTK_STACK(stack_main), page_output);
}

void start_scan() {
    gint fd_output;
    gchar *cmd[30] = {NULL};
    gchar *mirror;
    int i = 0;

    cmd[i++] = strdup(executable);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_cldap)) == TRUE) {
        mirror = strdup("cldap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_coap)) ==
               TRUE) {
        mirror = strdup("coap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_dns)) ==
               TRUE) {
        mirror = strdup("dns");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_memcached)) ==
               TRUE) {
        mirror = strdup("memcached");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ntp)) ==
               TRUE) {
        mirror = strdup("ntp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_snmp)) ==
               TRUE) {
        mirror = strdup("snmp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ssdp)) ==
               TRUE) {
        mirror = strdup("ssdp");
    } else {
        return;
    }

    cmd[i++] = strdup("-m");
    cmd[i++] = mirror;

    cmd[i++] = strdup("-s");
    cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_cidr)));

    gchar *scan_port = strdup(gtk_entry_get_text(GTK_ENTRY(entry_scan_port)));
    if (strlen(scan_port) > 0) {
        cmd[i++] = "-p";
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_scan_port)));
    }

    gchar *output_path = strdup(gtk_entry_get_text(GTK_ENTRY(entry_output_file)));
    if (strlen(output_path) > 0) {
        cmd[i++] = strdup("-n");
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_output_file)));
    }

    gchar *reflector_port =
        strdup(gtk_entry_get_text(GTK_ENTRY(entry_reflector_port)));
    if (strlen(reflector_port) > 0) {
        cmd[i++] = "-p";
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_reflector_port)));
    }

    if (!strcmp(mirror, "dns")) {
        gchar *dns_domain =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        if (strlen(dns_domain) > 0) {
            cmd[i++] = "-D";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
    }

    if (!strcmp(mirror, "ssdp")) {
        cmd[i++] = "-V";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_ssdp_upnp)));
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_ssdp_unicast)) ==
            TRUE) {
            cmd[i++] = "-U";
        }
    }

    if (!strcmp(mirror, "snmp")) {
        gchar *community_string =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_snmp_community_string)));
        if (strlen(community_string) > 0) {
            cmd[i++] = "-C";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
        cmd[i++] = "-R";
        gchar max_repetitions[10];
        sprintf(max_repetitions, "%d",
                (gint)gtk_spin_button_get_value(
                    GTK_SPIN_BUTTON(spin_snmp_max_repetitions)));
        cmd[i++] = strdup(max_repetitions);
    }

    if (!strcmp(mirror, "coap")) {
        gchar *uri_path =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        if (strlen(uri_path) > 0) {
            cmd[i++] = "-P";
            cmd[i++] =
                strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        }
        cmd[i++] = "-Z";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_coap_szx)));
    }

    cmd[i] = NULL;

    // print execution command
    for (i = 0; i < 30 && cmd[i] != NULL; i++) {
        printf("%s ", cmd[i]);
    }
    printf("\n");

    if (g_spawn_async_with_pipes(NULL, cmd, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
                                 NULL, NULL, &child_pid, NULL, &fd_output, NULL,
                                 NULL)) {
        GIOChannel *channel = g_io_channel_unix_new(fd_output);
        g_io_add_watch(channel, G_IO_IN | G_IO_HUP, io_dialog, NULL);
        g_io_channel_unref(channel);
    } else {
        exit(1);
    }

    gtk_button_set_label(GTK_BUTTON(button_start), "Stop");    

    if (!strcmp(gtk_stack_get_visible_child_name(GTK_STACK(stack_main)), "page_setup"))
        gtk_stack_set_visible_child(GTK_STACK(stack_main), page_output);
}

void start_benchmark() {
    gint fd_output;
    gchar *cmd[30] = {NULL};
    gchar *mirror;
    int i = 0;

    cmd[i++] = strdup(executable);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_cldap)) == TRUE) {
        mirror = strdup("cldap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_coap)) ==
               TRUE) {
        mirror = strdup("coap");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_dns)) ==
               TRUE) {
        mirror = strdup("dns");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_memcached)) ==
               TRUE) {
        mirror = strdup("memcached");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ntp)) ==
               TRUE) {
        mirror = strdup("ntp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_snmp)) ==
               TRUE) {
        mirror = strdup("snmp");
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_ssdp)) ==
               TRUE) {
        mirror = strdup("ssdp");
    } else {
        return;
    }

    cmd[i++] = strdup("-m");
    cmd[i++] = mirror;

    cmd[i++] = strdup("-r");
    cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_device_ip)));

    gchar *device_port = strdup(gtk_entry_get_text(GTK_ENTRY(entry_device_port)));
    if (strlen(device_port) > 0) {
        cmd[i++] = "-p";
        cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_device_port)));
    }

    if (!strcmp(mirror, "dns")) {
        gchar *dns_domain =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        if (strlen(dns_domain) > 0) {
            cmd[i++] = "-D";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
    }

    if (!strcmp(mirror, "ssdp")) {
        cmd[i++] = "-V";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_ssdp_upnp)));
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_ssdp_unicast)) ==
            TRUE) {
            cmd[i++] = "-U";
        }
    }

    if (!strcmp(mirror, "snmp")) {
        gchar *community_string =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_snmp_community_string)));
        if (strlen(community_string) > 0) {
            cmd[i++] = "-C";
            cmd[i++] = strdup(gtk_entry_get_text(GTK_ENTRY(entry_dns_domain)));
        }
        cmd[i++] = "-R";
        gchar max_repetitions[10];
        sprintf(max_repetitions, "%d",
                (gint)gtk_spin_button_get_value(
                    GTK_SPIN_BUTTON(spin_snmp_max_repetitions)));
        cmd[i++] = strdup(max_repetitions);
    }

    if (!strcmp(mirror, "coap")) {
        gchar *uri_path =
            strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        if (strlen(uri_path) > 0) {
            cmd[i++] = "-P";
            cmd[i++] =
                strdup(gtk_entry_get_text(GTK_ENTRY(entry_coap_uri_path)));
        }
        cmd[i++] = "-Z";
        cmd[i++] =
            strdup(gtk_combo_box_get_active_id(GTK_COMBO_BOX(cbx_coap_szx)));
    }

    cmd[i++] = strdup("-b");
    cmd[i] = NULL;

    // print execution command
    for (i = 0; i < 30 && cmd[i] != NULL; i++) {
        printf("%s ", cmd[i]);
    }
    printf("\n");

    if (g_spawn_async_with_pipes(NULL, cmd, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
                                 NULL, NULL, &child_pid, NULL, &fd_output, NULL,
                                 NULL)) {
        GIOChannel *channel = g_io_channel_unix_new(fd_output);
        g_io_add_watch(channel, G_IO_IN | G_IO_HUP, io_dialog, NULL);
        g_io_channel_unref(channel);
    } else {
        exit(1);
    }

    gtk_button_set_label(GTK_BUTTON(button_start), "Stop");    

    if (!strcmp(gtk_stack_get_visible_child_name(GTK_STACK(stack_main)), "page_setup"))
        gtk_stack_set_visible_child(GTK_STACK(stack_main), page_output);
}


void on_start_clicked(GtkButton *b) {

    if(child_pid != 0) {
        stopRunning();
        return;
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_attack)) == TRUE) {
        start_attack();
        return;
    }
    
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_scan)) == TRUE) {
        start_scan();
        return;
    }
    
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_benchmark)) == TRUE) {
        start_benchmark();
        return;
    }
}

void on_menuitem_new_activate(GtkMenuItem *menu_item) { clear_fields(); }

void on_menuitem_save_activate(GtkMenuItem *menu_item) { return; }
void on_menuitem_save_as_activate(GtkMenuItem *menu_item) { return; }

void on_menuitem_about_activate(GtkMenuItem *menu_item) {
    gtk_widget_show(dialog_about);
}

void on_menuitem_quit_activate(GtkMenuItem *menu_item) { gtk_main_quit(); }

void on_window_main_destroy() {
    printf("on_window_main_destroy");
    gtk_main_quit();
}

void load_config(gchar *filename) {
    clear_fields();
    ParserConfigGui(filename, fill_fields);
}

void on_menuitem_open_activate(GtkMenuItem *menu_item) {
    gchar *filename = NULL;

    gtk_widget_show(dialog_file_chooser);

    if (gtk_dialog_run(GTK_DIALOG(dialog_file_chooser)) == GTK_RESPONSE_OK) {
        filename = gtk_file_chooser_get_filename(
            GTK_FILE_CHOOSER(dialog_file_chooser));
        load_config(filename);
        g_free(filename);
    }

    gtk_widget_hide(dialog_file_chooser);
}

void on_open_reflectors_file_clicked(GtkButton *b) {
    gchar *filename = NULL;

    gtk_widget_show(dialog_file_chooser);

    if (gtk_dialog_run(GTK_DIALOG(dialog_file_chooser)) == GTK_RESPONSE_OK) {
        filename = gtk_file_chooser_get_filename(
            GTK_FILE_CHOOSER(dialog_file_chooser));
        gtk_entry_set_text(GTK_ENTRY(entry_reflector_ip), filename);
        g_free(filename);
    }

    gtk_widget_hide(dialog_file_chooser);
}

void on_open_rates_file_button_clicked(GtkButton *b) {
    gchar *filename = NULL;

    gtk_widget_show(dialog_file_chooser);

    if (gtk_dialog_run(GTK_DIALOG(dialog_file_chooser)) == GTK_RESPONSE_OK) {
        filename = gtk_file_chooser_get_filename(
            GTK_FILE_CHOOSER(dialog_file_chooser));
        gtk_entry_set_text(GTK_ENTRY(entry_rates_file), filename);
        g_free(filename);
    }

    gtk_widget_hide(dialog_file_chooser);
}

void on_open_output_file_button_clicked(GtkButton *b) {
    gchar *filename = NULL;

    gtk_widget_show(dialog_file_chooser);

    if (gtk_dialog_run(GTK_DIALOG(dialog_file_chooser)) == GTK_RESPONSE_OK) {
        filename = gtk_file_chooser_get_filename(
            GTK_FILE_CHOOSER(dialog_file_chooser));
        gtk_entry_set_text(GTK_ENTRY(entry_output_file), filename);
        g_free(filename);
    }

    gtk_widget_hide(dialog_file_chooser);
}

void on_open_devices_file_clicked(GtkButton *b) {
    gchar *filename = NULL;

    gtk_widget_show(dialog_file_chooser);

    if (gtk_dialog_run(GTK_DIALOG(dialog_file_chooser)) == GTK_RESPONSE_OK) {
        filename = gtk_file_chooser_get_filename(
            GTK_FILE_CHOOSER(dialog_file_chooser));
        gtk_entry_set_text(GTK_ENTRY(entry_device_ip), filename);
        g_free(filename);
    }

    gtk_widget_hide(dialog_file_chooser);
}

int run_gui(int argc, char *argv[]) {
    gtk_init(&argc, &argv);
    strcpy(executable, argv[0]);

    // establish contact with xml code used to adjust widget settings
    builder = gtk_builder_new_from_file("gui.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    gtk_window_set_title(GTK_WINDOW(window), "Linderhof");

    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    gtk_builder_connect_signals(builder, NULL);

    cb_aggressive =
        GTK_WIDGET(gtk_builder_get_object(builder, "cb_aggressive"));
    cb_duration = GTK_WIDGET(gtk_builder_get_object(builder, "cb_duration"));
    cb_flood = GTK_WIDGET(gtk_builder_get_object(builder, "cb_flood"));
    cb_increment = GTK_WIDGET(gtk_builder_get_object(builder, "cb_increment"));
    cb_shuffle = GTK_WIDGET(gtk_builder_get_object(builder, "cb_shuffle"));
    cb_ssdp_unicast =
        GTK_WIDGET(gtk_builder_get_object(builder, "cb_ssdp_unicast"));
    cb_rate = GTK_WIDGET(gtk_builder_get_object(builder, "cb_rate"));
    entry_rates_file =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_rates_file"));
    cbx_coap_szx = GTK_WIDGET(gtk_builder_get_object(builder, "cbx_coap_szx"));
    cbx_ssdp_upnp =
        GTK_WIDGET(gtk_builder_get_object(builder, "cbx_ssdp_upnp"));
    entry_coap_uri_path =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_coap_uri_path"));
    entry_dns_domain =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_dns_domain"));
    entry_reflector_ip =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_reflector_ip"));
    entry_reflector_port =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_reflector_port"));
    entry_snmp_community_string = GTK_WIDGET(
        gtk_builder_get_object(builder, "entry_snmp_community_string"));
    entry_target_ip =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_target_ip"));
    entry_target_port =
        GTK_WIDGET(gtk_builder_get_object(builder, "entry_target_port"));
    rb_attack = GTK_WIDGET(gtk_builder_get_object(builder, "rb_attack"));
    rb_scan = GTK_WIDGET(gtk_builder_get_object(builder, "rb_scan"));
    rb_benchmark = GTK_WIDGET(gtk_builder_get_object(builder, "rb_benchmark"));
    rb_cldap = GTK_WIDGET(gtk_builder_get_object(builder, "rb_cldap"));
    rb_coap = GTK_WIDGET(gtk_builder_get_object(builder, "rb_coap"));
    rb_dns = GTK_WIDGET(gtk_builder_get_object(builder, "rb_dns"));
    rb_memcached = GTK_WIDGET(gtk_builder_get_object(builder, "rb_memcached"));
    rb_ntp = GTK_WIDGET(gtk_builder_get_object(builder, "rb_ntp"));
    rb_snmp = GTK_WIDGET(gtk_builder_get_object(builder, "rb_snmp"));
    rb_ssdp = GTK_WIDGET(gtk_builder_get_object(builder, "rb_ssdp"));
    spin_duration =
        GTK_WIDGET(gtk_builder_get_object(builder, "spin_duration"));
    spin_increment =
        GTK_WIDGET(gtk_builder_get_object(builder, "spin_increment"));
    spin_level = GTK_WIDGET(gtk_builder_get_object(builder, "spin_level"));
    spin_snmp_max_repetitions = GTK_WIDGET(
        gtk_builder_get_object(builder, "spin_snmp_max_repetitions"));
    open_rates_file_button =
        GTK_WIDGET(gtk_builder_get_object(builder, "open_rates_file_button"));
    button_start = GTK_WIDGET(gtk_builder_get_object(builder, "button_start"));
    dialog_about = GTK_WIDGET(gtk_builder_get_object(builder, "dialog_about"));
    dialog_file_chooser =
        GTK_WIDGET(gtk_builder_get_object(builder, "dialog_file_chooser"));
    menuitem_about =
        GTK_WIDGET(gtk_builder_get_object(builder, "menuitem_about"));
    menuitem_new = GTK_WIDGET(gtk_builder_get_object(builder, "menuitem_new"));
    menuitem_open =
        GTK_WIDGET(gtk_builder_get_object(builder, "menuitem_open"));
    menuitem_save =
        GTK_WIDGET(gtk_builder_get_object(builder, "menuitem_save"));
    menuitem_save_as =
        GTK_WIDGET(gtk_builder_get_object(builder, "menuitem_save_as"));
    page_output = GTK_WIDGET(gtk_builder_get_object(builder, "page_output"));
    page_setup = GTK_WIDGET(gtk_builder_get_object(builder, "page_setup"));
    progress_bar = GTK_WIDGET(gtk_builder_get_object(builder, "progress_bar"));
    result_text_view =
        GTK_WIDGET(gtk_builder_get_object(builder, "result_text_view"));
    stack_main = GTK_WIDGET(gtk_builder_get_object(builder, "stack_main"));
    stk_mirror_configuration =
        GTK_WIDGET(gtk_builder_get_object(builder, "stk_mirror_configuration"));
    stk_action =
        GTK_WIDGET(gtk_builder_get_object(builder, "stk_action"));
    graph_packets =
        GTK_WIDGET(gtk_builder_get_object(builder, "graph_packets"));
    result_level = GTK_WIDGET(gtk_builder_get_object(builder, "result_level"));
    result_packets =
        GTK_WIDGET(gtk_builder_get_object(builder, "result_packets"));
    result_total = GTK_WIDGET(gtk_builder_get_object(builder, "result_total"));

    entry_output_file = GTK_WIDGET(gtk_builder_get_object(builder, "entry_output_file"));
    entry_scan_port = GTK_WIDGET(gtk_builder_get_object(builder, "entry_scan_port"));
    entry_cidr = GTK_WIDGET(gtk_builder_get_object(builder, "entry_cidr"));
    entry_device_port = GTK_WIDGET(gtk_builder_get_object(builder, "entry_device_port"));
    open_devices_file = GTK_WIDGET(gtk_builder_get_object(builder, "open_devices_file"));
    entry_device_ip = GTK_WIDGET(gtk_builder_get_object(builder, "entry_device_ip"));
    open_output_file_button = GTK_WIDGET(gtk_builder_get_object(builder, "open_output_file_button"));

    for (int i = 0; i < 100; i++) values[i] = 0;

    load_config(NULL);

    gtk_widget_show(window);

    gtk_main();

    return EXIT_SUCCESS;
}
