#include "interface/cliparser.h"

#include "common/common.h"

#define PARSER_DELIM "=\n"
#define PARSER_INITIALIZE(str) strtok(str, PARSER_DELIM)
#define PARSER_CONTINUE strtok(NULL, PARSER_DELIM)

/** @brief Identifies command line arguments of one char.
 *  @return NULL if argument is not found.
 */
static ArgsOpt *getOptbyShort(ArgsOpt *p_opt, char p_arg) {
    ArgsOpt *opt = p_opt;

    for (int i = 0; opt->args != 0; i++, opt = &p_opt[i]) {
        if (opt->args == p_arg) {
            return opt;
        }
    }
    return NULL;
}

/** @brief Identifies command line arguments of more than one char.
 *  @return NULL if the argument is not found.
 */
static ArgsOpt *getOptbyLong(ArgsOpt *p_opt, char *p_arg) {
    ArgsOpt *opt = p_opt;

    for (int i = 0; opt->args != 0; i++, opt = &p_opt[i]) {
        if (!strcmp(opt->argl, p_arg)) {
            return opt;
        }
    }
    return NULL;
}

// Used in interface for command line arguments.
int ParserCLI(ArgsCore *p_core, int p_argc, char **p_argv, void *p_data) {
    char key;
    ArgsOpt *opt;

    for (int i = 0; i < p_argc; i++) {
        char *value = p_argv[i];
        char *arg = NULL;
        char error_msg[70];

        if (value[0] == '-') {
            if (value[1] == '-') {
                char *fsp = &value[2];

                char *keyLong = PARSER_INITIALIZE(fsp);
                opt = getOptbyLong(p_core->opt, keyLong);

                // unknown argument
                if (NULL == opt) {
                    strcpy(error_msg, "unrecognized option '--");
                    strcat(error_msg, keyLong);
                    strcat(error_msg, "'");
                    ELOG(ERROR_INTERFACE, error_msg);
                }
                key = opt->args;

                if (opt->option != NONE) {
                    arg = PARSER_CONTINUE;

                    if (opt->option == REQUIRED && NULL == arg) {
                        strcpy(error_msg, "option '");
                        strcat(error_msg, keyLong);
                        strcat(error_msg, "' requires an argument");
                        ELOG(ERROR_INTERFACE, error_msg);
                    }
                }
            } else {
                char keyCand = value[1];
                opt = getOptbyShort(p_core->opt, keyCand);

                // unknown argument
                if (NULL == opt) {
                    strcpy(error_msg, "invalid option -- '");
                    strncat(error_msg, &keyCand, 1);
                    strcat(error_msg, "'");
                    ELOG(ERROR_INTERFACE, error_msg);
                }
                key = opt->args;

                if (opt->option != NONE) {
                    if (i + 1 < p_argc) {
                        // argument has value
                        if (p_argv[i + 1][0] != '-') {
                            arg = p_argv[++i];
                        } else {
                            // argument has no value
                            if (opt->option == REQUIRED) {
                                strcpy(error_msg,
                                       "option requires an argument -- '");
                                strncat(error_msg, &keyCand, 1);
                                strcat(error_msg, "'");
                                ELOG(ERROR_INTERFACE, error_msg);
                            }
                        }
                    } else {
                        // End of arguments with last having no value
                        if (opt->option == REQUIRED) {
                            strcpy(error_msg,
                                   "option requires an argument -- '");
                            strncat(error_msg, &keyCand, 1);
                            strcat(error_msg, "'");
                            ELOG(ERROR_INTERFACE, error_msg);
                        }
                    }
                }
            }
        } else {
            key = ARG_CMD;
            arg = value;
        }

        p_core->handler(key, arg, p_data);
    }

    return SUCCESS;
}
