#include "commander/manager.h"

#include "draft.h"
#include "hom/coap/coap.h"
#include "hom/dns/dns.h"
#include "hom/memcached/memcached.h"
#include "hom/ntp/ntp.h"
#include "hom/snmp/snmp.h"
#include "hom/ssdp/ssdp.h"
#include "hom/cldap/cldap.h"

LhfPlan *Planner(LhfDraft *p_draft) {
    LhfPlan *plan = NULL;

    memalloc(&plan, sizeof(LhfPlan));
    memalloc(&(plan->atkData), sizeof(LhfDraft));

    switch (p_draft->type) {
        // Please put new mirrors in alphabetical order
        case COAP:
            plan->type = COAP;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteCoapMirror;
            break;

        case DNS:
            plan->type = DNS;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteDnsMirror;
            break;

        case MEMCACHED_GETSET:
            plan->type = MEMCACHED_GETSET;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteMemcachedMirror;
            break;
        case MEMCACHED_STATS:
            plan->type = MEMCACHED_STATS;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteMemcachedMirror;
            break;
        case NTP:
            plan->type = NTP;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteNtpMirror;
            break;
        case SSDP:
            plan->type = SSDP;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteSsdpMirror;
            break;
        case SNMP:
            plan->type = SNMP;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteSnmpMirror;
            break;
        case CLDAP:
            plan->type = CLDAP;
            memcpy(plan->atkData, p_draft, sizeof(LhfDraft));
            plan->atk_cmd = ExecuteCldapMirror;
            break;

        default:
            Efatal(ERROR_PLANNER, "Mirror not implemented\n");
    }

    return plan;
}

void CallMirrors(LhfPlan *p_plan) {
    if (NULL != p_plan) {
        if ((p_plan->atk_cmd(p_plan->atkData)) < 0) {
            Efatal(ERROR_MIRROR, "Mirror not found\n");
        }
    }
}
