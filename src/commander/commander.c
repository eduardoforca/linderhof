#include "commander/commander.h"

#include <pthread.h>

#include "commander/manager.h"
#include "draft.h"

/** @brief  closes linderhof in case of error*/
static void closeLhf() {
    Elog(SUCCESS, "Closing lhf\n");
    memoryclean();
}

/** @brief handler for fatal errors*/
static void fatalHandler(int p_arg) {
    printf("Fatal error\n");
    closeLhf();
    exit(p_arg);
}

/** @brief closes handler when fatal errors happen*/
static void closeHandler(int p_arg) {
    closeLhf();
    exit(0);
}

/** @brief  sets signal handler */
int SetSigHdr(int p_sig, void p_func(int)) {
    struct sigaction act;

    memset(&act, '\0', sizeof(act));

    act.sa_handler = p_func;

    if (sigaction(p_sig, &act, NULL) < 0) {
        ELOG(ERROR_SIGNAL, "sigaction error");
    }

    return SUCCESS;
}

void linderhofBootstrap() {
    SetSigHdr(SIGINT, closeHandler);
    SetSigHdr(SIGQUIT, closeHandler);
    SetSigHdr(SIGPIPE, fatalHandler);
    ESetErrorAction(fatalHandler);
    srand(time(NULL));
}

int StartMirrorAttack(LhfDraft p_draft) {
    LhfPlan *plan;

    plan = Planner(&p_draft);
    CallMirrors(plan);

    return SUCCESS;
}
