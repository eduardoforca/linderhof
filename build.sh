#!/bin/bash

usage() {
  echo "Usage: $0 [-g|-d] [-b] [-h]"
  echo -e "\t-b Build"
  echo -e "\t-g Generate build files"
  echo -e "\t-d Generate build files for debugging"
  echo -e "\t-h Show help"
  echo -e ""
  echo -e "Examples:"
  echo -e "$0 -b     Build Linderhof"
  echo -e "$0 -g -b  Generate build files and build Linderhof\n"
  exit 1
}

if [[ $1 == "" ]]; then
  usage
fi

while getopts "gdbh" opt; do
  case "$opt" in
  g) BUILD_TYPE="Release" ;;
  d) BUILD_TYPE="Debug" ;;
  b) BUILD=1 ;;
  h) usage ;;
  *) usage ;;
  esac
done

for name in gcc cmake g++; do
  if ! type $name &>/dev/null; then
    echo -e "$name not found"
    deps=1
  fi
done

if [ "$deps" == 1 ]; then
  echo -e "Please install the above dependencies and rerun this script."
  exit 1
fi

if [ "$BUILD_TYPE" != "" ]; then
  if [ -d build ]; then
    rm -R build
  fi
  cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=$BUILD_TYPE
fi

if [ "$BUILD" == 1 ]; then
  if [ ! -d build ]; then
    echo -e "Please generate build files with -g or -d before building.\n"
    exit 1
  else
    cmake --build build -- -j3
  fi
  if [ $(whoami) == root ]; then
    setcap cap_net_admin,cap_net_raw+ep bin/lhf
  else
    echo -e "\nPlease rerun this script as root or using sudo if you want to run linderhof without superuser privileges, or simply run:"
    echo -e "sudo setcap cap_net_admin,cap_net_raw+ep bin/lhf\n"
    exit 1
  fi
fi
